import QueryBar from './queryBar';
import SjAutoComplete from './autocomplete';
import Terms from './terms';

import { $, constants, labels } from './helpers';

import '../css/jquery.sjautocomplete.scss';

function init() {

    if (!$) {
        console.warn('SjAutoComplete requires jQuery which was not loaded.');
        return;
    }

    if ($.fn.sjautocomplete) {
        return;
    }

    var checkSettings = function (_settings) {
        var success = true;
        if (!_settings.server) {
            console.warn('server is not specified');
            success = false;
        }
        if (!_settings.get_credentials) {
            console.warn('get_credentials are not specified');
            success = false;
        }
        return success;
    }

    $.fn.sjautocomplete = function (options) {
        var self = this;

        if (options === 'destroy') {
            var widget = self.data('sjautocomplete');
            if (widget && widget.destroy) {
                widget.destroy();
            }
            return;
        }

        var settings = $.extend({
            'server': '',
            'get_credentials': '',
            'get_param': '',
            'extra_get_params': {},
            // function can return string or promise
            'format_query': function (q) { return q; },
            'search_callback': function (q) { },
            'create_callback': function () { },
            'hotkeys': false,
            'visual_editor_callback': null,
            'placeholder': '',
            'hide_autocomplete': false,
            'query_changed': function (q) { },
            'showIcon': false // show or hide autocomplete suggestions icons (fuse-search widget)
        }, options);

        // merge class names and overrides
        settings.classNames = $.extend({}, constants, settings.classNames);
        settings.labels = $.extend({}, labels, settings.labels);

        if (!checkSettings(settings))
            return;

        var autoCompleteObj = new SjAutoComplete(self, settings);
        self.data('sjautocomplete', autoCompleteObj);
        autoCompleteObj.init();
        settings.create_callback();
        return autoCompleteObj;
    };

    $.fn.sjquerybar = function (options) {
        var self = this;

        if (options === 'destroy') {
            var widget = self.data('sjquerybar');
            if (widget && widget.destroy) {
                widget.destroy();
            }
            return;
        }

        var settings = $.extend({
            mode: null,
            enableLayouts: false,
            preferredLayout: null, // filters | query (null - filters)
            autocomplete: {},
            filtersChanged: (filters) => { },
            getFilters: () => { return []; },

            /**
             * prefered mode from state (e.g. supported in search widget, but not supported for view editor global query)
             */
            getPreferedMode: null
        }, options);

        if (!options.autocomplete.extra_get_params) {
            options.autocomplete.extra_get_params = {};
        }
        var types = [];
        if (options.autocomplete.extra_get_params.types) {
            types = options.autocomplete.extra_get_params.types.split(',');
        }
        if (types.indexOf('@fields') == -1 && types.indexOf('@') == -1) {
            types.push('@fields');
        }
        if (types.length) {
            options.autocomplete.extra_get_params.types = types.join(',');
        }

        if (!checkSettings(settings.autocomplete))
            return;

        var queryBarObj = new QueryBar(self, settings);
        self.data('sjquerybar', queryBarObj);
        queryBarObj.init();
        return queryBarObj;
    };

    $.fn.sjterms = function (options) {
        var self = this;

        if (options === 'destroy') {
            var widget = self.data('sjterms');
            if (widget && widget.destroy) {
                widget.destroy();
            }
            return;
        }

        var settings = $.extend({}, options);

        var termsObj = new Terms(options.settings, options.state);
        self.data('sjterms', termsObj);
        termsObj.render();
        return termsObj;
    };
}

// page can be already loaded
if (document.readyState == 'complete' || document.readyState == 'interactive') {
    init();
} else {
    // or not loaded
    document.addEventListener('DOMContentLoaded', function () {
        init();
    });
}
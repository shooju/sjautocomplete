import { jqOn, endsWith, tooltip, Request, $, suggestions } from './helpers';
import Annotations from './annotations';

var autocompleteCounter = 0;

var SjAutoComplete = function (domElement, options) {
    this.domElement = domElement;
    $.extend(this, options);

    this.id = autocompleteCounter++;
    this.request = new Request(options.server, options.get_credentials);

    if (this.parent) {
        this.annotations = this.parent.annotations;
    } else {
        this.annotations = new Annotations(this.request);
    }

    this.options = [];

    if (!this.parent) {
        this.domElement.attr('sj-ac-mode', 'simple');
    }
};

var SjRowResult = function (domElement, autocompleteInstance, option) {
    this.element = domElement;
    this.option = option;
    this.autocompleteInstance = autocompleteInstance;
    this.selectedCompleteEnding = null;
    this.selected = false;
    this.index = null;
    this.completeEndings = [];

    this.init();
};

var SjCompleteEnding = function (autocompleteInstance: any, domElement, sjResultRow, completeEndingFromOption) {
    this.autocompleteInstance = autocompleteInstance;
    this.value = completeEndingFromOption.v;
    this.index = null;
    this.row = sjResultRow;
    this.element = domElement;
    this.selected = false;

    this.init();
};

SjAutoComplete.prototype = (function () {
    // runs after events keyup paste mouseup
    var searchInput_changeHandler = function (autocompleteInstance, timeout, show, autoformat) {
        var val = autocompleteInstance.get_value() || '';

        if (autoformat) {
            if (val) {
                // trim and replace new lines/tabs after paste
                var newVal = val.replace(/\s+/g, ' ').replace(/^\s+/g, '').replace(/\s+$/g, '');
                if (val != newVal) {
                    val = newVal;
                    autocompleteInstance.set_value(newVal, false);
                }
            }
        }

        if (val == "") {
            autocompleteInstance.set_selectedRow(null);
            autocompleteInstance.set_selectedValue(null);
            //autocompleteInstance.hoveredRow = null;
        }

        if (show) {
            autocompleteInstance._visible = true;
        }
        autocompleteInstance.getDataAsync(null, null, timeout);

        autocompleteInstance.query_changed(val);
    };

    //returns markup of option value with highlighting of "complete" substring 
    var getHighlightedValueString = function (autocompleteInstance, result, option, annotations) {
        const { classNames } = autocompleteInstance;
        var showIcon = autocompleteInstance.showIcon;
        var str = option.value;

        switch (option.type) {
            case "field":
                if (annotations[option.value] && annotations[option.value].name) {
                    str = annotations[option.value].name;
                    return `<span class="${classNames.resultValue} ${classNames.resultAnnotation}">
                        ${$('<span>').text(str).html()}
                        <span class='${classNames.resultAnnotation}-tip'>
                            <span>
                                ${$('<span>').text(option.value).html()}
                            </span>
                        </span>
                    </span>`;
                }
                break;
            case "fieldvalue":
                var annotation = annotations[result.field];
                if (annotation && annotation.values && annotation.values[option.value]) {
                    str = annotation.values[option.value];
                    return `<span class="${classNames.resultValue} ${classNames.resultAnnotation}">
                        ${$('<span>').text(str).html()}
                        <span class='${classNames.resultAnnotation}-tip'>
                            <span>
                                ${$('<span>').text(option.value).html()}
                            </span>
                        </span>
                    </span>`;
                }
                break;
        }

        var highlightedSubstring = option.complete;
        var lengthWithoutHighlight = 0;
        if (option.complete_del_chars) {
            highlightedSubstring = highlightedSubstring.substring(option.complete_del_chars + 1);
            lengthWithoutHighlight = str.length - highlightedSubstring.length - 1;
        } else {
            lengthWithoutHighlight = str.length - highlightedSubstring.length;
        }

        highlightedSubstring = highlightedSubstring.replace(/[\-\[\]{}()*+?.,\\\^$|#\s]/g, "\\$&");
        var reg = new RegExp(highlightedSubstring + "(?!.*" + highlightedSubstring + ")", 'gi'); //rexes something like "aaa(?!.*aaa)"
        var final_str = str.replace(reg, function (str1) {
            if (autocompleteInstance.adjust_term) {
                str1 = autocompleteInstance.adjust_term(str1);
            }
            return `<span class="${classNames.resultHighlight}">` + str1 + '</span>';
        });

        var res = $(`${showIcon ? SJ_Result_Value_Icon(classNames) : ''}<span class="${classNames.resultValue} ${classNames.resultGreyHighlight}">` + final_str + '</span>');
        res.filter(`.${classNames.resultValue}`).attr('title', str);
        return res;
    };

    //returns markup of block with complete endings in every row 
    var buildCompleteEndingMarkup = function (autocompleteInstance, option) {
        const { classNames } = autocompleteInstance;

        if (!option.complete_endings)
            return "";
        var markup = "";
        markup += `<div class="${classNames.resultRow}-completeEnding-block">`;
        option.complete_endings.forEach(function (item, index) {
            markup += `<span tabindex="0" class="${classNames.resultRow}-complete-ending" value="` + item.v + '">' + item.v;
            if (item.h) {
                if (item.v) {
                    markup += '&nbsp;';
                }
                markup += (item.h + '');
            }
            markup += '</span>';
        });
        markup += '</div>';
        return markup;
    };

    //returns row jQuery object (not markup)
    var buildRowMarkup = function (result, option, autocompleteInstance, annotations) {
        const { classNames } = autocompleteInstance;

        var row = $(classNames.resultRowEl).addClass(classNames.resultRow);
        var valueMarkup = getHighlightedValueString(autocompleteInstance, result, option, annotations);
        var optionValue = valueMarkup;
        var div_1 = $(`<div class='${classNames.resultValueWrapper}'></div>`).appendTo(row);
        div_1.append(optionValue);

        var countAndHintHtml = '';

        // if types is ONLY text matches and text style is not term, then do not show count in autocomplete
        var hideCounters = autocompleteInstance.extra_get_params &&
            autocompleteInstance.extra_get_params.types === 'text' &&
            autocompleteInstance.extra_get_params.text_style !== 'term';

        if (!hideCounters) {
            if (option.count) {
                countAndHintHtml += ` <span class='${classNames.optionCount}'>` + option.count + "</span>";
            }
            if (option.hint && autocompleteInstance.show_hints !== false) {
                countAndHintHtml += ` <span class='${classNames.optionHint}'>` + option.hint + "</span>";
            }
        }
        div_1.append(countAndHintHtml);

        var completeEndingMarkup = buildCompleteEndingMarkup(autocompleteInstance, option);
        var completeEnding = $(completeEndingMarkup);
        row.append(completeEnding);
        row.attr('value', option.value);
        // row.attr('tabindex', 0);
        return row;
    };

    //callback being executed as success callback after users clicks on result row  
    var successCallback_afterRowClicked = function (response, autocompleteInstance) {
        // var selectedRow = autocompleteInstance.get_selectedRow();
        autocompleteInstance.set_selectedRow(null);
        autocompleteInstance.set_results(response, null, () => {
            autocompleteInstance.displayResultBlock('show');
        });
    };

    //callback being executed as success callback after users press TAB on result row  
    var successCallback_afterCompleteEndingClicked = function (response, autocompleteInstance) {
        autocompleteInstance.set_selectedRow(null);
        autocompleteInstance.set_results(response, null, () => {
            autocompleteInstance.displayResultBlock('show');
        });
    };

    //sets the scrolltop value of div with rows. It is useful with navigation through rows using UP and DOWN buttons
    var setRowContainerScrollTop = function (autocompleteInstance) {
        if (autocompleteInstance.hoveredRow) {
            var index = autocompleteInstance.hoveredRow.index;
            var rowContainer = autocompleteInstance._searchResults.find(`.${autocompleteInstance.classNames.searchResults}-rows`);
            var currentScrollTop = rowContainer.scrollTop();
            if (currentScrollTop < index * 40)
                autocompleteInstance._searchResults.find(`.${autocompleteInstance.classNames.searchResults}-rows`).scrollTop(index * 40); //40 is current row height
        }
    };

    return {
        SjRowResult: SjRowResult,
        _self: this,
        loading: 0,
        constructor: SjAutoComplete,
        _value: "", //string entered by user
        _searchInput: null,
        _searchButton: null,
        _searchResults: null,
        _autocompleteWrapper: null,
        _timer: 0,
        _lastCall: null,
        selectedRow: null, //SjRowResult
        selectedValue: null,
        options: [],
        hoveredRow: null, //SjRowResult

        setSuggestions(suggestions) {
            this._setSuggestions(suggestions, this._autocompleteWrapper);
        },

        _setSuggestions(suggestions, $container) {
            const { classNames } = this;

            if (suggestions && suggestions.length) {
                var $sEl = $container
                    .find(`.${classNames.queryBarSuggestions}-wrapper`)
                    .addClass(`${classNames.queryBarSuggestions}-visible`)
                    .find(`.${classNames.queryBarSuggestions}-items`)
                    .html('');
                suggestions.forEach((s, i) => {
                    $sEl.append(() => {
                        return $(classNames.queryBarSuggestionItemEl)
                            .html(s.highlighted)
                            .addClass(`${classNames.queryBarSuggestions}-item`)
                            .attr('data-text', s.value);
                    });
                    if (i < suggestions.length - 1) {
                        $sEl.append(', ');
                    }
                });
            } else {
                $container
                    .find(`.${classNames.queryBarSuggestions}-wrapper`)
                    .removeClass(`${classNames.queryBarSuggestions}-visible`);
            }
        },

        //executes method "handler" after specified timeout
        callMethodWithDelay: function (handler, timeout, onCancel) {
            var self = this;
            //check if pressed one of the buttons: left - 37, up - 38, right - 39, down - 40, shift - 16, ctrl - 17, alt - 18
            //if (event.keyCode && $.inArray(event.keyCode, [37, 38, 39, 40, 16, 17 ,18]) >= 0)
            //    return;
            if (this._timer) {
                clearTimeout(this._timer);
                this._timer = undefined;
                if (onCancel) {
                    onCancel();
                }
            }
            this._timer = setTimeout(function () {
                self._timer = undefined;
                handler();
            }, timeout);
        },

        //builds markup of plugin, binds events
        init: function () {
            var self = this;
            var nested_els = this.domElement.children();

            const { classNames, labels } = this;
            this._autocompleteWrapper = $(SJ_Autocomplete_Wrapper(classNames)).appendTo(this.domElement);
            var div_1 = $('<div></div>').appendTo(this._autocompleteWrapper);


            if (this.suggest && !this.parent) {
                this._autocompleteWrapper.append(suggestions(classNames, labels));
            }
            this._searchInput = $(SJ_Search_Input_Html(classNames));

            // check if input or textarea was already in control
            if (nested_els.length) {
                this._searchInput.find('input').replaceWith(nested_els);
                this._searchInput.find('input,textarea').addClass(`${classNames.searchInput} inputClass`);
            }

            if (this.placeholder) {
                this._searchInput.find('input,textarea').attr('placeholder', this.placeholder);
            }

            this._searchInput.appendTo(div_1);
            //this._searchButton = $(SJ_Search_Button_Html).appendTo(div_1);
            this._searchResults = $(this.dropdownElement || SJ_Search_Results_Html(classNames)).appendTo(this._autocompleteWrapper);

            this._searchInput.append(`<div class="${classNames.tips}">
                <div class="${classNames.searchHelp}-icon">
                    <svg viewBox="0 0 1792 1792">
                        <path d="M1024 1376v-192q0-14-9-23t-23-9h-192q-14 0-23 9t-9 23v192q0 14 9 23t23 9h192q14 0 23-9t9-23zm256-672q0-88-55.5-163t-138.5-116-170-41q-243 0-371 213-15 24 8 42l132 100q7 6 19 6 16 0 25-12 53-68 86-92 34-24 86-24 48 0 85.5 26t37.5 59q0 38-20 61t-68 45q-63 28-115.5 86.5t-52.5 125.5v36q0 14 9 23t23 9h192q14 0 23-9t9-23q0-19 21.5-49.5t54.5-49.5q32-18 49-28.5t46-35 44.5-48 28-60.5 12.5-81zm384 192q0 209-103 385.5t-279.5 279.5-385.5 103-385.5-103-279.5-279.5-103-385.5 103-385.5 279.5-279.5 385.5-103 385.5 103 279.5 279.5 103 385.5z"/>
                    </svg>
                </div>
                <div class="${classNames.searchHelp}-popup">
                    <div class="${classNames.searchHelp}-popup-inner">
                        ${tooltip(classNames)}
                    </div>
                </div>
            </div>`);

            // hide result block after ESC or ENTER or TAB pressed
            this._autocompleteWrapper.keydown(function (event) {
                // ESC or ENTER or TAB is pressed
                if (event.keyCode == 13 || event.keyCode == 27 /*|| event.keyCode == 9 */) {
                    self.displayResultBlock('hide');
                }
            });
            //hide result block after user clicked outside of plugin
            jqOn($(document), 'mousedown.sjautocomplete' + self.id, null, function (e) {
                if (self._autocompleteWrapper[0] != e.target // if the target of the click isn't the container...
                    && self._autocompleteWrapper.has(e.target).length === 0) // ... nor a descendant of the container
                {
                    self.displayResultBlock('hide');
                }
            });

            //"Search" button click event handler
            /* this._searchButton.find('.sj-searchButton')
                .click({ thisInstance: this },
                    function(eventObject) {
                        if (eventObject.data.thisInstance.search_callback)
                            eventObject.data.thisInstance.search_callback();
                    }
                ); */

            var $searchInput = this.getInput();

            //"enter" click on search input
            jqOn($searchInput, 'focus.sjautocomplete', null, function () {
                self._visible = true;
                self.getDataAsync();
            });
            jqOn($searchInput, 'hide.sjautocomplete', null, function () {
                self.displayResultBlock('hide');
            });
            jqOn($searchInput, 'keypress.sjautocomplete', null, function (event) {
                switch (event.which) {
                    case 13:
                        event.preventDefault();
                        if (self.search_callback)
                            self.search_callback();
                        break;
                    case 37:
                        break;
                    case 39:
                        break;
                }
            });

            //handles any changes of search input
            jqOn($searchInput, 'paste.sjautocomplete', null, function (event) {
                setTimeout(function () {
                    searchInput_changeHandler(self, null, true, true);
                }, 0);
            });
            jqOn($searchInput, 'keyup.sjautocomplete mouseup.sjautocomplete', null, function (event) {
                if (event.type != 'mouseup') {
                    // left, right, up and down btns
                    if (event.type == 'keyup' && [37, 38, 39, 40, 27, 9].indexOf(event.keyCode) !== -1) {
                        return;
                    }
                    searchInput_changeHandler(self, (event.keyCode == 13 || event.keyCode == 9) ? 0 : 400, event.keyCode != 13, false);
                }
                self.queryChanged();
            });

            //handles clicks of UP and DOWN buttons on input
            jqOn($searchInput, 'keydown.sjautocomplete', null, function (event) {
                var v = self.get_value();
                self.prevValue = v;

                if (self.hotkeys) {
                    if (event.altKey || event.ctrlKey) {
                        var char = String.fromCharCode(event.which);
                        // alt-e - expression
                        // alt-v - visual editor
                        if (event.altKey && char == 'E') {
                            if (v && v[0] != '=') {
                                v = '={{ ' + v + ' }}';
                                self.set_value(v, true);
                            }
                            console.log('expression');
                        }
                        /*
                        else if (event.altKey && char == 'V' && self.visual_editor_callback) {
                            console.log('visual editor');
                            self.visual_editor_callback(v);
                        }
                        */
                    }
                }

                //execute only after button pressed: left - 37, up - 38, right - 39, down - 40
                if ($.inArray(event.keyCode, [38, 40, 13, 39, 37, 9]) < 0) {
                    self.btnsNavigation = false;
                    return;
                }

                var is_option_select_event;
                if (self.is_option_select_event) {
                    is_option_select_event = self.is_option_select_event(event, self);
                } else {
                    is_option_select_event = event.keyCode == 9 || event.keyCode == 13; // tab
                }
                if (is_option_select_event) {
                    //if (self.btnsNavigation) {
                    var row = self.get_selectedRow() || self.hoveredRow;
                    if (row) {
                        row.fireClickEvent(event);
                        console.log('preventDefault');
                        event.preventDefault();
                        /*
                        if (row.fireClickEvent() !== false) {
                            event.stopPropagation();
                        }
                        */
                    }
                    //}
                    self.btnsNavigation = false;
                } else if (event.keyCode == 40) {
                    self.selectNextRow();
                    console.log('preventDefault');
                    event.preventDefault();
                    self.btnsNavigation = true;
                }
                else if (event.keyCode == 38) {
                    self.selectPreviousRow();
                    console.log('preventDefault');
                    event.preventDefault();
                    self.btnsNavigation = true;
                }
                else if (event.keyCode == 39 && self.hoveredRow) {
                    self.hoveredRow.selectNextCompleteEnding();
                    self.btnsNavigation = true;
                }
                else if (event.keyCode == 37 && self.hoveredRow) {
                    self.hoveredRow.selectPreviousCompleteEnding();
                    self.btnsNavigation = true;
                }
                /*
                else if (event.keyCode == 9 && self.hoveredRow) {
                    event.preventDefault();
                    var selectedRow = self.hoveredRow;
                    //event fires after click on row and after complete ending clicked. So we have to determine which element is clicked
                    var isRow = $(event.target).hasClass(classNames.resultRow);
                    //complete ending is clicked
                    if (!isRow && selectedRow && selectedRow.selectedCompleteEnding) {
                        var selectedCompleteEnding = selectedRow.selectedCompleteEnding;
                        selectedCompleteEnding.fireTabClickEvent();
                    } else {
                        selectedRow.fireClickEvent();
                    }
                    self.btnsNavigation = true;
                }
                */
            });

            jqOn(this._autocompleteWrapper, 'click', `.${classNames.acClear}`, function (event) {
                self.clear(event, true);
            });

            // clear hovered row when mouse moved outside of results container
            jqOn(this._searchResults, 'mouseleave', null, function (event) {
                if (self.hoveredRow) {
                    self.hoveredRow.unhover();
                    self.hoveredRow = null;
                }
            });

            this._searchInput.on('keydown', function (e) {
                // console.log('searchInput keydown', self._visible, self.hoveredRow);
                if (self._visible && self.hoveredRow) {
                    switch (e.keyCode) {
                        case 37:
                        case 39:
                        case 13:
                            // console.warn('Search prevented after autocomplete change', e.keyCode, e);
                            e.preventDefault();
                            e.stopPropagation();
                            return false;
                    }
                }
            });

            //handles clicks of UP, DOWN, RIGHT, LEFT and TAB buttons on rows
            jqOn(this._searchResults, 'keydown', `.${classNames.resultRow}`, function (event) {
                //execute only after button pressed: left - 37, up - 38, right - 39, down - 40
                /*
                if ($.inArray(event.keyCode, [37, 38, 39, 9, 40]) < 0) {
                    self.btnsNavigation = false;
                    return;
                }
                */

                if (event.keyCode == 40) {
                    self.selectNextRow();
                    self.btnsNavigation = true;
                }
                else if (event.keyCode == 38) {
                    self.selectPreviousRow();
                    self.btnsNavigation = true;
                }
                else if (event.keyCode == 39) {
                    self.hoveredRow.selectNextCompleteEnding();
                    self.btnsNavigation = true;
                }
                else if (event.keyCode == 37) {
                    self.hoveredRow.selectPreviousCompleteEnding();
                    self.btnsNavigation = true;
                }
                //tab
                else {
                    var is_option_select_event;
                    if (self.is_option_select_event) {
                        is_option_select_event = self.is_option_select_event(event, self, true);
                    } else {
                        is_option_select_event = event.keyCode == 9 || event.keyCode == 13; // tab or enter
                    }
                    if (is_option_select_event) {
                        console.log('preventDefault');
                        event.preventDefault();

                        var ac = self;
                        var selectedRow = ac.hoveredRow;
                        //event fires after click on row and after complete ending clicked. So we have to determine which element is clicked
                        var isRow = $(event.target).hasClass(classNames.resultRow);
                        //complete ending is clicked
                        if (!isRow) {
                            var selectedCompleteEnding = selectedRow.selectedCompleteEnding;
                            selectedCompleteEnding.fireTabClickEvent();
                        } else {
                            selectedRow.fireClickEvent();
                        }
                        self.btnsNavigation = true;
                        setRowContainerScrollTop(self);

                    } else {
                        self.btnsNavigation = false;
                        return;
                    }
                }
                return false;
            });

            this._searchResults.find(`.${classNames.searchResults}-function-show-more-label`)
                //.mousedown(function (event) {
                //event.preventDefault();
                //})
                .click(function (event) {
                    var $checkbox = $(this).prev();
                    $checkbox.prop('checked', !$checkbox.prop('checked'));
                    // waits while function docs expands after 'show more' click
                    //setTimeout(function () {
                    //    self.displayResultBlock('show');
                    //}, 10);
                });

            // close suggest
            jqOn(this._autocompleteWrapper, 'click', `.${classNames.queryBarSuggestions}-wrapper .${classNames.queryBarSuggestions}-close`, e => {
                $(e.currentTarget)
                    .closest(`.${classNames.queryBarSuggestions}-wrapper`)
                    .removeClass(`${classNames.queryBarSuggestions}-visible`);
                if (this.suggestionClosed) {
                    this.suggestionClosed();
                }
            });

            // select suggestion
            jqOn(this._autocompleteWrapper, 'click', `.${classNames.queryBarSuggestions}-wrapper .${classNames.queryBarSuggestions}-item`, e => {
                var newKeyword = $(e.currentTarget).attr('data-text');
                this.set_value(newKeyword);

                $(e.currentTarget)
                    .closest(`.${classNames.queryBarSuggestions}-wrapper`)
                    .removeClass(`${classNames.queryBarSuggestions}-visible`);
            });

            setTimeout(() => {
                self.queryChanged();
            });
        },

        queryChanged: function () {
            const { classNames } = this;

            if (this._autocompleteWrapper) {
                var $clear = this._autocompleteWrapper.find(`.${classNames.acClear}`);
                if (this.get_value()) {
                    $clear.removeClass(`${classNames.acClear}-hidden`);
                } else {
                    $clear.addClass(`${classNames.acClear}-hidden`);
                }
            }
        },

        //build input value. It calls after row was clicked
        buildSearchInputString: function (sjResultRow) {
            var inputValue = this.get_value();

            //remove chars (count = complete_del_chars) from the end of input string
            if (sjResultRow.option.complete_del_chars) {
                inputValue = inputValue.substring(0, inputValue.length - sjResultRow.option.complete_del_chars);
            }

            inputValue += sjResultRow.option.complete;

            if (sjResultRow.selectedCompleteEnding) {
                var completeEnding = sjResultRow.selectedCompleteEnding.value;
                if (!completeEnding) {
                    completeEnding = ' ';
                }

                inputValue += completeEnding;
            }

            return inputValue;
        },

        getInput: function () {
            const { classNames } = this;
            return this._searchInput.find(`.${classNames.searchInput}:not(input[type="button"])`);
        },

        //set value of input string
        set_value: function (value, fireReload) {
            this._set_value(value, fireReload);
        },
        _set_value: function (value, fireReload) {
            var inputEl = this.getInput();
            inputEl.val(value);

            if (fireReload !== false) {
                inputEl.change();
            }

            this.queryChanged();
        },

        //return value of selected row
        get_value: function (cursor) {
            var inputEl = this.getInput();
            var val = inputEl.val();
            if (cursor) {
                if (inputEl[0]) {
                    var start = inputEl[0].selectionStart;
                    val = val.substr(0, start);
                }
            }
            return val;
        },

        //get value (option.value) of selected row 
        get_selectedvalue: function () {
            //return this.selectedValue;
            if (this.get_selectedRow() != null) {
                var selRow = this.get_selectedRow();
                return selRow.option.value;
            }
        },

        set_selectedValue: function (value) {
            this.selectedValue = value;
        },

        clear: function (e, focus = true) {
            this._clear(e, focus);
        },
        _clear: function (e, focus = true) {
            var self = this;
            this.set_value('', true);
            setTimeout(() => {
                self.queryChanged();
                if (focus) {
                    self.getInput().focus();
                }
            });
        },

        //set focus on input, make new request and update results
        set_focus: function () {
            // console.warn('getDataAsync set_focus');
            // this.getDataAsync();
        },

        is_advanced_mode: function () {
            return this.parent && (this.parent.settings.mode == "advanced" || this.parent.settings.mode == "minimal");
        },

        set_status: function (data, err, q) {
            if (this.parent) {
                this.parent.set_status(data, err, q);
            }
        },

        // can be overriden
        getRequestParams: function (query) {
            return this._getRequestParams(query);
        },
        _getRequestParams: function (query) {
            var self = this;
            var data = {
                location_type: 'sjautocomplete',
                location: location.href
            };
            data[self.get_param] = query;
            Object.keys(self.extra_get_params).forEach(function (key, index) {
                data[key] = self.extra_get_params[key];
            });

            if (self.is_advanced_mode()) {
                data['validate_query'] = 'y';
            }

            return {
                url: self.server + "/api/1/series/autocomplete",
                data: data
            }
        },

        loadItems: function (requestParams) {
            return this._loadItems(requestParams);
        },
        _loadItems: function (requestParams) {
            var self = this;
            return $.ajax({
                url: requestParams.url,
                data: requestParams.data,
                beforeSend: function (xhr) {
                    var credentials = self.get_credentials();
                    xhr.setRequestHeader("Authorization", "Basic " + btoa(credentials.api_username + ":" + credentials.api_secret));
                }
            });
        },

        //sends request. Updates results in success callback if  successCallback is not specified, otherwise call successCallback
        getDataAsync: function (successCallback, errorCallback, timeout, force) {
            var self = this;
            const { classNames } = this;

            function handleResponse(response, redraw = undefined, forceRedraw = false) {
                if (self._visible || force || forceRedraw) {
                    if (successCallback) {
                        successCallback(response, self);
                    } else {
                        if (redraw !== false) {
                            self.set_results(response, null, () => {
                                if (self._visible) {
                                    self.displayResultBlock('show');
                                } else {
                                    self.displayResultBlock('hide');
                                }
                            });
                        }
                        if (self._visible) {
                            self.displayResultBlock('show');
                        } else {
                            self.displayResultBlock('hide');
                        }
                    }
                }
            }

            function finishLoading() {
                if (self.loading) {
                    self.loading--;
                }
                if (!self.loading && self._autocompleteWrapper && self._autocompleteWrapper.removeClass) {
                    self._autocompleteWrapper.removeClass(`${classNames.acLoading}`);
                }
            }

            function startLoading() {
                if (!self.loading && self._autocompleteWrapper) {
                    self._autocompleteWrapper.addClass(`${classNames.acLoading}`);
                }
                self.loading++;
            }

            function runQuery(query) {

                var requestParams = self.getRequestParams(query);

                // prevent the same call
                if (self._lastRequestParams && self._lastRequestParams == JSON.stringify(requestParams)) {
                    handleResponse(self._lastResponse, false);
                    return;
                }

                self._lastRequestParams = JSON.stringify(requestParams);

                if (self._lastCall && self._lastCall.abort) {
                    self._lastCall.abort();
                }

                startLoading();

                self._lastCall = self.loadItems(requestParams).then(function (response) {
                    self._lastResponse = response;
                    handleResponse(response, undefined, true);
                    finishLoading();
                    self.set_status(response, null, query);
                }, function (err) {
                    if (errorCallback) {
                        errorCallback();
                    }
                    finishLoading();
                    self.set_status(null, err, query);
                });
            }

            setTimeout(function () {
                var v = self.get_value(true) || '';

                if (typeof self.min_length == 'number' && v.length < self.min_length) {
                    self.displayResultBlock('hide');
                    return;
                }

                var query = self.format_query(v);
                var queryPromise;
                if (!query || typeof query == 'string') {
                    var queryDfd = $.Deferred();

                    queryDfd.resolve(query);

                    queryPromise = queryDfd.promise();
                } else {
                    queryPromise = query;
                }

                queryPromise.then(function (q) {
                    if (timeout) {
                        self.callMethodWithDelay(function () {
                            runQuery(q);
                        }, timeout, function () {
                            finishLoading();
                        });
                    } else {
                        runQuery(q);
                    }
                });
            });
        },

        //it is called after click on row 
        getDataAsync_afterRowClicked: function () {
            this.getDataAsync(successCallback_afterRowClicked);
        },

        //it is called after click on complete ending
        getDataAsync_afterCompleteEndingClicked: function (autocompleteInstance) {
            this.getDataAsync(successCallback_afterCompleteEndingClicked);
        },

        // can be called outside
        clear_cache: function () {
            this._lastRequestParams = null;
        },

        //Clears results and add new rows. Not removes preventToRemoveSelectedRow (sjResultRow).
        //it must to be selected row. It helps if case: 
        //1. select row without selecting complete ending
        //2. get new results from server but seleceted row will no be removed and stays selected
        //3. user can select complete ending and get new results
        set_results: function (result, preventToRemoveSelectedRow, readyCallback) {
            return this._set_results(result, preventToRemoveSelectedRow, readyCallback);
        },
        _set_results: function (result, preventToRemoveSelectedRow, readyCallback) {
            var self = this;
            const { classNames } = this;

            this.annotations.getAnnotations().then(annotations => {
                var resultsDiv = this._searchResults.find(`.${classNames.searchResults}-rows`);
                var resultHint = this._searchResults.find(`.${classNames.searchResults}-hint-text`);
                var resultHintBlock = this._searchResults.find(`.${classNames.searchResults}-hint`);
                this.clearResults(preventToRemoveSelectedRow);

                var hasHint = false;
                if (result.hint && this.show_hints !== false) {
                    resultHint.text(result.hint);
                    resultHintBlock.show();
                    hasHint = true;
                } else {
                    resultHint.text("");
                    resultHintBlock.hide();
                }

                var hasFunction = this.setFunctionInfo(result);

                var hasSeriesHint = this.setSeries(result);
                var hasDocs = this.setDocsBlock(result);

                if (result.options) {
                    result.options.forEach(function (option, index) {
                        var ignoreItem = preventToRemoveSelectedRow && preventToRemoveSelectedRow.option.value == option.value;
                        var addItem = !preventToRemoveSelectedRow || !ignoreItem;
                        if (addItem) {

                            var row = buildRowMarkup(result, option, self, annotations.by_field);
                            var sjRowResult = new SjRowResult(row, this, option);
                            // sjRowResult.index = index;
                            this.options.push(sjRowResult);
                            resultsDiv.append(row);
                        } else if (ignoreItem && this.options[0]) {
                            this.hoveredRow = this.options[0];
                        }
                    }, this);
                }

                this.options.forEach(function (op, i) {
                    op.index = i;
                });

                if (hasHint || hasFunction || hasSeriesHint || hasDocs || (result.options && result.options.length)) {
                    this._searchResults.attr('data-show', true).show();
                } else {
                    this._searchResults.attr('data-show', false).hide();
                }

                if (readyCallback) {
                    readyCallback();
                }
            });
        },

        //shows and hides result block
        displayResultBlock: function (action) {
            const { classNames } = this;

            if (this.hide_autocomplete) {
                action = 'hide';
            }
            if (!action)
                return;
            switch (action) {
                case 'show':
                    if (this._searchResults.attr('data-show') == 'false' || !this._autocompleteWrapper) {
                        return;
                    }
                    this._searchResults.show();
                    var $container = this._searchResults.find(`.${classNames.searchResults}`);
                    $container.css('max-height', '');

                    var resultWrapperHeight = window.innerHeight -
                        this._autocompleteWrapper.offset().top -
                        31 - // height of input container + space between input and result wrapper
                        20; // padding from the bottom                        

                    var resultBlockHeight = resultWrapperHeight -
                        20 - // padding at the top and bottom of .${classNames.searchResults}
                        11; // for pixel perfect 

                    if (this._searchResults.find(`.${classNames.searchResults}-hint-text`).text() != '')
                        resultBlockHeight -= this._searchResults.find(`.${classNames.searchResults}-hint`).outerHeight();

                    if (this._searchResults.find(`.${classNames.searchResults}-function-name`).text() != '')
                        resultBlockHeight -= this._searchResults.find(`.${classNames.searchResults}-function`).outerHeight();

                    if (this._searchResults.find(`.${classNames.searchResults}-info`).text() != '')
                        resultBlockHeight -= this._searchResults.find(`.${classNames.searchResults}-info`).outerHeight();

                    //if window height is too small then set height of div outside of the  window
                    if (resultBlockHeight < 0) {
                        resultBlockHeight += 200;
                    }

                    if (resultBlockHeight < 400) {
                        $container.css('max-height', resultBlockHeight);
                    }
                    if (this.onShow) {
                        this.onShow(true);
                    }
                    break;
                case 'hide':
                    this._visible = false;
                    this._searchResults.hide();
                    if (this.onShow) {
                        this.onShow(false);
                    }
                    break;
                default:
                    break;
            }
        },

        //shows or hides complete_ending popup inside selected row
        displayCompleteEnding: function () {
            /* if (this.selectedRow) {
                this.selectedRow.element.find(`.${classNames.resultRow}-completeEnding-block`).toggle();
            } */
        },

        //select row in results block
        set_selectedRow: function (sjResultRow) {
            if (sjResultRow != null)
                sjResultRow.select();
            else {
                var selectedRow = this.get_selectedRow();
                if (selectedRow != null)
                    selectedRow.unselect();
                this.selectedRow = null;
            }

        },

        //get selected row (SjResultRow)
        get_selectedRow: function () {
            return this.selectedRow;
        },

        //remove result rows (without current selected row if preventToRemoveSelectedRow is specified (SpResultRow type)) 
        clearResults: function (preventToRemoveSelectedRow) {
            this.set_selectedRow(null);
            this.set_selectedValue(null);
            this.setSeries('');
            this.setDocsBlock('');

            if (!preventToRemoveSelectedRow) {
                this.hoveredRow = null;
            }

            for (var i = this.options.length - 1; i >= 0; i--) {
                var row = this.options[i];
                if (row.equals(preventToRemoveSelectedRow)) {
                    row.completedItem = true;
                } else {
                    row.remove();
                    this.options.splice(i, 1);
                }
            }

            this.options.forEach(function (op, i) {
                op.index = i;
            });
        },

        //set docsBlock text from response if text is not specified
        setDocsBlock: function (response, text) {
            const { classNames } = this;

            if (text) {
                this._searchResults.find(`.${classNames.searchResults}-info`).html('<pre>' + text + '</pre>').show();
                return true;
            }

            if (response.docs) {
                this._searchResults.find(`.${classNames.searchResults}-info`).html('<pre>' + response.docs + '</pre>').show();
                return true;
            } else {
                this._searchResults.find(`.${classNames.searchResults}-info`).html('').hide();
            }
        },

        //set series text from response if text is not specified
        setSeries: function (response, text) {
            const { classNames } = this;

            if (response.return_type) {
                this._searchResults.find(`.${classNames.searchResults}-hint-badge`).text(response.return_type).show();
                return true;
            } else {
                this._searchResults.find(`.${classNames.searchResults}-hint-badge`).html('').hide();
            }
        },

        //set intellisense with function info
        setFunctionInfo: function (response) {
            const { classNames } = this;

            var functionBlock = this._searchResults.find(`.${classNames.searchResults}-function`);
            var functionPre = this._searchResults.find(`.${classNames.searchResults}-function-pre span`);
            var functionName = this._searchResults.find(`.${classNames.searchResults}-function-name`);
            var functionHint = this._searchResults.find(`.${classNames.searchResults}-function-hint`);
            var showMoreBlock = this._searchResults.find(`.${classNames.searchResults}-function-show-more-label`);

            if (response.function) {
                functionName.text(response.function.name);

                // show or hide docs div (show if docs is not empty string)
                if (response.function.docs != "") {
                    functionPre.text(response.function.docs);
                    functionPre.parent().show(); // functionPre is span, but we need <pre> tag
                } else {
                    functionPre.parent().hide(); // // functionPre is span, but we need <pre> tag
                }

                // ----- hint text -------------------
                var hintText = "";
                hintText = response.function.name + "(";
                var paramString = response.function.params.map(function (p) {
                    var name = p.name;
                    if (p.default)
                        name += "=" + p.default;
                    if (p.active)
                        name = "<b>" + name + "</b>"; //#90949D
                    return name;
                }).join(', ');
                hintText += paramString + ")";
                functionHint.html(hintText);

                // ------ show or hide 'show more' link
                // docs contains line breaks
                if (response.function.docs && response.function.docs.indexOf('\n') >= 0)
                    showMoreBlock.show();
                else {
                    var docsSpanWidth = $(`.${classNames.searchResults}-function-pre span`).innerWidth();
                    var docsWidth = $(`.${classNames.searchResults}-function-pre`).innerWidth();
                    if (docsSpanWidth > docsWidth)
                        showMoreBlock.show();
                }

                functionBlock.show();
            } else {
                functionName.text('');
                functionPre.text('');
                functionHint.text('');
                functionBlock.hide();
                showMoreBlock.hide();
                // uncheck 'show more'
                this._searchResults.find(`.${classNames.searchResults}-function-show-more`).removeAttr('checked');
            }

            this.displayResultBlock('show');
        },

        //allows to select next row after DOWN button is clicked
        selectNextRow: function () {
            var hoveredRow = this.hoveredRow; //get_selectedRow();
            if (hoveredRow) {
                var currentIndex = hoveredRow.index;
                var nextRow;
                if (++currentIndex < this.options.length) {
                    nextRow = this.options[currentIndex];
                    //this.set_selectedRow(nextRow);
                } else if (this.options.length > 0) {
                    nextRow = this.options[0];
                }

                //selectedRow.unselect();
                //nextRow.select();
                hoveredRow.unhover();
                nextRow.hover(true, 'next');
            } else {
                if (this.options.length > 0) {
                    this.options[0].hover(true, 'next'); //select();
                }
            }
        },

        //allows to select next row after UP button is clicked
        selectPreviousRow: function () {
            var hoveredRow = this.hoveredRow;
            if (hoveredRow) {
                var currentIndex = hoveredRow.index;
                var nextRow;
                if (--currentIndex < this.options.length && currentIndex >= 0) {
                    nextRow = this.options[currentIndex];
                    //this.set_selectedRow(nextRow);
                } else if (this.options.length > 0) {
                    nextRow = this.options[this.options.length - 1];
                }

                hoveredRow.unhover();
                nextRow.hover(true, 'prev');
            } else {
                if (this.options.length > 0) {
                    this.options[this.options.length - 1].hover(true, 'prev');
                }
            }
        },

        destroy: function () {
            const { classNames } = this;

            if (this.domElement) {
                this.domElement.removeAttr('sj-ac-mode');
            }

            if (this._autocompleteWrapper && this._searchInput) {
                $(document).off('.sjautocomplete' + this.id);
                this._searchInput.off('.sjautocomplete').find(`.${classNames.searchInput}`).off('.sjautocomplete');

                var originalEls = this._searchInput.children().not(`.${classNames.acInputEnter},.${classNames.tips},.${classNames.acClear}`);
                this._autocompleteWrapper.replaceWith(originalEls);
                this._autocompleteWrapper = null;
            }
        }
    }
})();


SjRowResult.prototype = (function () {

    //being executed after click on row
    var rowClickedHandler = function (autocompleteInstance, sjResultRow) {
        if (!sjResultRow.equals(autocompleteInstance.get_selectedRow())) {
            var selectedValue = autocompleteInstance.get_selectedvalue();
            var inputValue = autocompleteInstance.buildSearchInputString(sjResultRow);
            /*
            var currentValue = autocompleteInstance.get_value();
            if (inputValue == currentValue) {
                return false;
            }
            */
            autocompleteInstance.set_selectedRow(sjResultRow);
            autocompleteInstance.set_value(inputValue);
            //autocompleteInstance.callMethodWithDelay(function () {
            autocompleteInstance.getDataAsync_afterRowClicked(autocompleteInstance);
            //}, 200);
            autocompleteInstance.set_focus();
        } /* else {
            return false;
        } */
    };

    return {
        constructor: SjRowResult,

        init: function () {
            var self = this;
            const { classNames } = this.autocompleteInstance;

            if (!(this.element instanceof $))
                this.element = $(this.element);

            if (this.option.complete_endings) {
                this.option.complete_endings.forEach(function (item, index) {
                    var ce = new SjCompleteEnding(this.autocompleteInstance, this.element.find(`.${classNames.resultRow}-complete-ending`)[index], this, item);
                    ce.index = index;
                    this.completeEndings.push(ce);
                }, this);
            }
            //row click handler
            $(this.element)
                .mousedown(function (event) {
                    console.log('preventDefault');
                    event.preventDefault();
                })
                .mouseenter(function (event) {
                    if (self.autocompleteInstance.hoveredRow) {
                        self.autocompleteInstance.hoveredRow.unhover();
                    }
                    self.hover();
                })
                .click(function (event) {
                    self.fireClickEvent();
                });

            //RIGHT and LEFT buttons. Moves focus between complete endings
            jqOn($(this.element), 'keydown', `.${classNames.resultRow}-complete-ending`, function (event) {
                //39 - right button
                if (event.keyCode == 39) {
                    event.stopPropagation();
                    self.selectNextCompleteEnding();
                }
                //37 - left button
                else if (event.keyCode == 37) {
                    event.stopPropagation();
                    self.selectPreviousCompleteEnding();
                }
            });
        },

        //public method to execute row click handler
        fireClickEvent: function () {
            return rowClickedHandler(this.autocompleteInstance, this);
        },

        //set current row as selected
        select: function () {
            const { classNames } = this.autocompleteInstance;

            if (this.autocompleteInstance.selectedRow != null && this.autocompleteInstance.selectedRow != this) {
                this.autocompleteInstance.selectedRow.unselect();
            }

            //if current selected row not equals to new selected row
            if (this.autocompleteInstance.selectedRow != this) {
                this.autocompleteInstance.selectedRow = this;
                this.element.addClass(`${classNames.resultRow}-selected`);
                this.autocompleteInstance.displayCompleteEnding();

                var value = this.option.value;
                this.autocompleteInstance.set_selectedValue(value);
                this.selected = true;
                //this.element.focus();

                var $scrollContainer = this.element.closest(`.${classNames.searchResults}`)[0];
                var scrollTop = $scrollContainer.scrollTop;
                var top = this.element.position().top;
                if (top + this.element[0].offsetHeight > scrollTop + $scrollContainer.offsetHeight) {
                    $scrollContainer.scrollTop = top;
                } else if (top < scrollTop) {
                    $scrollContainer.scrollTop = top;
                }
            }
        },

        //unselect current row
        unselect: function () {
            const { classNames } = this.autocompleteInstance;

            this.element.removeClass(`${classNames.resultRow}-selected`);
            this.autocompleteInstance.displayCompleteEnding();
            this.autocompleteInstance.selectedRow = null;
            this.autocompleteInstance.selectedValue = null;
            this.selected = false;
        },

        //make row hovered (not selected) after UP and DOWN buttons clicked
        hover: function (setFocus, direction) {
            const { classNames } = this.autocompleteInstance;

            this.element.addClass(`${classNames.resultRowHovered}`);
            this.autocompleteInstance.hoveredRow = this;

            if (setFocus === true) {
                //this.element.focus();

                var $scrollContainer = this.element.closest(`.${classNames.searchResults}`)[0];
                var scrollTop = $scrollContainer.scrollTop;
                var top = this.element.position().top;

                if (this.element[0].offsetTop + this.element[0].offsetHeight > scrollTop + $scrollContainer.offsetHeight) {
                    $scrollContainer.scrollTop = this.element[0].offsetTop;
                } else if (this.element[0].offsetTop < scrollTop) {
                    $scrollContainer.scrollTop = this.element[0].offsetTop;
                }
                /*
                } else if (top < scrollTop) {
                    $scrollContainer.scrollTop = top;
                }
                */
            }

            if (this.selectedCompleteEnding) {
                this.selectedCompleteEnding.unselect();
            }
            if (this.completeEndings.length > 0) {
                this.completeEndings[0].select(setFocus);
            }
        },

        unhover: function () {
            const { classNames } = this.autocompleteInstance;

            this.element.removeClass(`${classNames.resultRowHovered}`);
            if (this.selectedCompleteEnding) {
                this.selectedCompleteEnding.unselect();
                this.selectedCompleteEnding = null;
            }
        },

        //method to compare row by their values
        equals: function (sjResultRow) {
            try {
                return this.option.value == sjResultRow.option.value;
            } catch (exception) {
                return false;
            }
        },


        remove: function () {
            $(this.element).remove();
        },

        //allow to select next complete ending in row after RIGHT button is clicked
        selectNextCompleteEnding: function () {
            if (this.selectedCompleteEnding == null) {
                if (this.completeEndings.length > 0)
                    this.completeEndings[0].select();
            } else {
                var selectedIndex = this.selectedCompleteEnding.index;
                this.selectedCompleteEnding.unselect();
                if (++selectedIndex < this.completeEndings.length) {
                    this.completeEndings[selectedIndex].select();
                } else {
                    this.completeEndings[0].select();
                }
                //this.selectedCompleteEnding.select();
            }
        },

        //allow to select next complete ending in row after LEFT button is clicked
        selectPreviousCompleteEnding: function () {
            if (this.selectedCompleteEnding == null) {
                if (this.completeEndings.length > 0)
                    this.completeEndings[0].select();
            } else {
                var selectedIndex = this.selectedCompleteEnding.index;
                this.selectedCompleteEnding.unselect();
                if (--selectedIndex < this.completeEndings.length && selectedIndex >= 0) {
                    this.selectedCompleteEnding = this.completeEndings[selectedIndex];
                } else {
                    this.selectedCompleteEnding = this.completeEndings[this.completeEndings.length - 1];
                }
                this.selectedCompleteEnding.select();
            }
        }
    }
})();

SjCompleteEnding.prototype = (function () {
    var completeEndingClickedHandler = function (autocompleteInstance, sjResultRow, completeEndingElement) {
        var selectedCompleteEnding = $(completeEndingElement).attr('value');
        var oldInputValue = autocompleteInstance.get_value(true);

        //removes option.value + complete_ending if new value not selected
        ['<=', '>=', '=', ':', '>', '<', '\\'].forEach(function (item) {
            if (endsWith(oldInputValue, sjResultRow.option.value + item)) {
                oldInputValue = oldInputValue.substring(0, oldInputValue.length - item.length);
            }
        });

        if (sjResultRow.selected == false) {
            sjResultRow.select();

            if (sjResultRow.option.complete_del_chars) {
                oldInputValue = oldInputValue.substring(0, oldInputValue.length - sjResultRow.option.complete_del_chars);
            }

            if (!sjResultRow.completedItem) {
                oldInputValue = oldInputValue + sjResultRow.option.complete;
            }
        }
        if (!selectedCompleteEnding) {
            selectedCompleteEnding = ' ';
        }

        autocompleteInstance.set_value(oldInputValue + selectedCompleteEnding, false);
        autocompleteInstance.callMethodWithDelay(function () {
            autocompleteInstance.getDataAsync_afterCompleteEndingClicked(autocompleteInstance);
        }, 200);
        autocompleteInstance.set_focus();
    };

    var completeEndingClickedHandler_TabClicked = function (autocompleteInstance, sjResultRow, completeEndingElement) {
        completeEndingClickedHandler(autocompleteInstance, sjResultRow, completeEndingElement);
    };

    return {
        constructor: SjCompleteEnding,

        init: function () {
            if (!(this.element instanceof $))
                this.element = $(this.element);

            var self = this;
            this.element
                .mousedown(function (event) {
                    console.log('preventDefault');
                    event.preventDefault();
                })
                .mouseenter(function (event) {
                    self.select();
                })
                .click(function (event) {
                    self.fireClickEvent();
                    //completeEndingClickedHandler(self.row.autocompleteInstance, self.row, self);
                });
        },

        select: function (scroll) {
            const { classNames } = this.autocompleteInstance;

            if (this.row.selectedCompleteEnding != null) {
                this.row.selectedCompleteEnding.unselect();
            }
            this.row.selectedCompleteEnding = this;
            this.selected = true;
            this.element.addClass(`${classNames.resultRow}-complete-ending-selected`);
            //this.element.focus();

            if (scroll) {
                //this.element.closest('.${classNames.searchResults}')[0].scrollTop = this.element.position().top;
                var $scrollContainer = this.element.closest(`.${classNames.searchResults}`)[0];
                var scrollTop = $scrollContainer.scrollTop;
                var top = this.element.position().top + scrollTop;
                if (top + this.element[0].offsetHeight > scrollTop + $scrollContainer.offsetHeight) {
                    $scrollContainer.scrollTop = top;
                } else if (top < scrollTop) {
                    $scrollContainer.scrollTop = top;
                }
            }
        },

        unselect: function () {
            const { classNames } = this.autocompleteInstance;

            this.selected = false;
            this.element.removeClass(`${classNames.resultRow}-complete-ending-selected`);
        },

        fireClickEvent: function () {
            completeEndingClickedHandler(this.row.autocompleteInstance, this.row, this);
        },

        fireTabClickEvent: function () {
            completeEndingClickedHandler_TabClicked(this.row.autocompleteInstance, this.row, this);
        }
    }
})();

const SJ_Search_Input_Html = function (classNames: any) {

    var acImg = `<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 352 512" aria-label="Clear">
    <path d="M242.72 256l100.07-100.07c12.28-12.28 12.28-32.19 0-44.48l-22.24-22.24c-12.28-12.28-32.19-12.28-44.48 0L176 189.28 75.93 89.21c-12.28-12.28-32.19-12.28-44.48 0L9.21 111.45c-12.28 12.28-12.28 32.19 0 44.48L109.28 256 9.21 356.07c-12.28 12.28-12.28 32.19 0 44.48l22.24 22.24c12.28 12.28 32.2 12.28 44.48 0L176 322.72l100.07 100.07c12.28 12.28 32.2 12.28 44.48 0l22.24-22.24c12.28-12.28 12.28-32.19 0-44.48L242.72 256z"></path>
</svg>
<span style="display:none">Clear</span>`;
    var $acEl = $(classNames.acClearEl).addClass(`${classNames.acClear} ${classNames.acClear}-hidden`).html(acImg);
    var acElhtml = $acEl.prop ? $acEl.prop("outerHTML") : $acEl[0].outerHTML; // for jQuery <1.6

    return `<div class="${classNames.searchInput}-wrapper">
            <input class="${classNames.searchInput} inputClass" />
            <span class="${classNames.acInputEnter} ${classNames.acInputEnter}-hidden">
                Press Enter to run Query
            </span>
            ${acElhtml}
        </div>`;
};

var SJ_Search_Results_Html = function (classNames: any) {
    return `<div class='${classNames.searchResultsWrapper}' id='${classNames.searchResultsWrapperId}' style='display:none;'>` +
        `   <div class='${classNames.acLoading}-inner'></div>` +
        `   <div class='${classNames.searchResults}-hint ${classNames.resultBlock}' style='display:none;'>` +
        `       <div class='${classNames.searchResults}-hint-badgeBlock'>` +
        `           <div class='${classNames.searchResults}-hint-badge' style='background-color: #D7F0F5; color: #2FC3E4'>Series</div>` +
        "       </div>" +
        `           <div class='${classNames.searchResults}-hint-text'></div>` +
        "   </div>" +

        //"<!-- function info -->"
        `   <div class='${classNames.searchResults}-function ${classNames.resultBlock}' style='display:none;'>` +
        "       <div>" +
        `           <div class='${classNames.searchResults}-function-name'></div>` +
        `           <div class='${classNames.searchResults}-function-hint'></div>` +
        `               <div class='${classNames.searchResults}-function-morewrapper'>` +
        `                   <input class='${classNames.searchResults}-function-show-more' id='ch' type='checkbox'>` +
        `                   <label class='${classNames.searchResults}-function-show-more-label' for='ch' style='display:none;'>Show More</label>` +
        `                   <pre class='${classNames.searchResults}-function-pre'><span></span></pre>` +
        "               </div>" +
        "           </div>" +
        "       </div>" +
        // "<!-- info text-->" +
        `   <div class='${classNames.searchResults}-info ${classNames.resultBlock}' style='display:none;'></div>` +

        //"<!-- results-->" +
        `   <div class='${classNames.searchResults} ${classNames.resultBlock}'>` +
        `       <div class='${classNames.searchResults}-currentRow'></div>` +
        `       <div class='${classNames.searchResults}-rows'></div>` +
        "   </div>" +
        "</div>"
},
    SJ_Autocomplete_Wrapper = function (classNames: any) {
        return `<div class='${classNames.autocompleteWrapper}' ></div>`;
    },
    SJ_Result_Value_Icon = function (classNames: any) {
        return `<svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" class="${classNames.resultValueIcon}"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M7 21h10a2 2 0 002-2V9.414a1 1 0 00-.293-.707l-5.414-5.414A1 1 0 0012.586 3H7a2 2 0 00-2 2v14a2 2 0 002 2z"></path></svg>`;
    }

export default SjAutoComplete;

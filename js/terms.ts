import { endsWith, getFieldMode, shortNumber, $ } from './helpers';
import Annotations from './annotations';

export default class Terms {

  mode: 'num' | 'date' | 'set' | 'text' | 'tree';
  state: any;
  $el: any;
  term: any;
  fieldsToHideMap: any;
  annotationValues: any = {};
  recommended: string[];
  showMore: boolean = false;

  minFacets = 200;
  maxFacets = 800;

  id: number;

  constructor(protected settings, state) {
    var field = this.settings.field;
    this.mode = getFieldMode(field);

    this.initState(state);
  }

  initState(state) {
    if (state) {
      this.state = state;
      return;
    }
    switch (this.mode) {
      case 'num':
        this.state = {
          op: '>',
          val: ''
        };
        break;
      case 'date':
        this.state = {
          style: 'relative',

          rel_sign: '+',
          rel_op: '<',
          rel_val: '',
          rel_per: 'd',

          abs_per: 'd',
          selected: {}
        };
        break;
      case 'text':
        this.state = {
          selected: {},
          fuzzy: false,
          val: ''
        };
        break;
      case 'tree':
        this.state = {
          selected: {},
          val: '',
          exact: false // true - `=`, false - `:`
        };
        break;
      case 'set':
        this.state = {
          selected: {},
          fuzzy: false,
          val: ''
        };
        break;
    }
  }

  setPreferences(annotations, recommended, fieldsToHide) {
    this.recommended = recommended;
    this.fieldsToHideMap = {};
    fieldsToHide.forEach(f => {
      this.fieldsToHideMap[f] = 1;
    });

    var field = this.settings.field;

    if (annotations && Object.keys(annotations).length) {
      if (this.mode == 'set') {
        this.annotationValues = annotations;
      } else {
        if (annotations[field]) {
          this.annotationValues = annotations[field].values || {};
        }
      }
    }
  }

  formatFacet(facetFilter, partialSearch = false) {
    var facet = this.settings.field || 'set';

    var facetFilter;
    switch (this.mode) {
      case 'date':
        facet += '@period:' + this.state.abs_per + '@sort:term.desc';
        break;
      case 'tree':
        facetFilter = facetFilter || '';
        if (facetFilter && partialSearch) {
          facetFilter = `${facetFilter}*`;
        }
        facet += `@${partialSearch ? 'filter' : 'tree'}:${facetFilter}`;
        if (!partialSearch) {
          facet += '@sort:term.asc';
        }
        break;
      default:
        if (facetFilter) {
          var searchTemplate = `${facetFilter}*`;
          if (this.settings.field) {
            searchTemplate = '*' + searchTemplate;
          }
          facet += `@filter:${searchTemplate}`;
        }
        facet += '@sort';
        break;
    }
    return facet;
  }

  loadChildren(item, $li) {
    this.settings.loadChildren(item, $li);
  }

  loadFacets() {
    // pass `loadFacets` callback in settings
    /*
    var facetFilter = this.$el.find('input').val();
    this.renderTerms(facetFilter);
    */
    this.settings.loadFacets();
  }

  clearSearch() {
    var $el = this.$el.find('.sj-query-bar-filter-search-remove');
    $el.removeClass('sj-query-bar-filter-search-shown');
    var $input = $el.prev('input');
    $input.val('').trigger('input').trigger('focus');
    this.loadFacets();
  }

  initHandlers() {
    // filter search
    this.$el.on('input', 'input[type="text"]', e => {
      var $input = $(e.currentTarget);
      var $removeIcon = this.$el.find('.sj-query-bar-filter-search-remove');
      if ($input.val()) {
        $removeIcon.addClass('sj-query-bar-filter-search-shown');
      } else {
        $removeIcon.removeClass('sj-query-bar-filter-search-shown');
      }
      this.showMore = false;
      this.loadFacets();
    });

    // clear search
    this.$el.on('click', '.sj-query-bar-filter-search-remove', e => {
      this.showMore = false;
      this.clearSearch();
    });

    // toggle term
    this.$el.on('click', '.sj-query-bar-filter-item label', e => {
      this.toggleTerm(e);
      this.refreshAllState();
    });

    // toggle all values
    this.$el.on('click', '.sj-query-bar-filter-header-all label', e => {
      var $el = $(e.currentTarget);

      var $parent = $el.closest('.sj-query-bar-filter-header-all');
      if ($parent.hasClass('sj-query-bar-filter-header-all-disabled')) {
        return;
      }

      this.selectionChanged($el);
    });

    // replace with set:xxx
    this.$el.on('click', '.sj-query-bar-filter-tip > strong', e => {
      this.settings.onFilterChange(this.settings.field, false, true);
    });

    this.$el.on('click', '.sj-terms-show-more', e => {
      var path = $(e.currentTarget).data('path');
      if (path) {
        var $li = $(e.currentTarget).closest('.sj-query-bar-filter-item');
        var item = $li.data('node');
        item.showMore = true;
        item.children = null;
        this.loadChildren(item, $li);
      } else {
        this.showMore = true;
        this.loadFacets();
      }
    });
  }

  selectionChanged($el) {
    if (this.term && this.term.terms) {
      var checked = $el.attr('data-checked') == 'true';
      var selected = this.state.selected;
      if (!checked) {
        this.term.terms.forEach(t => {
          selected[t.term] = true;
        });
      } else {
        this.term.terms.forEach(t => {
          delete selected[t.term];
        });
      }
    }

    this.renderTerms();
    this.settings.onStateChange(this.settings.field, 'toggleall');
  }

  getSelectedLower() {
    var res = {};
    var s = this.state.selected || {};
    Object.keys(s).forEach(k => {
      res[k.toLowerCase()] = s[k];
    });
    return res;
  }

  refreshAllState() {
    var field = this.settings.field;
    if (field) {
      var $allLabel = this.$el.find('.sj-query-bar-filter-header-all label');

      var allChecked = true;
      var anyChecked = false;
      var selected = this.getSelectedLower();

      this.term.terms.forEach(t => {
        var term = (t.term || '') + ''; // convert to string
        if (selected[term.toLowerCase()]) {
          anyChecked = true;
        } else {
          allChecked = false;
        }
      });

      var $tip = this.$el.find('.sj-query-bar-filter-tip');
      if (allChecked && !this.state.val) {
        $tip.addClass('sj-query-bar-filter-tip-active');
      } else {
        $tip.removeClass('sj-query-bar-filter-tip-active');
      }

      var checkedAttr: any = false;
      if (allChecked) {
        checkedAttr = true;
      } else if (anyChecked) {
        checkedAttr = 'partial';
      }

      $allLabel.attr('data-checked', checkedAttr);
    }
  }

  destroy() {
    this.settings.$root.empty();
  }

  render() {
    this.$el = $(`<div class="sj-query-bar-filter-list">
        <div class="sj-query-bar-filter-list-inner">
            <ul data-mode="${this.mode}">
              <li class="sj-query-bar-filter-search">
                <div class="sj-query-bar-flex">
                  <div>
                      <input placeholder="Find field..." type="text"/>
                      <span class="sj-query-bar-filter-search-remove">
                          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 352 512">
                              <path d="M242.72 256l100.07-100.07c12.28-12.28 12.28-32.19 0-44.48l-22.24-22.24c-12.28-12.28-32.19-12.28-44.48 0L176 189.28 75.93 89.21c-12.28-12.28-32.19-12.28-44.48 0L9.21 111.45c-12.28 12.28-12.28 32.19 0 44.48L109.28 256 9.21 356.07c-12.28 12.28-12.28 32.19 0 44.48l22.24 22.24c12.28 12.28 32.2 12.28 44.48 0L176 322.72l100.07 100.07c12.28 12.28 32.2 12.28 44.48 0l22.24-22.24c12.28-12.28 12.28-32.19 0-44.48L242.72 256z"/>
                          </svg>
                      </span>
                  </div>
                </div>
              </li>
              <li class="sj-query-bar-filter-loading">Loading...</li>
              <li class="sj-query-bar-filter-item-empty">
                  <svg width="17" height="16" viewBox="0 0 17 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                      <circle cx="7.5" cy="7" r="6" stroke="#D0D4D8" stroke-width="2"/>
                      <path d="M15.5 15L12 11.5" stroke="#D0D4D8" stroke-width="2" stroke-linecap="round"/>
                  </svg>
                  <span>No Fields</span>
              </li>
            </ul>
        </div>
    </div>`);
    this.settings.$root.append(this.$el);

    this.initHandlers();
  }

  toggleTerm(e) {
    var $el = $(e.currentTarget);
    var $item = $el.closest('.sj-query-bar-filter-item');

    if ($item.hasClass('sj-query-bar-filter-item-disabled')) {
      return;
    }

    var term = $item.attr('data-term');
    var checked = $el.attr('data-checked') || 'false';

    var treeNode;
    if (this.mode == 'tree') {
      treeNode = $item.data('node');
      term = this.buildTreeItemPath(treeNode);
    }

    switch (checked) {
      case 'true':
        checked = 'false';
        break;
      case 'false':
        checked = 'true';
        break;
      case 'partial':
        checked = 'true';
        break;
    }

    switch (checked) {
      case 'true':
        if (this.mode == 'tree') {
          this.markTreeChecked(treeNode);
        } else {
          this.state.selected[term] = true;
        }
        break;
      case 'false':
        if (this.mode == 'tree') {
          this.markTreeUnchecked(treeNode);
        } else {
          delete this.state.selected[term];
        }
        break;
      case 'partial':
        break;
    }

    $el.attr('data-checked', checked);

    if (this.mode == 'tree') {
      this.refreshTreeChecked();
    }

    if (this.settings.field) {
      this.settings.onStateChange(this.settings.field, 'toggle');
    } else {
      this.settings.onFilterChange(term, checked);
    }

    if (checked == "true" && this.$el.find('.sj-query-bar-filter-search input[type=text]').val()) {
      this.clearSearch();
    }
  }

  refreshTreeChecked() {
    var $items = this.$el.find('.sj-query-bar-filter-item');
    $items.each((i, itemEl) => {
      var $item = $(itemEl);
      var item = $item.data('node');
      var checked;
      var isChecked = this.isTreeChecked(item);
      switch (isChecked) {
        case true:
          checked = 'true';
          break;
        case false:
          checked = 'false';
          break;
        case null:
          checked = 'partial';
          break;
      }
      $item.find('> div > label').attr('data-checked', checked);
    });
  }

  markTreeChecked(item) {
    // remove children items
    var fullName = this.buildTreeItemPath(item);
    var fPrefix = fullName + '\\';

    if (this.state.selected) {
      for (let key in this.state.selected) {
        if (key.indexOf(fPrefix) != -1) {
          delete this.state.selected[key];
        }
      }
    }

    if (this.state.selected[fullName] === false) {
      delete this.state.selected[fullName];
      return;
    } else {
      this.state.selected[fullName] = true;
    }

    var nameParts = fullName.split('\\');
    nameParts.pop();

    // find items and prefix for all parents
    var folderItems = [];
    var prefixes: string[] = [];
    var terms = this.term.terms;
    var currentPath;

    for (let p of nameParts) {
      currentPath = currentPath ? `${currentPath}\\${p}` : `${p}`;
      prefixes.push(currentPath);
      if (terms) {
        var fTerms = terms.filter(x => x.term == p)[0];
        if (fTerms) {
          terms = fTerms.children;
          if (terms) {
            folderItems.push(terms);
          }
        }
      }
    }

    folderItems.reverse();
    prefixes.reverse();

    // iterate through all parents
    for (var fi = 0; fi < folderItems.length; fi++) {
      let fitems = folderItems[fi];
      let prefix = prefixes[fi];
      var allChecked = true;
      fitems.forEach(item => {
        if (!this.isTreeChecked(item)) {
          allChecked = false;
        }
      });

      if (allChecked) { //remove selected children and select
        fitems.forEach(item => {
          if (this.state.selected) {
            delete this.state.selected[this.formatTreeName(prefix, item)];
          }
        });
        this.state.selected[prefix] = true;
      } else {
        break;
      }
    }
  }

  markTreeUnchecked(item) {
    var prefix = item.prefix;
    var fullName = this.buildTreeItemPath(item);

    // exact name match
    if (this.state.selected && this.state.selected[fullName]) {
      delete this.state.selected[fullName];
    } else {

      var parts = fullName.split('\\');
      var parentNames = [];
      var prevName;
      for (let part of parts) {
        if (prevName) {
          prevName = prevName + '\\' + part;
        } else {
          prevName = part;
        }
        parentNames.push(prevName);
      }

      parentNames.pop(); // remove first name (and leave only parent folders)
      parentNames.reverse();

      // uncheck parents and check all other children
      var found = false;

      // iterate from current item to the root
      var level = 0;
      for (let parentName of parentNames) {
        var navPath = parentName.split('\\');

        // find folder items
        var terms = this.term.terms;
        for (var p of navPath) {
          if (terms) {
            var matchedTerm = terms.filter(x => x.term == p);
            if (matchedTerm && matchedTerm.length) {
              terms = matchedTerm[0].children;
            } else {
              terms = null;
            }
          }
        }

        if (terms) {
          terms.forEach(t => {
            // go through all children
            if (t.term !== item.term) { // ignore unchecked item
              var n = `${parentName}\\${t.term}`;
              if (parentNames.indexOf(n) == -1) { // ignore all folders
                // if item already unchecked through search then just remove key
                if (this.state.selected[n] === false) {
                  delete this.state.selected[n];
                } else {
                  // otherwise set them checked
                  this.state.selected[n] = true;
                }
              }
            }
          });
        } else if (level === 0 && !terms) {
          // if children terms not found then this is search (using search icon) and we should uncheck items using `false` value
          this.state.selected[fullName] = false;
          break;
        }

        // if matched parent is selected then we unselect it and break
        if (this.state.selected && this.state.selected[parentName]) {
          delete this.state.selected[parentName];
          break;
        }

        level++;
      }
    }
  }

  setTerm(term) {
    this.term = term;
  }

  renderTerms(facetFilter = null) {
    var field = this.settings.field;
    var $dropdown = this.$el;
    var $ul = $dropdown.find('ul');

    $dropdown.find('.sj-query-bar-filter-tip,.sj-query-bar-filter-item,.sj-query-bar-filter-header,.sj-query-bar-filter-other').remove();

    var hasResults = false;
    var lastUsedIgnore = {};

    var allFacetsMap = {};
    if (this.term && this.term.terms) {
      this.term.terms.forEach(t => {
        allFacetsMap[t.term] = t;
      });
    }

    var filter = field => {
      if (this.fieldsToHideMap[field]) {
        return false;
      }
      if (this.state.selected[field]) {
        return false;
      }
      if (!allFacetsMap[field] && ['sid', 'set'].indexOf(field) == -1 && field.indexOf('meta.') == -1) {
        return false;
      }
      if (facetFilter) {
        var matched = (field || '').toLowerCase().indexOf(facetFilter.toLowerCase()) != -1;
        if (!matched) {
          var annotation = this.getAnnotationTerm(field);
          if (annotation && annotation.toLowerCase().indexOf(facetFilter.toLowerCase()) != -1) {
            return true;
          }
        }
        return matched;
      }
      return true;
    };

    if (this.mode == 'set' /* || this.mode == 'text' || (this.mode == 'date' && this.state.style == 'absolute') */) {

      var selected = Object.keys(this.state.selected);
      if (selected.length) {
        hasResults = true;
        selected.sort();
        $ul.append(`<li class='sj-query-bar-filter-header'>Selected</li>`);
        selected.forEach(term => {
          var item = allFacetsMap[term];
          if (!item) {
            item = { term: term };
          }
          var $li = this.renderTermLi(item, this.state.selected[term]);
          $ul.append($li);
        });
      }

      if (this.mode == 'set') {

        var alreadyAdded = {};

        var recommendedFields = [];
        if (this.recommended && this.recommended.length) {
          this.recommended.forEach(field => {
            if (!alreadyAdded[field]) {
              recommendedFields.push(field);
              alreadyAdded[field] = true;
            }
          });
        }
        var lastUsed = this.settings.getLastUsedArr();
        if (lastUsed && lastUsed.length) {
          lastUsed.forEach(field => {
            if (!alreadyAdded[field]) {
              recommendedFields.push(field);
              alreadyAdded[field] = true;
            }
          });
        }

        var mainFields = ['set', 'sid'];

        // show top 10 for recommended + last used
        recommendedFields = recommendedFields.filter(filter)
          .sort((a, b) => {
            // sort by count
            var ac = (allFacetsMap[a] ? allFacetsMap[a].count : 0) || 0;
            var bc = (allFacetsMap[b] ? allFacetsMap[b].count : 0) || 0;
            if (ac === bc) {
              return a > b ? 1 : -1;
            } else {
              return ac > bc ? -1 : 1;
            }
          })
          .slice(0, 10);

        mainFields.filter(filter).forEach(field => {
          if (!alreadyAdded[field]) {
            recommendedFields.unshift(field);
            alreadyAdded[field] = true;
          }
        });

        if (recommendedFields.length) {
          hasResults = true;
          $ul.append(`<li class='sj-query-bar-filter-header'>Recommended Fields</li>`);
          recommendedFields.forEach(x => {
            lastUsedIgnore[x] = true;
            var item = allFacetsMap[x];
            if (!item) {
              item = { term: x };
            }
            var $li = this.renderTermLi(item, this.state.selected[x]);
            $ul.append($li);
          });
        }
      }

    }

    if (this.term) {
      var termsToRender = this.term.terms.filter(term => {
        if (this.mode == 'set' && term.term.indexOf && term.term.indexOf('meta.') == 0) {
          return;
        }
        if (this.mode != 'tree' && this.state.selected[term.term]) {
          return;
        }
        if (lastUsedIgnore[term.term]) {
          return;
        }
        return true;
      });

      if (this.mode != 'set' && this.mode != 'tree') {
        var selectedTerms = Object.keys(this.state.selected);
        selectedTerms.sort();
        selectedTerms.reverse();
        selectedTerms.forEach(t => {
          termsToRender.unshift({ term: t });
        });
      }

      if (termsToRender.length) {
        if (field) {
          if (termsToRender.length > 1) {

            if (field && field != 'set' && !this.state.val) {
              $ul.prepend(`<li class="sj-query-bar-filter-tip">
                <span>
                  ${termsToRender.length} visible values selected.
                </span>
                <strong>
                  Replace with "Fields Set" to select all.
                </strong>

                <i>
                  <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M8.00025 2C4.68751 2 2 4.68412 2 7.99708C2 11.3132 4.68751 14.0005 8.00022 14.0005C11.3148 14.0005 14 11.3132 14 7.99708C14 4.68412 11.3148 2 8.00025 2ZM8.42783 11.3358C8.28847 11.4598 8.12696 11.5221 7.94401 11.5221C7.75471 11.5221 7.5896 11.4609 7.44865 11.3383C7.30749 11.2158 7.23677 11.0444 7.23677 10.8241C7.23677 10.6288 7.30521 10.4643 7.44165 10.331C7.57809 10.1978 7.74546 10.1311 7.94401 10.1311C8.13939 10.1311 8.30384 10.1978 8.43733 10.331C8.5706 10.4643 8.63746 10.6287 8.63746 10.8241C8.63721 11.0412 8.56741 11.2118 8.42783 11.3358ZM10.1655 7.02783C10.0584 7.22638 9.93128 7.39758 9.78378 7.54194C9.63672 7.68627 9.37224 7.92886 8.99048 8.26993C8.88522 8.36616 8.80052 8.45064 8.73705 8.52336C8.67359 8.59633 8.62615 8.66294 8.5952 8.72349C8.56402 8.78402 8.54009 8.84457 8.52314 8.90509C8.50619 8.96539 8.48068 9.07179 8.44611 9.22379C8.3874 9.54636 8.20285 9.70761 7.89271 9.70761C7.73143 9.70761 7.5959 9.65498 7.48545 9.5495C7.37543 9.44401 7.32055 9.28748 7.32055 9.07968C7.32055 8.81925 7.36098 8.59358 7.44162 8.40273C7.52182 8.21186 7.62911 8.04449 7.76238 7.90016C7.89588 7.75583 8.07567 7.58459 8.30222 7.38605C8.50077 7.21234 8.64421 7.08135 8.73252 6.99301C8.82106 6.90448 8.89539 6.806 8.95569 6.69757C9.01646 6.58891 9.04628 6.47123 9.04628 6.34407C9.04628 6.09584 8.95436 5.88668 8.76959 5.71614C8.58505 5.54561 8.34696 5.46021 8.05535 5.46021C7.71406 5.46021 7.46288 5.54627 7.3016 5.71839C7.14032 5.89051 7.00413 6.14394 6.89231 6.47893C6.7866 6.82951 6.58647 7.00477 6.29214 7.00477C6.11843 7.00477 5.97185 6.94355 5.85236 6.82111C5.73309 6.69868 5.67346 6.5661 5.67346 6.42335C5.67346 6.1288 5.76811 5.83019 5.95718 5.52774C6.14647 5.22529 6.42249 4.97478 6.78546 4.77646C7.14821 4.57791 7.57176 4.47851 8.05535 4.47851C8.50508 4.47851 8.90195 4.56163 9.24619 4.72766C9.59043 4.89345 9.8565 5.11911 10.0442 5.40461C10.2317 5.68988 10.3256 6.00003 10.3256 6.33501C10.3261 6.5982 10.2726 6.82928 10.1655 7.02783Z" fill="#D0D4D8"/>
                  </svg>
                  <div>
                    <div>Instead of selecting all values of a field, you can</div>
                    <div>use <strong>Fields Set</strong> to show fields that have values.</div>
                    <div>Click to replace current field with <strong>Fields Set: ${field}</strong></div>
                  </div>
                </i>
                
              </li>`);
            }

            $ul.append(`<li class='sj-query-bar-filter-header sj-query-bar-filter-header-all ${this.state.not ? "sj-query-bar-filter-header-all-disabled" : ""}'>
            <label data-checked="">
              <i>
                <span>
                  <svg width="14" height="10" fill="none" xmlns="http://www.w3.org/2000/svg">
                      <path data-checked="true" d="M1 5L5 9L13 1" stroke="white" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path>
                      <path data-checked="partial" d="M3 5H11" stroke="white" stroke-width="2" stroke-linecap="round"></path>
                  </svg>
                </span>
              </i>
              <span>
                All ${termsToRender.length} values
              </span>
            </label>
          </li>`);
          }
        } else {
          $ul.append(`<li class='sj-query-bar-filter-header'>Other Fields</li>`);
        }
        termsToRender.forEach(term => {
          hasResults = true;
          var isSelected;
          if (this.mode == 'tree') {
            isSelected = this.isTreeChecked(term);
          } else {
            isSelected = this.state.selected[term.term];
          }
          var $li = this.renderTermLi(term, isSelected);
          $ul.append($li);
        });

        this.refreshAllState();
      }

      if (this.term.other) {
        $ul.append(`<li class='sj-query-bar-filter-other'>
                only showing top ${this.showMore ? this.maxFacets : this.minFacets} ${field ? "values" : "fields"} - ${this.showMore ? 'search to see more' : '<span class="sj-terms-show-more">show more</span>'}
            </li>`);
      }
    }

    if (this.mode == 'set') {
      var metaFieldsMap = {};
      var metaFields;
      if (this.term && this.term.terms) {
        metaFields = this.term.terms.filter(term => {
          if (term.term.indexOf && term.term.indexOf('meta.') == 0) {
            metaFieldsMap[term.term] = 1;
            return true;
          }
        });
      } else {
        metaFields = [];
      }

      this.getMetaFields().filter(filter).forEach(f => {
        if (!metaFieldsMap[f]) {
          metaFields.push({ term: f });
        }
      });

      if (metaFields.length) {
        $ul.append(`<li class='sj-query-bar-filter-header'>Meta Fields</li>`);
        metaFields.forEach(term => {
          hasResults = true;
          var $li = this.renderTermLi(term, this.state.selected[term]);
          $ul.append($li);
        });
      }
    }

    return hasResults;
  }

  getMetaFields() {
    if (window['SJSearch'] && window['SJSearch'].metaFields) {
      return window['SJSearch'].metaFields;
    }
    return [
      'meta.dates_max',
      'meta.dates_min',
      'meta.freq',
      'meta.levels',
      'meta.points_count',
      'meta.points_max',
      'meta.points_min',
      'meta.processor',
      'meta.updated_at'
    ];
  }

  renderTermLi(item, selected) {
    var disabled = item.disabled;
    var term = item.term;

    var $span = $('<span>');
    var $spanW = $('<span class="sj-query-bar-field-label-inner">');

    var annotationTerm = this.getAnnotationTerm(term);
    var disabledLabel;
    if (annotationTerm) {
      $span.addClass('sj-query-bar-field-label');
      disabledLabel = disabled ? this.getAnnotationDisabled(term) : '';
      $spanW.text(annotationTerm);
      var $tipSpan = $('<span>').text(disabledLabel || term);
      var $tipWrap = $('<span class="sj-query-bar-field-tooltip">').append($tipSpan);
      if (item['tipClass']) {
        $tipWrap.addClass(item['tipClass']);
      }
      $span.append($tipWrap);
    } else {
      $spanW.text(term);
    }
    $span.append($spanW);

    var $label = $('<label>')
      .addClass('sj-query-bar-flex')
      .append($('<i>').append(`<span>
            <svg width="14" height="10" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path data-checked="true" d="M1 5L5 9L13 1" stroke="white" stroke-width="2" 
                    stroke-linecap="round" stroke-linejoin="round"/>
                <path data-checked="partial" d="M3 5H11" stroke="white" stroke-width="2" stroke-linecap="round"/>
            </svg>
        </span>`))
      .append($span);
    if (item.count) {
      $label.append($('<strong class="sj-query-bar-count">').text(shortNumber(item.count)));
    }

    var $div = $('<div>');
    var $li = $('<li>')
      .addClass('sj-query-bar-filter-item')
      .append($div)
      .attr('data-term', term);

    $div.append($label);

    if (selected) {
      $label.attr('data-checked', 'true');
    } else if (selected === null) {
      $label.attr('data-checked', 'partial');
    }

    if (disabled) {
      $li.addClass('sj-query-bar-filter-item-disabled');
      if (disabledLabel) {
        $li.addClass('sj-query-bar-filter-item-disabled-label');
      }
    }
    if (item['class']) {
      $li.addClass(item['class']);
    }

    if (this.mode == 'tree') {
      $li.data('node', item);
      if (item.more) {
        var $arrow = $(`<span class='sj-query-bar-filter-tree-arrow'></span>`);
        var uniqId = new Date().getTime();
        $arrow.append(`
          <svg class="spinner" width="12px" height="12px" viewBox="0 0 18 18" xmlns="http://www.w3.org/2000/svg">
            <circle class="path" fill="transparent" stroke-width="2" cx="9" cy="9" r="8" stroke="url(#sjac-tree-spin${uniqId})"></circle>
            <radialGradient id="sjac-tree-spin${uniqId}" cx="0.15" cy="0.15" r="0.7">  
                <stop offset="0%" stop-color="#567483" stop-opacity="0"></stop>
                <stop offset="25%" stop-color="#567483" stop-opacity="0"></stop>
                <stop offset="85%" stop-color="#567483" stop-opacity="0.5"></stop>
                <stop offset="100%" stop-color="#567483" stop-opacity="1"></stop>
            </radialGradient>
          </svg>
        `);
        if (item.expanded) {
          $li.addClass('sj-query-bar-filter-item-expanded');
        }
        $arrow.insertBefore($label);

        if (item.children) {
          var $ul = $('<ul>');

          var prefix = this.buildTreeItemPath(item);
          item.children.forEach(ch => {
            var isChecked = this.isTreeChecked(ch);
            var $treeLi = this.renderTermLi(ch, isChecked);
            $ul.append($treeLi);
          });

          if (item.other) {
            var $liOther = $(`<li class='sj-query-bar-filter-other'>
              only showing top ${item.showMore ? this.maxFacets : this.minFacets} values - ${item.showMore ? 'search to see more' : '<span class="sj-terms-show-more">show more</span>'}
            </li>`);
            $ul.append($liOther);
            $liOther.find('.sj-terms-show-more').attr('data-path', prefix);
          }

          $li.append($ul);
        }
      }

    }

    return $li;
  }

  getAnnotationTerm(term) {
    var annotationTerm;
    if (this.mode == 'set') {
      if (this.annotationValues[term] && this.annotationValues[term].name) {
        annotationTerm = this.annotationValues[term].name;
      }
    } else {
      annotationTerm = this.annotationValues[term];
    }
    return annotationTerm;
  }

  getAnnotationDisabled(term) {
    var annotationTerm;
    if (this.mode == 'set') {
      if (this.annotationValues[term] && this.annotationValues[term].disabled) {
        annotationTerm = this.annotationValues[term].disabled;
      }
    }
    return annotationTerm;
  }

  // `true` - checked, `false` - unchecked, `null` - partially checked/unchecked
  isTreeChecked(item) {
    var prefix = item.prefix;
    var fullName = this.formatTreeName(prefix, item).toLowerCase();

    var selected = this.getSelectedLower();

    // unchecked this item
    if (selected[fullName] === false) {
      return false;
    }

    // child is checked or not checked
    for (var sKey in selected) {
      if (sKey.indexOf(fullName + '\\') != -1) {
        return null;
      }
    }

    // checked this item (only if no unchecked children)
    if (selected && selected[fullName]) {
      return true;
    }

    // parent is checked
    var parts = fullName.split('\\');
    var name;
    for (var part of parts) {
      if (name) {
        name = name + '\\' + part;
      } else {
        name = part;
      }
      if (selected && selected[name])
        return true;
    }

    return false;
  }

  formatTreeName(prefix, item) {
    if (!prefix)
      return `${item.term}`;
    return `${prefix}\\${item.term}`;
  }

  buildTreeItemPath(item) {
    var parts = [];
    if (item.prefix) {
      parts.push(item.prefix);
    }
    parts.push(item.term);
    return parts.join('\\');
  }

}
function tooltip(classNames?: any) {
    const searchHelp = classNames?.searchHelp || 'sjsf-search-help';

    return `
        <h4 class="${searchHelp}-popup__heading" data-section='general'>General</h4>
        <ul class="${searchHelp}__operators" data-section='general'>
            <li>
                (apple <strong class="${searchHelp}__mark">OR</strong>&nbsp;pear)&nbsp;<strong class="${searchHelp}__mark">AND NOT</strong> cider</li>
            <li>
                <strong class="${searchHelp}__mark">"big pear"</strong>
                <span class="${searchHelp}__operators__res"><span class="${searchHelp}__operators__res_sep">|</span>use "" to match phrase</span>
            </li>
        </ul>
        <h4 class="${searchHelp}-popup__heading" data-section='structured'>Structured Query Operators</h4>
        <ul class="${searchHelp}__operators" data-section='structured'>
            <li>
                <span class="${searchHelp}__mark">fruit=pear</span>
                <span class="${searchHelp}__operators__res">
                    <span class="${searchHelp}__operators__res_sep">|</span><b>fruit</b> field must be <b>pear</b> (case-sensitive)
                </span>
            </li>
            <li>
                <span class="${searchHelp}__mark">fruit="big pear"</span>
                <span class="${searchHelp}__operators__res"><span class="${searchHelp}__operators__res_sep">|</span><b>fruit</b> field must be <b>big pear</b></span>
            </li>
            <li>
                <span class="${searchHelp}__mark">my_num&lt;12.42 my_date&gt;2011</span>
            </li>
            <li>
                <span class="${searchHelp}__mark">set=country</span>
                <span class="${searchHelp}__operators__res"><span class="${searchHelp}__operators__res_sep">|</span>country field is <b>set</b></span>
            </li>
            <li>
                <span class="${searchHelp}__mark">sid=myseriesid</span> 
                <span class="${searchHelp}__operators__res"><span class="${searchHelp}__operators__res_sep">|</span>exact Series ID</span>
            </li>
            <li>
                <span class="${searchHelp}__mark">sid:users</span>
                <span class="${searchHelp}__operators__res"><span class="${searchHelp}__operators__res_sep">|</span>Series ID starts with users</span>
            </li>
        </ul>
        <h4 class="${searchHelp}-popup__heading" data-section='unstructured'>
            Unstructured Query Operators
            <br>
            <small>(case-insensitive)</small>
        </h4>
        <ul class="${searchHelp}__operators" data-section='unstructured'>
            <li>
                <span class="${searchHelp}__mark">pear</span>
                <span class="${searchHelp}__operators__res"><span class="${searchHelp}__operators__res_sep">|</span>finds pear anywhere in any default field</span>
            </li>
            <li>
                <span class="${searchHelp}__mark">fruit:pear</span>
                <span class="${searchHelp}__operators__res"><span class="${searchHelp}__operators__res_sep">|</span>finds pear anywhere in fruit field</span>
            </li>
            <li>
                <span class="${searchHelp}__mark">*:(pear)</span>
                <span class="${searchHelp}__operators__res"><span class="${searchHelp}__operators__res_sep">|</span>finds pear anywhere in any field</span>
            </li>
        </ul>
        <div class="${searchHelp}__see_more">
            See 
            <a target="_blank" class="${searchHelp}-popup__link" href="http://docs.shooju.com/queries/">Queries</a>
            for more details
        </div>
        `;
}

var $ = (window['SJSearchLoader'] && window['SJSearchLoader'].jQuery) ? window['SJSearchLoader'].jQuery : window['jQuery'];

var constants = {
    // elements
    queryBarSuggestionItemEl: '<span></span>',
    queryBarSuggestionsCloseEl: '<i></i>',
    resultRowEl: '<div></div>',

    // ids
    searchResultsWrapperId: 'sj-searchResults-wrapper',

    // class names
    autocompleteWrapper: 'sj-autocomplete-wrapper',
    resultRow: 'sj-resultRow',
    resultRowHovered: 'sj-resultRow-hovered',

    // getHighlightedValueString
    resultValueWrapper: 'sj-result-value-wrapper',
    resultValue: 'sj-result-value',
    resultValueIcon: 'sj-result-value-icon',
    resultAnnotation: 'sj-result-annotation',
    resultHighlight: 'sj-result-highlight',
    resultGreyHighlight: 'sj-result-greyHighlight',

    // buildRowMarkup
    optionCount: 'sj-optionCount',
    optionHint: 'sj-optionHint',

    // helpers
    queryBarSuggestions: 'sj-query-bar-suggestions',
    searchHelp: 'sjsf-search-help',

    // SJ_Search_Input_Html
    searchInput: 'sj-searchInput',
    acInputEnter: 'sj-ac-input-enter',
    acClearEl: '<i></i>', // or '<button></button>'
    acClear: 'sj-ac-clear',

    // SJ_Search_Results_Html
    searchResultsWrapper: 'sj-searchResults-wrapper',
    searchResults: 'sj-searchResults',
    acLoading: 'sj-ac-loading',
    resultBlock: 'sj-result-block',

    // init
    tips: 'sjsf-tips',
};

var labels = {
    didYouMean: 'Did you mean'
};

function suggestions(classNames?: any, labels?: any) {
    const queryBarSuggestions = classNames?.queryBarSuggestions;
    var $closeEl = $(classNames?.queryBarSuggestionsCloseEl);
    $closeEl
        .addClass(`${classNames.queryBarSuggestions}-close`)
        .html(`<svg aria-label="Close Suggestions" width="12" height="12" viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path d="M11.7071 1.70711C12.0977 1.31658 12.0977 0.683417 11.7071 0.292893C11.3166 -0.0976311 10.6835 -0.0976311 10.2929 0.292893L6.00002 4.58581L1.70711 0.292894C1.31658 -0.0976304 0.683418 -0.0976304 0.292894 0.292894C-0.0976312 0.683418 -0.0976312 1.31658 0.292894 1.70711L4.58581 6.00002L0.292932 10.2929C-0.0975927 10.6834 -0.0975927 11.3166 0.292932 11.7071C0.683456 12.0976 1.31662 12.0976 1.70715 11.7071L6.00002 7.41423L10.2929 11.7071C10.6834 12.0976 11.3166 12.0976 11.7071 11.7071C12.0976 11.3166 12.0976 10.6834 11.7071 10.2929L7.41423 6.00002L11.7071 1.70711Z" fill="#ABB1B8"/>
</svg>`);

    return `<div class="${queryBarSuggestions}-wrapper" id="downshift-menu">
            <label>${labels.didYouMean || 'Did you mean'}<span class="${queryBarSuggestions}-colon">:</span></label>
            <div class="${queryBarSuggestions}-items"></div><span class="${queryBarSuggestions}-question">?</span>
            ${$closeEl[0].outerHTML}
        </div>`;
}

// support old versions of jquery (e.g. 1.4.2+)
function jqOn($el, event, selector, handler) {
    if ($.fn.on) {
        if (selector) {
            $el.on(event, selector, handler);
        } else {
            $el.on(event, handler);
        }
    } else {
        if (selector) {
            $el.delegate(selector, event, handler);
        } else {
            $el.bind(event, handler);
        }
    }
}

function endsWith(str, suffix) {
    str = str || '';
    suffix = suffix || '';
    if (str && suffix && str.length >= suffix.length) {
        var end = str.slice(str.length - suffix.length, str.length);
        return suffix == end;
    }
    return false;
}

function trim(str) {
    str = str || '';
    return str.replace(/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, '');
}

function getFieldMode(field) {
    field = field || '';
    if (!field) {
        return 'set';
    }
    if (['meta.levels', 'meta.points_count', 'meta.points_max', 'meta.points_min']
        .indexOf(field) != -1 || endsWith(field, '_num')) {
        return 'num';
    }
    if (['meta.dates_max', 'meta.dates_min', 'meta.updated_at'].indexOf(field) != -1 || endsWith(field, '_date')) {
        return 'date';
    }
    if (field.toLowerCase() == 'sid' || endsWith(field, '_tree')) {
        return 'tree';
    }
    return 'text';
}

var escapeRegexp = /[~><\*(),:=\@\s]/;

/**
 * Escape text for query
 * */
function escapeQueryText(val) {
    var str = String(val);
    var shouldEscape = !str || escapeRegexp.test(str);
    return shouldEscape ? `"${str}"` : str;
}

function shortNumber(num) {
    num = parseFloat(num);
    if (num >= 100000000000)
        return (num / 1000000000).toFixed(0).replace(/\.0$/, '') + 'g';
    if (num >= 10000000000)
        return (num / 1000000000).toFixed(1).replace(/\.0$/, '') + 'g';
    if (num >= 1000000000)
        return (num / 1000000000).toFixed(2).replace(/\.0$/, '') + 'g';

    if (num >= 100000000)
        return (num / 1000000).toFixed(0).replace(/\.0$/, '') + 'm';
    if (num >= 10000000)
        return (num / 1000000).toFixed(1).replace(/\.0$/, '') + 'm';
    if (num >= 1000000)
        return (num / 1000000).toFixed(2).replace(/\.0$/, '') + 'm';

    if (num >= 100000)
        return (num / 1000).toFixed(0).replace(/\.0$/, '') + 'k';
    if (num >= 10000)
        return (num / 1000).toFixed(1).replace(/\.0$/, '') + 'k';
    if (num >= 1000)
        return (num / 1000).toFixed(2).replace(/\.0$/, '') + 'k';
    else
        return Math.round(num * 1000) / 1000;
};

class Request {
    constructor(private server, private get_credentials) {
    }

    call(args) {
        var self = this;
        args.url = this.server + args.url;

        var locationParams = $.param({
            location_type: 'sjautocomplete',
            location: location.href
        });
        if (args.url.indexOf('?') == -1) {
            args.url += '?' + locationParams;
        } else {
            args.url += '&' + locationParams;
        }

        args.dataType = args.dataType || "json";
        args.contentType = args.contentType || "application/json; charset=utf-8";
        args.beforeSend = function (xhr) {
            var credentials = self.get_credentials();
            xhr.setRequestHeader("Authorization", "Basic " + btoa(credentials.api_username + ":" + credentials.api_secret));
        };
        return $.ajax(args);
    }
}

export { tooltip, jqOn, endsWith, getFieldMode, escapeQueryText, Request, shortNumber, trim, $, suggestions, constants, labels };
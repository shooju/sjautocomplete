import { endsWith, getFieldMode, shortNumber, $ } from './helpers';
import Terms from './terms';

class QueryBarFilter extends Terms {

    ajaxRequests = {};
    globalClickEventHandler: any;
    wizard = false;
    removed = false;

    constructor(settings, private queryBar, state) {
        super(settings, state);
    }

    draw(annotations, recommended, fieldsToHide, wizard) {
        if (this.removed) {
            return;
        }
        super.setPreferences(annotations, recommended, fieldsToHide);

        var field = this.settings.field;
        var fieldLabel = field;
        var tooltip = '';
        var addFieldsToTable = !field && !!this.settings.addSelectedToTable;

        if (annotations && Object.keys(annotations).length) {
            if (this.mode != 'set') {
                if (annotations[field]) {
                    if (annotations[field].name) {
                        fieldLabel = annotations[field].name;
                        tooltip = `<span class='sj-query-bar-field-tooltip'><span>${field}</span></span>`;
                    }
                }
            }
        }

        var iconHtml = `<i class="sj-query-bar-filter-plus">
            <svg width="30" height="30" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M0 2C0 0.89543 0.895431 0 2 0H28C29.1046 0 30 0.895431 30 2V28C30 29.1046 29.1046 30 28 30H2C0.89543 30 0 29.1046 0 28V2Z" fill="#567483"/>
                <path fill-rule="evenodd" clip-rule="evenodd" d="M6 9H18V11H6V9ZM6 13H18V15H6V13ZM14 17H6V19H14V17ZM22 13V17H26V19H22V23H20V19H16V17H20V13H22Z" fill="white"/>
            </svg>
        </i>`;

        var pinHtml = `<div class="sj-query-bar-filter-pin ${this.state.pined ? 'sj-query-bar-filter-pin-active' : ''}">
            <span class="sj-tip-root">
                <svg width="14" height="15" viewBox="0 0 14 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M0.892578 13.6076V1.39258H13.1076" stroke="#B1C3CC" stroke-width="1.71" stroke-linecap="round" stroke-linejoin="round"/>
                    <path d="M4.50968 4.36377C4.11674 4.36377 3.80799 4.67251 3.80799 5.03739V8.85458C3.80799 9.21946 4.03253 9.55627 4.39741 9.6966C4.73422 9.83694 5.12717 9.75274 5.37978 9.50013L6.2218 8.6581L10.2074 12.6156C10.5723 12.9805 11.1336 12.9805 11.4704 12.6156L12.116 11.9981C12.4809 11.6333 12.4809 11.0719 12.116 10.707L8.1304 6.74951L8.97242 5.90748C9.22503 5.65488 9.30923 5.26193 9.1689 4.92512C9.02856 4.58831 8.69175 4.36377 8.32687 4.36377H4.50968Z" fill="#B1C3CC"/>
                </svg>            
                <span class="sj-query-bar-filter-pin-tip sj-tip sj-tip-top">
                    <span>
                        Snap to left side to keep this list open
                    </span>
                </span>
            </span>
        </div>`;

        var notHtml = `<div class="sj-query-bar-filter-not ${this.state.not ? 'sj-query-bar-filter-not-active' : ''}">
            <span class="sj-tip-root">
                <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" clip-rule="evenodd" d="M2 8C2 4.68629 4.68629 2 8 2C9.29587 2 10.4958 2.41082 11.4766 3.10931L3.10931 11.4766C2.41081 10.4958 2 9.29587 2 8ZM4.52355 12.8908C5.50435 13.5892 6.70421 14 8 14C11.3137 14 14 11.3137 14 8C14 6.70421 13.5892 5.50435 12.8908 4.52355L4.52355 12.8908ZM8 0C3.58172 0 0 3.58172 0 8C0 12.4183 3.58172 16 8 16C12.4183 16 16 12.4183 16 8C16 3.58172 12.4183 0 8 0Z" fill="#ABB1B8"/>
                </svg>
                <i class="fa-solid fa-ban"></i>
                <span class="sj-query-bar-filter-not-tip sj-tip sj-tip-top">
                    <span>
                        <span>NOT mode excludes the selection below</span>
                        <span>Back to normal mode</span>
                    </span>
                </span>
            </span>
        </div>`;

        var pinSmallIconHtml = `<div class="sj-query-bar-filter-pin-icon">
            <svg width="14" height="15" viewBox="0 0 14 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M0.892578 13.6076V1.39258H13.1076" stroke="#567483" stroke-width="1.71" stroke-linecap="round" stroke-linejoin="round"/>
                <path d="M4.50968 4.36377C4.11674 4.36377 3.80799 4.67251 3.80799 5.03739V8.85458C3.80799 9.21946 4.03253 9.55627 4.39741 9.6966C4.73422 9.83694 5.12717 9.75274 5.37978 9.50013L6.2218 8.6581L10.2074 12.6156C10.5723 12.9805 11.1336 12.9805 11.4704 12.6156L12.116 11.9981C12.4809 11.6333 12.4809 11.0719 12.116 10.707L8.1304 6.74951L8.97242 5.90748C9.22503 5.65488 9.30923 5.26193 9.1689 4.92512C9.02856 4.58831 8.69175 4.36377 8.32687 4.36377H4.50968Z" fill="#567483"/>
            </svg>
        </div>`;

        var notSmallIconHtml = `<div class="sj-query-bar-filter-not-icon">
            <svg width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                <rect width="18" height="18" rx="2.25" fill="#E84C02"/>
                <path fill-rule="evenodd" clip-rule="evenodd" d="M5 9C5 6.79086 6.79086 5 9 5C9.74181 5 10.4365 5.20193 11.032 5.55382L5.55382 11.032C5.20193 10.4365 5 9.74181 5 9ZM6.96803 12.4462C7.56355 12.7981 8.25819 13 9 13C11.2091 13 13 11.2091 13 9C13 8.25819 12.7981 7.56355 12.4462 6.96803L6.96803 12.4462ZM9 3C5.68629 3 3 5.68629 3 9C3 12.3137 5.68629 15 9 15C12.3137 15 15 12.3137 15 9C15 5.68629 12.3137 3 9 3Z" fill="white"/>
            </svg>
            <i class="fa-solid fa-ban"></i>
        </div>`;

        var pinedHtml = `<div class="sj-query-bar-filter-pined-icon">
            <div class="sj-query-bar-filter-pined-icon-svg">
                <svg width="14" height="15" viewBox="0 0 14 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M0.892578 13.6076V1.39258H13.1076" stroke="#567483" stroke-width="1.71" stroke-linecap="round" stroke-linejoin="round"/>
                    <path d="M4.50968 4.36377C4.11674 4.36377 3.80799 4.67251 3.80799 5.03739V8.85458C3.80799 9.21946 4.03253 9.55627 4.39741 9.6966C4.73422 9.83694 5.12717 9.75274 5.37978 9.50013L6.2218 8.6581L10.2074 12.6156C10.5723 12.9805 11.1336 12.9805 11.4704 12.6156L12.116 11.9981C12.4809 11.6333 12.4809 11.0719 12.116 10.707L8.1304 6.74951L8.97242 5.90748C9.22503 5.65488 9.30923 5.26193 9.1689 4.92512C9.02856 4.58831 8.69175 4.36377 8.32687 4.36377H4.50968Z" fill="#567483"/>
                </svg>            
            </div>
            <span class="sj-query-bar-filter-pined-text">
                ${this.settings.field || '<em>field selector</em>'}
            </span>
        </div>`;

        var lis;

        if (field) {
            var mode = this.mode;
            switch (mode) {
                case 'num':
                    lis = `
                        <li class="sj-query-bar-filter-num">
                            <div class='sj-query-bar-flex'>
                                <div style='flex: 1;'>
                                    ${field}
                                </div>
                                <div style='flex: 90px 0 0;'>
                                    <select data-state="op">
                                        <option value=">" ${this.state.op == '>' ? 'selected' : ''}>greater than</option>
                                        <option value="<" ${this.state.op == '<' ? 'selected' : ''}>less than</option>
                                        <option value="=" ${this.state.op == '=' ? 'selected' : ''}>equals</option>
                                    </select>
                                </div>
                                <div>
                                    <input type="number" style='width: 50px;' value="${this.state.val}" data-state="val"/>
                                </div>
                            </div>
                        </li>
                    `;
                    break;
                case 'date':
                    lis = `
                        <li class="sj-query-bar-filter-date sj-query-bar-flex sj-btns sj-btns-alt sj-btns-inline">
                            <div class="sj-btn sj-btn-light" data-date-style='relative'>
                                <span>
                                    Relative
                                </span>
                            </div>
                            <div class="sj-btn sj-btn-light" data-date-style='absolute'>
                                <span>
                                    Absolute
                                </span>
                            </div>
                        </li>
                        <li class="sj-query-bar-filter-date-relative sj-query-bar-flex">
                            <div data-date-relative="rel_op">
                                <select data-state="rel_op">
                                    <option value=">" ${this.state.rel_op == '>' ? 'selected' : ''}>since</option>
                                    <option value="<" ${this.state.rel_op == '<' ? 'selected' : ''}>until</option>
                                </select>

                                <div>
                                    <div class="sj-label sj-label-inline">
                                        <label>
                                            <div class="sj-input">
                                                <input type="radio" data-state="rel_op" value=">"
                                                    ${this.state.rel_op == '>' ? 'checked="checked"' : ''}
                                                    name="qbf-date-op-${field}"/>
                                            </div>
                                            <span>since</span>
                                        </label>
                                    </div>
                                </div>
                                <div>
                                    <div class="sj-label sj-label-inline">
                                        <label>
                                            <div class="sj-input">
                                                <input type="radio" data-state="rel_op" value="<"
                                                    ${this.state.rel_op == '<' ? 'checked="checked"' : ''}
                                                    name="qbf-date-op-${field}"/>
                                            </div>
                                            <span>until</span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div data-date-relative="rel_val">
                                <input type="number" style='width: 50px;' value="${this.state.rel_val}" 
                                    data-state="rel_val"/>
                                
                                <div class="sj-input sj-input-xs">
                                    <input type="text" type="number" value="${this.state.rel_val}" 
                                        data-state="rel_val"/>
                                </div>
                            </div>
                            <div data-date-relative="rel_per">
                                <select data-state="rel_per">
                                    <option value="d" ${this.state.rel_per == 'd' ? 'selected' : ''}>days</option>
                                    <option value="w" ${this.state.rel_per == 'w' ? 'selected' : ''}>weeks</option>
                                    <option value="M" ${this.state.rel_per == 'm' ? 'selected' : ''}>months</option>
                                    <option value="y" ${this.state.rel_per == 'y' ? 'selected' : ''}>years</option>
                                </select>

                                <div class="sj-input sj-input-xs">
                                    <select data-state="rel_per">
                                        <option value="d" ${this.state.rel_per == 'd' ? 'selected' : ''}>days</option>
                                        <option value="w" ${this.state.rel_per == 'w' ? 'selected' : ''}>weeks</option>
                                        <option value="M" ${this.state.rel_per == 'm' ? 'selected' : ''}>months</option>
                                        <option value="y" ${this.state.rel_per == 'y' ? 'selected' : ''}>years</option>
                                    </select>
                                </div>
                            </div>
                            <div data-date-relative="rel_sign">
                                <select data-state="rel_sign">
                                    <option value="-" ${this.state.rel_sign == '-' ? 'selected' : ''}>ago</option>
                                    <option value="+" ${this.state.rel_sign == '+' ? 'selected' : ''}>from now</option>
                                </select>

                                <div>
                                    <div class="sj-label sj-label-inline">
                                        <label>
                                            <div class="sj-input">
                                                <input type="radio" value="-" 
                                                    data-state="rel_sign"
                                                    ${this.state.rel_sign == '-' ? 'checked="checked"' : ''}
                                                    name="qbf-date-sign-${field}"/>
                                            </div>
                                            <span>AGO</span>
                                        </label>
                                    </div>
                                </div>
                                <div>
                                    <div class="sj-label sj-label-inline">
                                        <label>
                                            <div class="sj-input">
                                                <input type="radio" value="+"
                                                    data-state="rel_sign"
                                                    ${this.state.rel_sign == '+' ? 'checked="checked"' : ''}
                                                    name="qbf-date-sign-${field}"/>
                                            </div>
                                            <span>FROM NOW</span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="sj-query-bar-filter-date-absolute">
                            <div class="sj-query-bar-flex">
                                <div>
                                    Attribute
                                </div>
                                <div class="sj-input sj-input-xs">
                                    <select data-state="abs_per">
                                        <option value="m" ${this.state.abs_per == 'm' ? 'selected' : ''}>minutes</option>
                                        <option value="h" ${this.state.abs_per == 'h' ? 'selected' : ''}>hours</option>
                                        <option value="d" ${this.state.abs_per == 'd' ? 'selected' : ''}>days</option>
                                        <option value="w" ${this.state.abs_per == 'w' ? 'selected' : ''}>weeks</option>
                                        <option value="M" ${this.state.abs_per == 'M' ? 'selected' : ''}>months</option>
                                        <option value="y" ${this.state.abs_per == 'y' ? 'selected' : ''}>years</option>
                                    </select>
                                </div>
                            </div>
                        </li>
                        <li class="sj-query-bar-filter-date-absolute sj-query-bar-filter-loading">Loading...</li>
                        <li class="sj-query-bar-filter-date-absolute sj-query-bar-filter-item-empty">
                            <svg width="17" height="16" viewBox="0 0 17 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <circle cx="7.5" cy="7" r="6" stroke="#D0D4D8" stroke-width="2"/>
                                <path d="M15.5 15L12 11.5" stroke="#D0D4D8" stroke-width="2" stroke-linecap="round"/>
                            </svg>
                            <span>No Values</span>
                        </li>
                    `;
                    break;
                case 'tree':
                    lis = `
                        <li class="sj-query-bar-filter-search">
                            <div class="sj-query-bar-pined-container">
                                ${this.settings.pined ? pinedHtml : ''}
                            </div>
                            <div class="sj-query-bar-flex">
                                ${this.settings.pinable ? pinHtml : ''}
                                ${notHtml}
                                <div class="sj-query-bar-filter-search-input sj-input sj-input-xs sj-input-search">
                                    <input placeholder="Search..." type="text" value="${this.state.val}" data-state="val"/>
                                    <span class="sj-query-bar-filter-search-icon sj-input-search-icon">
                                        <svg width="16" height="16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M15.6344 13.8641L11.8938 10.1239C12.7444 8.86123 13.1641 7.28359 12.9394 5.60252C12.5563 2.74313 10.2125 0.416195 7.34999 0.0538272C3.09404 -0.484647 -0.484725 3.09372 0.0538391 7.3492C0.41634 10.2126 2.74385 12.558 5.60386 12.9392C7.28512 13.1639 8.86325 12.7444 10.1258 11.8937L13.8664 15.6339C14.3545 16.122 15.1461 16.122 15.6342 15.6339C16.1219 15.1452 16.1219 14.3516 15.6344 13.8641ZM2.47185 6.4993C2.47185 4.29391 4.26623 2.49972 6.47186 2.49972C8.6775 2.49972 10.4719 4.29391 10.4719 6.4993C10.4719 8.70469 8.6775 10.4989 6.47186 10.4989C4.26623 10.4989 2.47185 8.70531 2.47185 6.4993Z" fill="#D8DDE3"/>
                                        </svg>
                                    </span>
                                    <span class="sj-query-bar-filter-search-remove sj-input-clear-icon ${this.state.val ? 'sj-query-bar-filter-search-shown' : ''}">
                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 352 512">
                                            <path d="M242.72 256l100.07-100.07c12.28-12.28 12.28-32.19 0-44.48l-22.24-22.24c-12.28-12.28-32.19-12.28-44.48 0L176 189.28 75.93 89.21c-12.28-12.28-32.19-12.28-44.48 0L9.21 111.45c-12.28 12.28-12.28 32.19 0 44.48L109.28 256 9.21 356.07c-12.28 12.28-12.28 32.19 0 44.48l22.24 22.24c12.28 12.28 32.2 12.28 44.48 0L176 322.72l100.07 100.07c12.28 12.28 32.2 12.28 44.48 0l22.24-22.24c12.28-12.28 12.28-32.19 0-44.48L242.72 256z"/>
                                        </svg>
                                    </span>
                                </div>
                                <div>
                                    <button class='sj-query-bar-toggle ${this.state.exact ? "sj-query-bar-toggle-active" : ""}' data-state="exact">:exact</button>
                                </div>
                            </div>
                        </li>
                        <li class="sj-query-bar-filter-loading">Loading...</li>
                        <li class="sj-query-bar-filter-item-empty">
                            <svg width="17" height="16" viewBox="0 0 17 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <circle cx="7.5" cy="7" r="6" stroke="#D0D4D8" stroke-width="2"/>
                                <path d="M15.5 15L12 11.5" stroke="#D0D4D8" stroke-width="2" stroke-linecap="round"/>
                            </svg>
                            <span>No Values</span>
                        </li>
                    `;
                    break;
                case 'text':
                    lis = `
                        <li class="sj-query-bar-filter-search">
                            <div class="sj-query-bar-pined-container">
                                ${this.settings.pined ? pinedHtml : ''}
                            </div>
                            <div class="sj-query-bar-flex">
                                ${this.settings.pinable ? pinHtml : ''}
                                ${notHtml}
                                <div class="sj-query-bar-filter-search-input sj-input sj-input-xs sj-input-search">
                                    <input placeholder="Search..." type="text" value="${this.state.val}" data-state="val"/>
                                    <span class="sj-query-bar-filter-search-icon sj-input-search-icon">
                                        <svg width="16" height="16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M15.6344 13.8641L11.8938 10.1239C12.7444 8.86123 13.1641 7.28359 12.9394 5.60252C12.5563 2.74313 10.2125 0.416195 7.34999 0.0538272C3.09404 -0.484647 -0.484725 3.09372 0.0538391 7.3492C0.41634 10.2126 2.74385 12.558 5.60386 12.9392C7.28512 13.1639 8.86325 12.7444 10.1258 11.8937L13.8664 15.6339C14.3545 16.122 15.1461 16.122 15.6342 15.6339C16.1219 15.1452 16.1219 14.3516 15.6344 13.8641ZM2.47185 6.4993C2.47185 4.29391 4.26623 2.49972 6.47186 2.49972C8.6775 2.49972 10.4719 4.29391 10.4719 6.4993C10.4719 8.70469 8.6775 10.4989 6.47186 10.4989C4.26623 10.4989 2.47185 8.70531 2.47185 6.4993Z" fill="#D8DDE3"/>
                                        </svg>
                                    </span>
                                    <span class="sj-query-bar-filter-search-remove sj-input-clear-icon ${this.state.val ? 'sj-query-bar-filter-search-shown' : ''}">
                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 352 512">
                                            <path d="M242.72 256l100.07-100.07c12.28-12.28 12.28-32.19 0-44.48l-22.24-22.24c-12.28-12.28-32.19-12.28-44.48 0L176 189.28 75.93 89.21c-12.28-12.28-32.19-12.28-44.48 0L9.21 111.45c-12.28 12.28-12.28 32.19 0 44.48L109.28 256 9.21 356.07c-12.28 12.28-12.28 32.19 0 44.48l22.24 22.24c12.28 12.28 32.2 12.28 44.48 0L176 322.72l100.07 100.07c12.28 12.28 32.2 12.28 44.48 0l22.24-22.24c12.28-12.28 12.28-32.19 0-44.48L242.72 256z"/>
                                        </svg>
                                    </span>
                                </div>
                                <div>
                                    <button class='sj-query-bar-toggle ${this.state.fuzzy ? "sj-query-bar-toggle-active" : ""}' data-state="fuzzy">:partial</button>
                                </div>
                            </div>
                        </li>
                        <li class="sj-query-bar-filter-loading">Loading...</li>
                        <li class="sj-query-bar-filter-item-empty">
                            <svg width="17" height="16" viewBox="0 0 17 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <circle cx="7.5" cy="7" r="6" stroke="#D0D4D8" stroke-width="2"/>
                                <path d="M15.5 15L12 11.5" stroke="#D0D4D8" stroke-width="2" stroke-linecap="round"/>
                            </svg>
                            <span>No Values</span>
                        </li>
                    `;
                    break;
            }
        } else {
            this.wizard = wizard;
            lis = `
                <li class="sj-query-bar-filter-search">
                    <div class="sj-query-bar-pined-container">
                        ${this.settings.pined ? pinedHtml : ''}
                    </div>
                    <div class="sj-query-bar-flex">
                        ${this.settings.pinable ? pinHtml : ''}
                        <div class="sj-query-bar-filter-search-input sj-input sj-input-xs sj-input-search">
                            <input placeholder="Find field..." type="text"/>
                            <span class="sj-query-bar-filter-search-icon sj-input-search-icon">
                                <svg width="16" height="16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M15.6344 13.8641L11.8938 10.1239C12.7444 8.86123 13.1641 7.28359 12.9394 5.60252C12.5563 2.74313 10.2125 0.416195 7.34999 0.0538272C3.09404 -0.484647 -0.484725 3.09372 0.0538391 7.3492C0.41634 10.2126 2.74385 12.558 5.60386 12.9392C7.28512 13.1639 8.86325 12.7444 10.1258 11.8937L13.8664 15.6339C14.3545 16.122 15.1461 16.122 15.6342 15.6339C16.1219 15.1452 16.1219 14.3516 15.6344 13.8641ZM2.47185 6.4993C2.47185 4.29391 4.26623 2.49972 6.47186 2.49972C8.6775 2.49972 10.4719 4.29391 10.4719 6.4993C10.4719 8.70469 8.6775 10.4989 6.47186 10.4989C4.26623 10.4989 2.47185 8.70531 2.47185 6.4993Z" fill="#D8DDE3"/>
                                </svg>
                            </span>
                            <span class="sj-query-bar-filter-search-remove sj-input-clear-icon">
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 352 512">
                                    <path d="M242.72 256l100.07-100.07c12.28-12.28 12.28-32.19 0-44.48l-22.24-22.24c-12.28-12.28-32.19-12.28-44.48 0L176 189.28 75.93 89.21c-12.28-12.28-32.19-12.28-44.48 0L9.21 111.45c-12.28 12.28-12.28 32.19 0 44.48L109.28 256 9.21 356.07c-12.28 12.28-12.28 32.19 0 44.48l22.24 22.24c12.28 12.28 32.2 12.28 44.48 0L176 322.72l100.07 100.07c12.28 12.28 32.2 12.28 44.48 0l22.24-22.24c12.28-12.28 12.28-32.19 0-44.48L242.72 256z"/>
                                </svg>
                            </span>
                        </div>
                        ${addFieldsToTable ? `<div class='sj-query-bar-addfield-wrapper'>
                            <button class='sj-query-bar-addfield' data-state="addfield">
                                <span>
                                    Add selected fields to table
                                </span>
                                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M4 12H8" stroke="#D0D4D8" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                    <path d="M4 8H7" stroke="#567483" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                    <path d="M4 16H8" stroke="#D0D4D8" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                    <path d="M4 20H8" stroke="#D0D4D8" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                    <path d="M11 12H19" stroke="#D0D4D8" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                    <path d="M11 8H14" stroke="#567483" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                    <path d="M11 16H19" stroke="#D0D4D8" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                    <path d="M11 20H19" stroke="#D0D4D8" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                    <path fill-rule="evenodd" clip-rule="evenodd" d="M19 7C19 7.55228 19.4477 8 20 8C20.5523 8 21 7.55228 21 7V5H23C23.5523 5 24 4.55228 24 4C24 3.44772 23.5523 3 23 3H21V1C21 0.447715 20.5523 0 20 0C19.4477 0 19 0.447715 19 1V3H17C16.4477 3 16 3.44772 16 4C16 4.55228 16.4477 5 17 5H19V7Z" fill="#567483"/>
                                </svg>
                            </button>
                            <i>
                                <svg class="spinner" width="18px" height="18px" viewBox="0 0 18 18" xmlns="http://www.w3.org/2000/svg">
                                    <circle class="path" fill="transparent" stroke-width="2" cx="9" cy="9" r="8" stroke="url(#sj-ac-wiz-spin)"/>
                                    <radialGradient id="sj-ac-wiz-spin" cx="0.15" cy="0.15" r="0.7">  
                                        <stop offset="0%" stop-color="#567483" stop-opacity="0"/>
                                        <stop offset="25%" stop-color="#567483" stop-opacity="0"/>
                                        <stop offset="85%" stop-color="#567483" stop-opacity="0.5"/>
                                        <stop offset="100%" stop-color="#567483" stop-opacity="1"/>
                                    </radialGradient>
                                    </circle>
                                </svg> 
                            </i>
                            <span>
                                <svg width="18" height="14" viewBox="0 0 18 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M2 7.71429L6.2 12L16 2" stroke="#75C669" stroke-width="3" stroke-linecap="round" stroke-linejoin="round"/>
                                </svg>
                            </span>
                        </div>` : ''}
                        ${wizard ? `<div class='sj-query-bar-wizard-wrapper'>
                            <button class='sj-query-bar-wizard' data-state="wizard">
                                <span>
                                    Adds optimal fields to filter on based on the current query.
                                </span>
                                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M5 19L12 12" stroke="#567483" stroke-width="2" stroke-linecap="round"/>
                                    <path d="M18.1113 5.23811C18.507 5.09921 18.8876 5.47981 18.7487 5.87549L17.8755 8.36296L19.4746 10.4589C19.729 10.7923 19.4846 11.2718 19.0654 11.262L16.4298 11.2002L14.9307 13.3688C14.6922 13.7137 14.1606 13.6295 14.0404 13.2277L13.2847 10.7021L10.759 9.94641C10.3573 9.82621 10.2731 9.29459 10.618 9.05611L12.7865 7.55695L12.7248 4.9214C12.7149 4.50216 13.1945 4.2578 13.5279 4.51218L15.6238 6.11127L18.1113 5.23811Z" fill="#567483"/>
                                </svg>
                            </button>
                            <i>
                                <svg class="spinner" width="18px" height="18px" viewBox="0 0 18 18" xmlns="http://www.w3.org/2000/svg">
                                    <circle class="path" fill="transparent" stroke-width="2" cx="9" cy="9" r="8" stroke="url(#sj-ac-wiz-spin)"/>
                                    <radialGradient id="sj-ac-wiz-spin" cx="0.15" cy="0.15" r="0.7">  
                                        <stop offset="0%" stop-color="#567483" stop-opacity="0"/>
                                        <stop offset="25%" stop-color="#567483" stop-opacity="0"/>
                                        <stop offset="85%" stop-color="#567483" stop-opacity="0.5"/>
                                        <stop offset="100%" stop-color="#567483" stop-opacity="1"/>
                                    </radialGradient>
                                    </circle>
                                </svg> 
                            </i>
                            <span>
                                <svg width="18" height="14" viewBox="0 0 18 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M2 7.71429L6.2 12L16 2" stroke="#75C669" stroke-width="3" stroke-linecap="round" stroke-linejoin="round"/>
                                </svg>
                            </span>
                        </div>` : ''}
                    </div>
                </li>
                <li class="sj-query-bar-filter-loading">Loading...</li>
                <li class="sj-query-bar-filter-item-empty">
                    <svg width="17" height="16" viewBox="0 0 17 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <circle cx="7.5" cy="7" r="6" stroke="#D0D4D8" stroke-width="2"/>
                        <path d="M15.5 15L12 11.5" stroke="#D0D4D8" stroke-width="2" stroke-linecap="round"/>
                    </svg>
                    <span>No Fields</span>
                </li>
            `;
        }

        var removeHtml = `
        <span class="sj-query-bar-filter-remove-wrapper">
            <i class="sj-query-bar-filter-remove">
                <svg width="16" height="16" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <circle cx="9" cy="9" r="9" fill="#ABB1B8"/>
                    <path fill-rule="evenodd" clip-rule="evenodd" d="M12.7071 6.70711C13.0976 6.31658 13.0976 5.68342 12.7071 5.29289C12.3166 4.90237 11.6834 4.90237 11.2929 5.29289L9 7.58579L6.70711 5.29289C6.31658 4.90237 5.68342 4.90237 5.29289 5.29289C4.90237 5.68342 4.90237 6.31658 5.29289 6.70711L7.58579 9L5.29289 11.2929C4.90237 11.6834 4.90237 12.3166 5.29289 12.7071C5.68342 13.0976 6.31658 13.0976 6.70711 12.7071L9 10.4142L11.2929 12.7071C11.6834 13.0976 12.3166 13.0976 12.7071 12.7071C13.0976 12.3166 13.0976 11.6834 12.7071 11.2929L10.4142 9L12.7071 6.70711Z" fill="white"/>
                </svg>
            </i>
        </span>
        `;

        var fieldHtml;
        if (field) {
            fieldHtml = fieldLabel;
            if (fieldHtml.length <= 3) {
                fieldHtml = `&nbsp;&nbsp;&nbsp;${fieldHtml}&nbsp;&nbsp;&nbsp;`;
            } else if (fieldHtml.length <= 4) {
                fieldHtml = `&nbsp;&nbsp;${fieldHtml}&nbsp;&nbsp;`;
            }
        } else {
            fieldHtml = iconHtml;
        }
        this.$el = $(`<div style="position:relative" class="sj-query-bar-filter-dropdown">
            <div class="sj-query-bar-filter-btn">
                ${this.settings.pinable ? pinSmallIconHtml : ''}
                ${mode != 'set' ? notSmallIconHtml : ''}
                <span class="${tooltip ? 'sj-query-bar-field-label' : ''}">${fieldHtml}${tooltip}</span><span class="sj-query-bar-filter-term-label"></span>
                ${field ? removeHtml : ''}
            </div>
            <div class="sj-query-bar-filter-list">
                <div class="sj-query-bar-filter-list-inner">
                    <ul data-mode="${this.mode}">
                        ${lis}
                    </ul>
                </div>
            </div>
        </div>`).attr('data-field', field || '_set');

        if (this.settings.pined) {
            this.$el.addClass('sj-query-bar-filter-pined');
        }
        if (this.state.pined) {
            this.$el.addClass('sj-query-bar-filter-pinable');
        }
        if (this.state.not) {
            this.$el.addClass('sj-query-bar-filter-noted');
        }

        if (this.mode == 'date') {
            this.$el.find('.sj-query-bar-filter-list-inner > ul')
                .attr('data-date-style', this.state.style)
                .find('.sj-query-bar-filter-date')
                .find('.sj-btn').addClass('sj-btn-light')
                .filter(`[data-date-style="${this.state.style}"]`).removeClass('sj-btn-light');
        }

        if (this.settings.pined) {
            this.$el.insertBefore(this.settings.$root.find('.sj-query-bar-filter-dropdown').eq(0));
        } else {
            this.$el.insertBefore(this.settings.$root.find('.sj-query-bar-placeholder'));
        }

        this.renderTermLabel();

        // click handler to not close dropdown in global click handler
        this.$el.on('click', e => {
            e.originalEvent.closeSjAc = false;
        });

        this.initHandlers();

        // toggle dropdown
        this.$el.on('click', '.sj-query-bar-filter-btn', e => {
            if (this.$el.hasClass('sj-query-bar-filter-dropdown-shown')) {
                this.hide();
            } else {
                if (this.$el.hasClass('sj-query-bar-filter-pinable')) {
                    return;
                }
                this.show();
            }
        });

        // toggle
        this.$el.on('click', '.sj-query-bar-toggle', e => {
            $(e.currentTarget).toggleClass('sj-query-bar-toggle-active');
        });

        // not
        this.$el.on('click', '.sj-query-bar-filter-not', e => {
            this.state.not = !this.state.not;
            $(e.currentTarget).toggleClass('sj-query-bar-filter-not-active');
            this.$el.toggleClass('sj-query-bar-filter-noted');

            if (this.queryBar.getPinFilter() == this) {
                var pinFilter = this.queryBar.getFilterForPin();
                if (pinFilter) {
                    pinFilter.state.not = this.state.not;

                    pinFilter.$el.find('.sj-query-bar-filter-not').toggleClass('sj-query-bar-filter-not-active');
                    pinFilter.$el.toggleClass('sj-query-bar-filter-noted');
                    if (pinFilter.state.not) {
                        pinFilter.$el.addClass('sj-query-bar-filter-header-all-disabled');
                    } else {
                        pinFilter.$el.removeClass('sj-query-bar-filter-header-all-disabled');
                    }
                }
            }

            var $allValues = this.$el.find('.sj-query-bar-filter-header-all');
            if (this.state.not) {
                $allValues.addClass('sj-query-bar-filter-header-all-disabled');
            } else {
                $allValues.removeClass('sj-query-bar-filter-header-all-disabled');
            }

            this.renderTerms();
            this.settings.onStateChange(this.settings.field, 'not');
        });

        // pin
        this.$el.on('click', '.sj-query-bar-filter-pin', e => {
            this.state.pined = !this.state.pined;
            this.$el.toggleClass('sj-query-bar-filter-pinable');
            if (this.state.pined) {
                this.queryBar.addFilter(this.settings.field, this.state, null, true, this.lastResponse);
            } else {
                this.queryBar.removeFilter(this.settings.field, true);
            }
        });

        // unpin
        this.$el.on('click', '.sj-query-bar-filter-pined-icon-svg', e => {
            this.queryBar.removeFilter(this.settings.field, true);
            var pinFilter = this.queryBar.filters.find(f => f.settings.field == this.settings.field);
            if (pinFilter) {
                pinFilter.state.pined = false;
                pinFilter.$el.removeClass('sj-query-bar-filter-pinable');
                pinFilter.show();
            }
        });

        // state select
        this.$el.on('change', 'select[data-state]', e => {
            var $el = $(e.currentTarget);
            var stateKey = $el.data('state');
            switch (stateKey) {
                case 'abs_per':
                    this.state[stateKey] = $el.val();
                    this.state.selected = {};
                    this.loadFacets();
                    this.renderTermLabel();
                    break;
                default:
                    this.state[stateKey] = $el.val();
                    this.settings.onStateChange(this.settings.field, 'dropdown');
                    this.renderTermLabel();
                    break;
            }
        });

        // state input blur
        this.$el.on('change', 'input[type="radio"][data-state], input[type="checkbox"][data-state]', e => {
            var $el = $(e.currentTarget);

            var stateKey = $el.data('state');
            switch (stateKey) {
                case 'val':
                    if (this.mode == 'text') {
                        this.state.val = $el.val();
                        if (this.state.fuzzy) {
                            this.settings.onStateChange(this.settings.field, 'search');
                            this.renderTermLabel();
                        }
                    } else {
                        var v = $el.val();
                        if (this.state[stateKey] !== v) {
                            this.state[stateKey] = $el.val();
                            this.settings.onStateChange(this.settings.field, 'search');
                            this.renderTermLabel();
                        }
                    }
                    break;
                default:
                    this.state[stateKey] = $el.val();
                    this.settings.onStateChange(this.settings.field, 'search');
                    this.renderTermLabel();
                    break;
            }
        });

        // state input blur
        this.$el.on('blur', 'input[data-state]', e => {
            var $el = $(e.currentTarget);

            var type = $el.prop('type');
            if (type == 'radio' || type == 'checkbox') {
                return;
            }

            var stateKey = $el.data('state');
            switch (stateKey) {
                case 'val':
                    if (this.mode == 'text') {
                        this.state.val = $el.val();
                        if (this.state.fuzzy) {
                            this.settings.onStateChange(this.settings.field, 'search');
                            this.renderTermLabel();
                        }
                    } else {
                        var v = $el.val();
                        if (this.state[stateKey] !== v) {
                            this.state[stateKey] = $el.val();
                            this.settings.onStateChange(this.settings.field, 'search');
                            this.renderTermLabel();
                        }
                    }
                    break;
                default:
                    this.state[stateKey] = $el.val();
                    this.settings.onStateChange(this.settings.field, 'search');
                    this.renderTermLabel();
                    break;
            }
        });

        // state input keydown
        this.$el.on('keydown', 'input[data-state]', e => {
            if (event['keyCode'] != 13) {
                return;
            }
            var $el = $(e.currentTarget);
            var stateKey = $el.data('state');
            switch (stateKey) {
                case 'val':
                    if (this.mode == 'text') {
                        this.state.val = $el.val();
                        if (this.state.fuzzy) {
                            this.settings.onStateChange(this.settings.field, 'search');
                            this.renderTermLabel();
                        }
                    } else {
                        this.state[stateKey] = $el.val();
                        this.settings.onStateChange(this.settings.field, 'search');
                        this.renderTermLabel();
                    }
                    break;
                default:
                    this.state[stateKey] = $el.val();
                    this.settings.onStateChange(this.settings.field, 'search');
                    this.renderTermLabel();
                    break;
            }
        });

        this.$el.on('mousemove', '.sj-query-bar-wizard-wrapper button', e => {
            var $wEl = $(e.currentTarget);
            var $tip = $wEl.find('span');
            var offset = $wEl.offset();
            $tip.css({
                top: (offset.top - 46 - $(document).scrollTop()) + 'px',
                left: (offset.left - 97) + 'px'
            });
        });

        this.$el.on('mousemove', '.sj-query-bar-addfield-wrapper button', e => {
            var $wEl = $(e.currentTarget);
            var $tip = $wEl.find('span');
            var offset = $wEl.offset();
            $tip.css({
                top: (offset.top - 46 - $(document).scrollTop()) + 'px',
                left: (offset.left - 40) + 'px'
            });
        });

        // state button
        this.$el.on('click', 'button[data-state]', e => {
            var $el = $(e.currentTarget);
            var stateKey = $el.data('state');
            switch (stateKey) {
                case 'exact':
                    this.state[stateKey] = !this.state[stateKey];
                    this.settings.onStateChange(this.settings.field, 'exact');
                    this.renderTermLabel();
                    break;
                case 'fuzzy':
                    this.state[stateKey] = !this.state[stateKey];
                    if (this.state[stateKey]) {
                        this.state.selected = {};
                        this.$el.find('.sj-query-bar-filter-item,.sj-query-bar-filter-header,.sj-query-bar-filter-other').remove();
                    } else {
                        this.loadFacets();
                    }
                    this.settings.onStateChange(this.settings.field, 'fuzzy');
                    this.renderTermLabel();
                    break;
                case 'addfield':
                    var $addFEl = this.$el.find('.sj-query-bar-addfield-wrapper');
                    var selectedFields = [];
                    Object.keys(this.state.selected || {}).forEach(k => {
                        if (k != 'set') {
                            selectedFields.push(k);
                        }
                    });
                    this.settings.addSelectedToTable(selectedFields);
                    $addFEl.addClass('sj-query-bar-addfield-active');
                    break;
                case 'wizard':
                    var $wEl = this.$el.find('.sj-query-bar-wizard-wrapper');
                    $wEl.addClass('sj-query-bar-wizard-loading');
                    this.settings.getQuery().then(q => {
                        this.getWizardFields(q, true, response => {
                            if (response.success && response.result && response.result.fields) {
                                var newFieldsMap = {};
                                response.result.fields.forEach(f => {
                                    newFieldsMap[f] = 1;
                                });

                                var changed = false;

                                // remove not used filters
                                /*
                                this.queryBar.filters = this.queryBar.filters.filter(f => {
                                    if (f.settings.field && !newFieldsMap[f.settings.field]) {
                                        f.remove();
                                        delete this.state.selected[f.settings.field];
                                        changed = true;
                                        return false;
                                    }
                                    return true;
                                });
                                */

                                // add new filters
                                response.result.fields.forEach(f => {
                                    if (!this.state.selected[f]) {
                                        this.state.selected[f] = true;
                                        this.queryBar.addFilter(f, null, true);
                                        changed = true;
                                    }
                                });

                                this.queryBar.runSearch(false); // update query in state, but don't run search and not saving state
                                this.queryBar.filtersListChanged(); // update filters in state and save state
                                this.queryBar.runSearch(); // run search and save state (but state will be the same, so hash will not change second time)

                                this.queryBar.redrawPlaceholder();
                                this.queryBar.updateCopyStatus();

                                this.renderTerms(this.state.val);
                                $wEl.removeClass('sj-query-bar-wizard-loading').addClass('sj-query-bar-wizard-active');
                            } else {
                                $wEl.removeClass('sj-query-bar-wizard-loading');
                            }
                        }, err => {
                            $wEl.removeClass('sj-query-bar-wizard-loading');
                        });
                    });
                    break;
            }
        });

        // date mode changed
        this.$el.on('click', '.sj-query-bar-filter-date [data-date-style]', e => {
            var $el = $(e.currentTarget);
            var style = $el.data('dateStyle');

            this.state.style = style;
            $el.closest('ul')
                .attr('data-date-style', this.state.style)
                .find('.sj-query-bar-filter-date')
                .find('.sj-btn').addClass('sj-btn-light')
                .filter(`[data-date-style="${this.state.style}"]`).removeClass('sj-btn-light');

            if (this.state.style == 'absolute') {
                this.loadFacets();
            }
            this.settings.onStateChange(this.settings.field, 'datestyle');
            this.renderTermLabel();
        });

        // remove filter
        this.$el.find('.sj-query-bar-filter-remove').on('click', e => {
            this.queryBar.removeFilter(this.settings.field);
        });

        // tree expand/collapse
        this.$el.on('click', '.sj-query-bar-filter-tree-arrow', e => {
            var $el = $(e.target);
            var $li = $el.closest('li');
            var item = $li.data('node');
            e.stopPropagation();
            this.loadChildren(item, $li);
        });
    }

    loadChildren(item, $li) {
        if (item.children) {
            // expand with already loaded items
            if (item.expanded) {
                delete item.expanded;
                $li.removeClass('sj-query-bar-filter-item-expanded');
                this.queryBar.refreshUI();
                return;
            }

            // collapse
            if (!item.expanded) {
                item.expanded = true;
                $li.addClass('sj-query-bar-filter-item-expanded');
                this.queryBar.refreshUI();
                return;
            }
        }

        var $arrowEl = $li.find('.sj-query-bar-filter-tree-arrow').eq(0);
        var field = this.settings.field;
        var prefix = this.buildTreeItemPath(item);

        // for the facets if there is a , in the @tree operator param, you have to quote the whole param in "
        // otherwise API thinks you are doing multiple facets because we use , as special delimit character
        if (prefix && prefix.indexOf(',') != -1) {
            prefix = `"${prefix}"`;
        }

        var facet = this.formatFacet(prefix);

        $arrowEl.addClass('sj-query-bar-filter-tree-arrow-loading');
        this.requestFacets(facet, field, term => {
            term.terms.forEach(x => {
                x.prefix = prefix;
                x.parent = item;
            });
            item.children = term.terms;
            item.other = term.other;
            item.expanded = true;

            $arrowEl.removeClass('sj-query-bar-filter-tree-arrow-loading');
            this.renderTerms(prefix);
            this.queryBar.refreshUI();
        }, err => {
            // $dropdown.removeClass('sj-query-bar-filter-dropdown-loading');
        }, {
            showMore: item.showMore
        });
    }

    toggleTerm(e) {
        super.toggleTerm(e);

        this.renderTermLabel();
        if (!this.settings.field) {
            var $wEl = this.$el.find('.sj-query-bar-wizard-active');
            if ($wEl.length) {
                this.checkWizardState();
            }
            var $addFEl = this.$el.find('.sj-query-bar-addfield-wrapper');
            if ($addFEl.length) {
                this.checkAddFieldState();
            }
        }
        this.refreshPositionAndSize();
    }

    wizardFields = {};
    getWizardFields(q, makeApiCall, success, error) {
        if (this.wizardFields[q]) {
            success(this.wizardFields[q]);
            return;
        }
        if (!makeApiCall) {
            error();
            return;
        }
        this.settings.request.call({
            method: 'POST',
            url: "/api/1/processors/sjextensions/call/field_chooser_wizard",
            data: JSON.stringify({
                settings: {
                    query: q || ''
                }
            }),
            success: (r) => {
                this.wizardFields[q] = r;
                success(r);
            },
            error: error
        });
    }

    checkAddFieldState() {
        var $addFEl = this.$el.find('.sj-query-bar-addfield-wrapper');
        if (!this.settings.field && this.settings.addSelectedToTable && this.settings.getTableFields) {
            var tableFields = this.settings.getTableFields() || [];
            var tableFieldsMap = {};
            tableFields.forEach(f => {
                tableFieldsMap[f] = true;
            });

            var allSelected = true;
            var anySelected = false;
            Object.keys(this.state.selected || {}).forEach(k => {
                if (k != 'set') {
                    anySelected = true;
                    if (!tableFieldsMap[k]) {
                        allSelected = false;
                    }
                }
            });

            if (anySelected && !allSelected) {
                $addFEl.addClass('sj-query-bar-addfield-shown');
            } else {
                $addFEl.removeClass('sj-query-bar-addfield-shown');
            }

            if (allSelected) {
                $addFEl.addClass('sj-query-bar-addfield-active');
            } else {
                $addFEl.removeClass('sj-query-bar-addfield-active');
            }
        }
    }

    checkWizardState() {
        var $wEl = this.$el.find('.sj-query-bar-wizard-wrapper');
        $wEl.addClass('sj-query-bar-wizard-loading').removeClass('sj-query-bar-wizard-active');
        this.settings.getQuery().then(q => {
            this.getWizardFields(q, false, response => {
                if (response.success && response.result && response.result.fields) {
                    var hasFieldsToAdd = false;
                    response.result.fields.forEach(f => {
                        if (!this.state.selected[f]) {
                            hasFieldsToAdd = true;
                        }
                    });
                    if (!hasFieldsToAdd) {
                        $wEl.addClass('sj-query-bar-wizard-active');
                    }
                    $wEl.removeClass('sj-query-bar-wizard-loading');
                } else {
                    $wEl.removeClass('sj-query-bar-wizard-loading');
                }
            }, err => {
                $wEl.removeClass('sj-query-bar-wizard-loading');
            });
        });
    }

    clearWizardState() {
        var $wEl = this.$el.find('.sj-query-bar-wizard-wrapper');
        $wEl.removeClass('sj-query-bar-wizard-loading sj-query-bar-wizard-active');
    }

    clearAddFieldState() {
        var $addFEl = this.$el.find('.sj-query-bar-addfield-wrapper');
        $addFEl.removeClass('sj-query-bar-addfield-active');
        this.checkAddFieldState();
    }

    renderTermLabel() {
        var termLabel = this.getTermLabel();
        var $btn = this.$el.find('.sj-query-bar-filter-btn');
        var $el = this.$el.find('.sj-query-bar-filter-term-label');
        $el.empty();

        if (this.mode == 'set') {
            if (Object.keys(this.state.selected).length) {
                $btn.removeClass('sj-query-bar-filter-btn-blank');
                var hasValues = false;
                this.queryBar.filters.forEach(f => {
                    if (f.getBox()) {
                        hasValues = true;
                    }
                });
            } else {
                $btn.addClass('sj-query-bar-filter-btn-blank');
            }
        } else {
            if (termLabel) {
                var $inner = $('<i>').text(termLabel);
                if (this.mode == 'text' || this.mode == 'tree' || (this.mode == 'date' && this.state.style == 'absolute')) {
                    $el.append(':&nbsp;');
                }
                $el.append($inner);
                $btn.removeClass('sj-query-bar-filter-btn-blank');
                if ((this.mode == 'text' && this.state.fuzzy) || (this.mode == 'tree' && this.state.exact)) {
                    $btn.addClass('sj-query-bar-filter-btn-unstr');
                } else {
                    $btn.removeClass('sj-query-bar-filter-btn-unstr');
                }
            } else {
                $btn.addClass('sj-query-bar-filter-btn-blank');
                $btn.removeClass('sj-query-bar-filter-btn-unstr');
            }
        }

        if (this.settings.pined) {
            var pinFilter = this.queryBar.filters.find(f => f.settings.field == this.settings.field && !f.settings.pined);
            if (pinFilter) {
                pinFilter.renderTermLabel();
            }
        }
    }

    isUnstructured() {
        if (this.mode == 'text' && this.state.fuzzy) {
            return true;
        }
    }

    getTermLabel() {
        switch (this.mode) {
            case 'text':
                if (this.state.fuzzy) {
                    if (this.state.val) {
                        return `${this.state.val}`;
                    }
                } else {
                    var selected = Object.keys(this.state.selected);
                    if (selected.length) {
                        var textLabel = selected.map(v => {
                            return this.annotationValues[v] || v || '""';
                        }).join(', ');
                        if (textLabel.length > 50) {
                            textLabel = `${textLabel.substr(0, 50)}... (${selected.length})`;
                        }
                        return textLabel;
                    }
                }
                break;
            case 'tree':
                var selected = Object.keys(this.state.selected);
                if (selected.length) {
                    var textLabel = selected.map(v => {
                        var res = v;
                        if (this.state.selected[v] === false) {
                            res = 'NOT ' + v;
                        }
                        return res;
                    }).join(', ');
                    if (textLabel.length > 50) {
                        textLabel = `${textLabel.substr(0, 50)}... (${selected.length})`;
                    }
                    return textLabel;
                }
                break;
            case 'num':
                if (this.state.val || this.state.val === 0) {
                    return `${this.state.op}${this.state.val}`;
                }
                break;
            case 'date':
                switch (this.state.style) {
                    case 'absolute':
                        var selected = Object.keys(this.state.selected);
                        if (selected.length) {
                            var absLabel = selected.join(', ');
                            if (absLabel.length > 50) {
                                absLabel = `${absLabel.substr(0, 50)}... (${selected.length})`;
                            }
                            return absLabel;
                        }
                        break;
                    case 'relative':
                        if (this.state.rel_val || this.state.rel_val === 0) {
                            var val = this.state.rel_val;
                            if (this.state.rel_sign == '-') {
                                val *= -1;
                            }
                            return `${this.state.rel_op}${val}${this.state.rel_per}`;
                        }
                        break;
                }
                break;
        }
    }

    getBox() {
        if (this.settings.field) {
            if (this.settings.pined) {
                return;
            }
            switch (this.mode) {
                case 'text':
                    if (this.state.fuzzy) {
                        if (this.state.val) {
                            return {
                                type: 'text',
                                field: this.settings.field,
                                op: ':',
                                value: this.state.val,
                                not: this.state.not
                            };
                        }
                    } else {
                        var selected = this.state.selected;
                        var selectedVals = Object.keys(this.state.selected);
                        if (selectedVals.length) {
                            return {
                                type: 'text',
                                field: this.settings.field,
                                op: '=',
                                value: selectedVals,
                                not: this.state.not
                            };
                        }
                    }
                    break;
                case 'num':
                    if (this.state.val || this.state.val === 0) {
                        return {
                            type: 'num',
                            field: this.settings.field,
                            op: this.state.op,
                            value: this.state.val
                        };
                    }
                    break;
                case 'date':
                    var dateRes = {
                        type: 'date',
                        field: this.settings.field,
                        style: this.state.style,
                        not: this.state.not
                    };

                    /*
                    rel_op: '<',
                    rel_val: 1,
                    rel_per: 'd',

                    abs_per: 'd',
                    selected: { }
                    */

                    switch (this.state.style) {
                        case 'absolute':
                            var abs_selected = Object.keys(this.state.selected);
                            if (!abs_selected.length) {
                                return;
                            }
                            Object.assign(dateRes, {
                                period: this.state.abs_per,
                                value: abs_selected
                            });
                            break;
                        case 'relative':
                            if (!this.state.rel_val) {
                                return;
                            }
                            var val = this.state.rel_val - 0;
                            Object.assign(dateRes, {
                                op: this.state.rel_op,
                                sign: this.state.rel_sign,
                                value: Math.abs(val),
                                period: this.state.rel_per
                            });
                            break;
                    }

                    return dateRes;
                case 'tree':
                    var checked = [];
                    var unchecked = [];
                    Object.keys(this.state.selected).forEach(k => {
                        switch (this.state.selected[k]) {
                            case true:
                                checked.push(k);
                                break;
                            case false:
                                unchecked.push(k);
                                break;
                        }
                    });
                    if (checked.length || unchecked.length) {
                        return {
                            type: 'tree',
                            op: this.state.exact ? '=' : ':',
                            field: this.settings.field,
                            value: checked,
                            except: unchecked,
                            not: this.state.not
                        };
                    }
                    break;
            }
        }
    }

    hide() {
        this.$el.removeClass('sj-query-bar-filter-dropdown-shown');
        document.removeEventListener('click', this.globalClickEventHandler);
    }

    refreshPositionAndSize() {
        var $ul = this.$el.find('.sj-query-bar-filter-list-inner > ul');

        var isTheme = !!this.$el.parents('.sj-theme-shooju-22').length;

        // align dropdown to the left when there is not enough space at the right
        var $btn = this.$el.find('.sj-query-bar-filter-btn');
        var isMultiline = $btn.height() > (isTheme ? 32 : 22);
        $ul.css({
            left: ''
        });
        if (isMultiline) {
            this.$el.removeClass('sj-query-bar-filter-dropdown-right');
            $ul.css({
                left: -($ul.offset().left - $btn.closest('.sj-query-bar').offset().left)
            });
        } else {
            var $container = this.$el.closest('.sj-query-bar');
            var offsetLeft = $btn.offset().left; // - $container.offset().left;
            if ($container.width() - offsetLeft - $btn.width() < 400) {
                this.$el.addClass('sj-query-bar-filter-dropdown-right');
            } else {
                this.$el.removeClass('sj-query-bar-filter-dropdown-right');
            }
        }

        // until bottom of screen - 100px
        var maxHeight = window.innerHeight - $ul.offset().top - 100;
        if (maxHeight < 400) {
            maxHeight = 400;
        }
        $ul.css('max-height', maxHeight);
    }

    show(leaveActiveFilter = false): Promise<void> {
        if (!leaveActiveFilter) {
            this.queryBar.hideFilters();
        }
        var result = this.loadFacets();

        if (!this.settings.field && this.wizard) {
            //this.checkWizardState();
            this.clearWizardState();
        }

        if (!this.settings.field && this.settings.addSelectedToTable) {
            this.clearAddFieldState();
        }

        var $ul = this.$el.find('.sj-query-bar-filter-list-inner > ul');

        this.refreshPositionAndSize();

        // focus first input
        this.$el.find('.sj-query-bar-filter-search input[type="text"]').eq(0).trigger('focus');

        this.globalClickEventHandler = (e) => {
            if (e.closeSjAc !== false) {
                this.hide();
            }
        };
        document.removeEventListener('click', this.globalClickEventHandler);
        document.addEventListener('click', this.globalClickEventHandler);

        return result;
    }

    remove() {
        if (this.$el) {
            this.$el.remove();
        }
        this.removed = true;
    }

    loadFacets(): Promise<void> {
        var $dropdown = this.$el;
        var field = this.settings.field;

        if (this.ajaxRequests[field]) {
            this.ajaxRequests[field].abort();
        }

        $dropdown.addClass('sj-query-bar-filter-dropdown-shown sj-query-bar-filter-dropdown-loading');
        $dropdown.removeClass('sj-query-bar-filter-dropdown-empty');

        $dropdown.find('.sj-query-bar-filter-item,.sj-query-bar-filter-header,.sj-query-bar-filter-other').remove();

        var $drList;
        if (this.state.pined) {
            $drList = $dropdown.find('.sj-query-bar-filter-list-inner > ul').eq(0);
            $drList.width($drList.width() + 'px');
        }

        this.queryBar.refreshUI();

        var mode = this.mode;

        var ignoreFacets = mode == 'num';
        if (!ignoreFacets) {
            if (mode == 'date' && this.state.style == 'relative') {
                ignoreFacets = true;
            }
            if (mode == 'text' && this.$el.find('.sj-query-bar-toggle').hasClass('sj-query-bar-toggle-active')) {
                ignoreFacets = true;
            }
        }

        if (ignoreFacets) {
            $dropdown.removeClass('sj-query-bar-filter-dropdown-loading');
            if ($drList) {
                $drList.width('');
            }
            return;
        }

        var facetFilter = $dropdown.find('input').val();

        var facet = this.formatFacet(facetFilter, facetFilter ? true : false);
        return new Promise(resolve => {
            this.requestFacets(facet, field, term => {
                $dropdown.removeClass('sj-query-bar-filter-dropdown-loading');

                // remove first part (but not for sid)
                if (facetFilter && this.mode == 'tree' && this.settings.field !== 'sid') {
                    term.terms.forEach(x => {
                        var parts = x.term.split('\\');
                        parts.shift();
                        if (!parts[parts.length - 1]) {
                            parts.pop();
                        }
                        x.term = parts.join('\\');
                    });
                }

                this.term = term;
                var hasResults = this.renderTerms(facetFilter);
                if (!hasResults) {
                    $dropdown.addClass('sj-query-bar-filter-dropdown-empty');
                }
                if ($drList) {
                    $drList.width('');
                }
                this.queryBar.refreshUI();
                resolve();
            }, err => {
                $dropdown.removeClass('sj-query-bar-filter-dropdown-loading');
                if ($drList) {
                    $drList.width('');
                }
                this.queryBar.refreshUI();
                resolve();
            }, {
                showMore: this.showMore
            });
        });
    }

    selectionChanged($el) {
        super.selectionChanged($el);
        this.renderTermLabel();
    }

    handleRequestFacets(facet, response, success) {
        if (response.success) {
            if (response.facets && response.facets[facet] && response.facets[facet].terms) {
                var term = response.facets[facet];
                switch (this.mode) {
                    case 'set':
                        term.terms.forEach(t => {
                            if (endsWith(t.term, '_obj') || endsWith(t.term, '_geo')) {
                                t.disabled = true;
                            }
                        });
                        break;
                    case 'tree':
                        break;
                }
                success(term);
                return;
            }
        }
        success(null);
    }

    lastResponse: any = {};

    requestFacetsTimeout: any;
    requestFacets(facet, apiKey, success, error, options?: any) {

        clearTimeout(this.requestFacetsTimeout);
        this.requestFacetsTimeout = setTimeout(() => {
            this.settings.getQuery().then(q => {

                var request = {
                    facets: facet,
                    max_facet_values: this.mode == 'date' ? 500 : (options && options.showMore ? this.maxFacets : this.minFacets),
                    per_page: 0,
                    query: q
                };

                var key = JSON.stringify(request);

                if (this.lastResponse && this.lastResponse.response && this.lastResponse.key == key) {
                    this.lastResponse.response.then(r => {
                        this.handleRequestFacets(facet, r, (data) => {
                            success(data);
                        });
                    }, err => {
                        error(err);
                    })
                    return;
                }

                var promise = new Promise((resolve, reject) => {
                    this.ajaxRequests[apiKey] = this.settings.request.call({
                        url: "/api/1/series",
                        data: request,
                        success: response => {
                            resolve(response);
                            this.handleRequestFacets(facet, response, (data) => {
                                success(data);
                            });
                        },
                        error: (err) => {
                            reject(err);
                            error(err);
                        }
                    });
                });

                // define handlers to not throw unhandled promise rejection
                promise.then(resp => {
                    // OK
                }, err => {
                    // Error
                });

                this.lastResponse.key = key;
                this.lastResponse.response = promise;

            });
        }, 150);
    }

}

export default QueryBarFilter;
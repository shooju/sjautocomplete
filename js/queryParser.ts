import { escapeQueryText, endsWith, getFieldMode } from './helpers';

var splitRespectingQuotes = function (str, by) {
    var badQuoteParts = str.split(by);
    var parts = [];
    var openQuoted = false;
    var openParens = false;

    badQuoteParts.forEach(function (p) {
        if (openQuoted || openParens) { //part of last quoted
            parts[parts.length - 1] = parts[parts.length - 1] + ' ' + p;
        } else { //non-quoted
            parts.push(p);
        }

        var parensCounter = 0;
        var quotedCounter = 0;
        parts[parts.length - 1].split('').forEach(x => {
            switch (x) {
                case '(':
                    if (!(quotedCounter % 2)) {
                        parensCounter++;
                    }
                    break;
                case ')':
                    if (!(quotedCounter % 2)) {
                        parensCounter--;
                    }
                    break;
                case '"':
                    quotedCounter++;
                    break;
            }
        });

        openParens = !!parensCounter;
        openQuoted = !!(quotedCounter % 2);
    });

    return parts;
};

var extractParened = function (p, options) {
    options = options || {};

    var parenedStart = p.length > 2 && p.substr(0, 1) == '(';
    var parenedAllFieldsStart = options.supportAllFields && !parenedStart && p.length > 4 && p.substr(0, 3) == '*:(';
    var success = (parenedStart || parenedAllFieldsStart) && p.substr(p.length - 1, p.length) == ')';
    if (success) {
        return {
            val: p.substr(parenedAllFieldsStart ? 3 : 1, p.length - (parenedAllFieldsStart ? 4 : 2)),
            allFields: parenedAllFieldsStart
        };
    }
    return null;
}
var extractQuoted = function (p) {
    if (p.substr(0, 1) == '"' && p.substr(p.length - 1, p.length) == '"') {
        return p.substr(1, p.length - 2);
    }
    return p;
}
var parseValOrVals = function (value, options) {
    options = options || {};

    var parened = extractParened(value, options);
    if (parened) {
        return splitRespectingQuotes(parened.val, ',').map(function (e) {
            return extractQuoted(e);
        });
    }
    return extractQuoted(value);
};
var _queryToBoxes = function (query, options) {
    options = options || {};

    //now try to get basic boxes structure
    var boxes = [];
    if (!query) {
        return boxes;
    }

    splitRespectingQuotes(query, ' ').forEach(function (p) {
        var period_date = p.match(/^\"?([a-z0-9_\.]+_date)@period:([yqMwdhm])\"?=(.*)/);
        if (period_date) {
            boxes.push({
                'type': 'date',
                'field': period_date[2],
                'style': 'absolute',
                'period': period_date[3],
                'value': parseValOrVals(period_date[4], options)
            });
        } else { //not _date@period
            var parened = extractParened(p, options);
            if (parened) { //free
                var pbox = {
                    'type': 'nofield',
                    'value': parened.val
                };
                if (parened.allFields) {
                    pbox['fmode'] = 'all';
                }
                boxes.push(pbox);
            } else { //field-val
                var field_val = p.match(/^([a-z0-9_\.]+)([\:\=\<\>]+)(.*)/);
                if (!field_val) {
                    boxes.push({
                        'type': 'nomatch',
                        'value': p
                    });
                    return;
                }

                var value = field_val[3]
                var b: any = {
                    field: field_val[1],
                    op: field_val[2]
                };

                var fieldType = getFieldMode(b.field);
                switch (fieldType) {
                    case 'date':
                        b.type = 'date';
                        b.style = 'relative';
                        var val = value.match(/^([-+])([0-9]+)([yqMwdhm])$/);
                        b.sign = val[1];
                        b.value = val[2];
                        b.period = val[3];
                        break;
                    case 'num':
                        b.type = 'num';
                        b.value = value;
                        break;
                    case 'tree':
                    case 'text':
                        b.type = fieldType;
                        var textVal = parseValOrVals(value, options);
                        // can be `xxx=""` so value should be array with empty string
                        if (typeof textVal === 'string') {
                            textVal = [textVal];
                        }
                        b.value = textVal;
                        break;
                }

                boxes.push(b);
            }
        }
    });

    var boxGroups = {};
    boxes.forEach((box, i) => {
        if (box.type == 'nomatch') {
            if (box.value.toLowerCase() === 'not') {
                if (boxes[i + 1]) {
                    boxes[i + 1].not = true;
                    box.type = 'delete';
                    return;
                }
            }
            box.type = 'nofield';
        }
        box.index = i;

        var key = box.field;
        if (!boxGroups[key]) {
            boxGroups[key] = [];
        }
        boxGroups[key].push(box);
    });

    var rBoxes = [];
    for (var key in boxGroups) {
        var boxGroupItems = boxGroups[key];
        if (boxGroupItems.length > 1) {
            var typesMap = {};
            boxGroupItems.forEach(b => {
                typesMap[b.type] = 1;
            });
            var types = Object.keys(typesMap);
            if (types.length == 1) {
                switch (types[0]) {
                    case 'nofield':
                        rBoxes.push({
                            type: 'nofield',
                            value: boxGroupItems.map(b => b.value).filter(v => v).join(' '),
                            index: boxGroupItems[0].index
                        });
                        break;
                    case 'tree':
                        var checked = [];
                        var unchecked = [];
                        boxGroupItems.forEach(b => {
                            var arr = b.not ? unchecked : checked;
                            if (Array.isArray(b.value)) {
                                b.value.forEach(v => {
                                    arr.push(v);
                                });
                            } else {
                                arr.push(b.value);
                            }
                        });
                        rBoxes.push({
                            type: 'tree',
                            field: key,
                            op: boxGroupItems[0].op,
                            value: checked,
                            except: unchecked,
                            index: boxGroupItems[0].index
                        });
                        break;
                    default:
                        boxGroupItems.forEach(b => {
                            rBoxes.push(b);
                        });
                        break;
                }
            } else {
                boxGroupItems.forEach(b => {
                    rBoxes.push(b);
                });
            }
        } else {
            rBoxes.push(boxGroupItems[0]);
        }
    }

    rBoxes.sort((a, b) => {
        return a.index > b.index ? 1 : -1;
    });

    return rBoxes;
}
var queryToBoxes = function (query, options) {
    options = options || {};

    try {
        return _queryToBoxes(query, options);
    } catch (e) {
        console.log(e);
        return null;
    }
}
var valToString = function (v) {
    if (typeof (v) == 'object') {
        if (Array.isArray(v) && v.length == 1) {
            return escapeQueryText(v[0]);
        }
        return '(' + v.map(function (e) {
            return escapeQueryText(e);
        }) + ')';
    } else {
        return escapeQueryText(v);
    }
}
var boxesToQuery = function (boxes) {
    return boxes.map(function (b) {
        var notPart = `${b.not ? 'NOT ' : ''}`;
        if (b.type == 'nofield') {
            if (b.value) {
                var bres = '(' + b.value + ')';
                if (b.fmode == 'all') {
                    bres = `*:${bres}`;
                }
                return bres;
            }
        } else if (b.type == 'text') {
            return `${notPart}${escapeQueryText(b.field)}${b.op}${valToString(b.value)}`;
        } else if (b.type == 'num') {
            return `${notPart}${escapeQueryText(b.field)}${b.op}${b.value}`;
        } else if (b.type == 'date' && b.style == 'absolute') {
            return `${notPart}${escapeQueryText(b.field + '@period:' + b.period)}=${valToString(b.value)}`;
        } else if (b.type == 'date' && b.style == 'relative') {
            return `${notPart}${escapeQueryText(b.field)}${b.op}${b.sign}${b.value}${b.period}`;
        } else if (b.type == 'tree') {
            var parts = [];
            if (b.value && b.value.length) {
                parts.push(`${escapeQueryText(b.field)}${b.op}${valToString(b.value)}`);
            }
            var hasExcept = false;
            if (b.except && b.except.length) {
                hasExcept = true;
                parts.push(`NOT ${escapeQueryText(b.field)}${b.op}${valToString(b.except)}`);
            }
            return `${notPart}${notPart && hasExcept ? '(' : ''}${parts.join(' ')}${notPart && hasExcept ? ')' : ''}`;
        }
    }).filter(x => x).join(' ');
}

/*
bquery = 'field="value(" field_a=value field_b="hey  there" field_c=(value1,"value 2") field_d:hey field_e:hey field_f_num<12 field_g_date<-1M field_h_num<=44 field_i_date@period:M=2018-10 field_j_date@period:M=(2018-10,2018-11) (my text)';
var bq=queryToBoxes(bquery);
bq.forEach(function(p){outJSON(p)});
out('---');
queryToBoxes('(my text) field=(value1,"value 2")').forEach(function(p){outJSON(p)});
out('---');
queryToBoxes('field:hey (my text)').forEach(function(p){outJSON(p)});
out('---');
out(queryToBoxes('field:hey hello world')==null);
out(queryToBoxes('not field:hey')==null);
out(queryToBoxes('field:hey (hello world)@operator')==null);
out('---');
out(boxesToQuery(bq));
out(boxesToQuery(bq)==bquery);

bquery = 'ataglance_obj.fte_num>5 description=("API Chaya","Alliance of People with disAbilities","Alpha Supported Living Services","American Friends Service Committee - West Region","American Red Cross Western Washington Chapters")';
var bq=queryToBoxes(bquery);
bq.forEach(function(p){outJSON(p)});

*/

export { queryToBoxes, boxesToQuery };
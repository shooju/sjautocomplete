import { tooltip, getFieldMode, Request, endsWith, trim, $, suggestions, constants } from './helpers';
import { queryToBoxes, boxesToQuery } from './queryParser';
import QueryBarFilter from './queryBarFilter';
import Annotations, { baseFieldNamesMap } from './annotations';

class QueryBar {
    request: any;

    $rootEl: any;
    $innerEl: any;
    $acEl: any;
    $children: any;
    $bottomEl: any;
    $statusEl: any;
    $actionsEl: any;
    $msgEl: any;
    $filtersEl: any;
    $filtersContainer: any;

    autocomplete: any;
    filters: any;

    annotations: any;

    // unique id for some unique elements (e.g. part of radio group)
    id: string;

    constructor(private domElement, private settings) {
    }
    init() {
        var self = this;
        this.$rootEl = $(this.domElement);
        this.$innerEl = $('<div class="sj-query-bar sj-section-box"></div>');
        this.$acEl = $('<div class="sj-query-bar-autocomplete"></div>');
        this.$acEl.appendTo(this.$innerEl);

        // unique id for some unique elements
        this.id = 'sjqb-' + Math.round(Math.random() * 10000).toString(16);

        this.request = new Request(this.settings.autocomplete.server, this.settings.autocomplete.get_credentials);

        if (this.settings.fields_service) {
            this.annotations = this.settings.fields_service;
        } else {
            this.annotations = new Annotations(this.request);
        }

        this.settings.autocomplete = this.settings.autocomplete || {};
        this.settings.autocomplete.classNames = $.extend({}, constants, this.settings.autocomplete.classNames);

        this.$children = this.$rootEl.children();
        if (this.$children.length) {
            this.$acEl.append(this.$children);
        }

        this.$rootEl.attr('data-sj-widget', 'sj-query-bar')
            .attr('sj-qb-mode', this.settings.mode || 'simple');

        var enableLayouts = this.settings.mode == 'advanced' && this.settings.enableLayouts;

        if (this.settings.mode == 'advanced' || this.settings.mode == 'minimal') {
            this.$bottomEl = $('<div class="sj-query-bar-bottom"></div>');

            if ((this.settings.autocomplete || {}).suggest) {
                this.$bottomEl.append(suggestions(this.settings.autocomplete.classNames));
            }

            this.$statusEl = $('<div class="sj-query-bar-status"></div>');
            this.$bottomEl.append(this.$statusEl);

            var docSvg = `<svg width="12" height="12" xmlns="http://www.w3.org/2000/svg">
                <path d="M6.00025 0C2.68751 0 0 2.68412 0 5.99708C0 9.31321 2.68751 12.0005 6.00022 12.0005C9.31477 12.0005 12 9.31319 12 5.99708C12 2.68412 9.31477 0 6.00025 0ZM6.42783 9.33578C6.28847 9.4598 6.12696 9.52213 5.94401 9.52213C5.75471 9.52213 5.5896 9.46091 5.44865 9.33825C5.30749 9.21582 5.23677 9.04439 5.23677 8.82414C5.23677 8.62876 5.30521 8.46431 5.44165 8.33104C5.57809 8.19777 5.74546 8.13113 5.94401 8.13113C6.13939 8.13113 6.30384 8.19777 6.43733 8.33104C6.5706 8.46431 6.63746 8.62874 6.63746 8.82414C6.63721 9.04122 6.56741 9.21179 6.42783 9.33578ZM8.16551 5.02783C8.05844 5.22638 7.93128 5.39758 7.78378 5.54194C7.63672 5.68627 7.37224 5.92886 6.99048 6.26993C6.88522 6.36616 6.80052 6.45064 6.73705 6.52336C6.67359 6.59633 6.62615 6.66294 6.5952 6.72349C6.56402 6.78402 6.54009 6.84457 6.52314 6.90509C6.50619 6.96539 6.48068 7.07179 6.44611 7.22379C6.3874 7.54636 6.20285 7.70761 5.89271 7.70761C5.73143 7.70761 5.5959 7.65498 5.48545 7.5495C5.37543 7.44401 5.32055 7.28748 5.32055 7.07968C5.32055 6.81925 5.36098 6.59358 5.44162 6.40273C5.52182 6.21186 5.62911 6.04449 5.76238 5.90016C5.89588 5.75583 6.07567 5.58459 6.30222 5.38605C6.50077 5.21234 6.64421 5.08135 6.73252 4.99301C6.82106 4.90448 6.89539 4.806 6.95569 4.69757C7.01646 4.58891 7.04628 4.47123 7.04628 4.34407C7.04628 4.09584 6.95436 3.88668 6.76959 3.71614C6.58505 3.54561 6.34696 3.46021 6.05535 3.46021C5.71406 3.46021 5.46288 3.54627 5.3016 3.71839C5.14032 3.89051 5.00413 4.14394 4.89231 4.47893C4.7866 4.82951 4.58647 5.00477 4.29214 5.00477C4.11843 5.00477 3.97185 4.94355 3.85236 4.82111C3.73309 4.69868 3.67346 4.5661 3.67346 4.42335C3.67346 4.1288 3.76811 3.83019 3.95718 3.52774C4.14647 3.22529 4.42249 2.97478 4.78546 2.77646C5.14821 2.57791 5.57176 2.47851 6.05535 2.47851C6.50508 2.47851 6.90195 2.56163 7.24619 2.72766C7.59043 2.89345 7.8565 3.11911 8.04421 3.40461C8.2317 3.68988 8.32565 4.00003 8.32565 4.33501C8.32609 4.5982 8.27257 4.82928 8.16551 5.02783Z" fill="#D0D4D8"/>
            </svg>
            `;

            /*
                Add operators – adds operator to end - @
                Run query - runs query - enter symbol
                Turn query into expression – only show if doesn't start with; does ={{ xxx }} – alt-e
                Open visual editor – opens visual editor – alt-v
            */

            this.$actionsEl = $('<div class="sj-query-bar-actions"></div>');

            var switchHtml = `<div class="sj-query-bar-switch-wrapper">
                <span class="sj-querybar-bar-switch">
                    <span data-mode="sjql">
                        <span data-msg="switch">Switch to Query Builder</span>
                        <span data-msg="error">SjQL too complex to switch to Query Builder</span>
                    </span>
                    <span data-mode="filters">Switch to SjQL</span>
                </span>
            </div>`;

            var hotActions = '';
            if (this.settings.autocomplete.extra_get_params && this.settings.autocomplete.extra_get_params.types) {
                var contexts = (this.settings.autocomplete.extra_get_params.types || '').split(',');
                if (contexts.indexOf('@') != -1) {
                    // only show in points context
                    hotActions = `
                        <div class="sj-query-bar-hot-actions">
                            <ul>
                                <li data-action="operator">
                                    Add operators
                                    <span>@</span>
                                </li>
                                <li data-action="expression">
                                    Turn query into expression
                                    <span>alt-e</span>
                                </li>
                            </ul>
                        </div>
                    `;
                }
            }

            this.$actionsEl.html(`
                ${enableLayouts ? switchHtml : ''}
                <div class="sj-query-bar-copy-sjql">
                    <span class="sj-query-bar-icon">
                        <span class="sj-query-bar-copy-tooltip">
                            <span data-msg="copy">Copy SjQL to clipboard</span>
                            <span data-msg="copied">Copied!</span>
                            <span data-msg="empty">Query is empty</span>
                        </span>
                        <svg width="16" height="16" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" clip-rule="evenodd" d="M6 1C5.44772 1 5 1.44772 5 2C5 2.55228 5.44772 3 6 3H12V11C12 11.5523 12.4477 12 13 12C13.5523 12 14 11.5523 14 11V2C14 1.44772 13.5523 1 13 1H6ZM3 4C2.44772 4 2 4.44772 2 5V14C2 14.5523 2.44772 15 3 15H10C10.5523 15 11 14.5523 11 14V5C11 4.44772 10.5523 4 10 4H3ZM4 13V6H9V13H4Z" fill="#ABB1B8"/>
                        </svg>
                    </span>
                </div>
                <div class="sj-query-bar-help">
                    <a class="sj-query-bar-icon" href="http://docs.shooju.com/queries/" target="_blank">
                        ${docSvg}
                    </a>
                    <div class="sjsf-search-help-popup">
                        <div class="sjsf-search-help-popup-inner">
                            <div data-mode="filters">
                                <strong>
                                    Query Builder
                                </strong>
                                <div>
                                    Click the
                                    <i>
                                        <svg width="30" height="30" fill="none" xmlns="http://www.w3.org/2000/svg"
                                            data-help-main-icon="add">
                                            <path d="M0 2C0 0.89543 0.895431 0 2 0H28C29.1046 0 30 0.895431 30 2V28C30 29.1046 29.1046 30 28 30H2C0.89543 30 0 29.1046 0 28V2Z" fill="#567483"/>
                                            <path fill-rule="evenodd" clip-rule="evenodd" d="M6 9H18V11H6V9ZM6 13H18V15H6V13ZM14 17H6V19H14V17ZM22 13V17H26V19H22V23H20V19H16V17H20V13H22Z" fill="white"/>
                                        </svg>
                                        <svg fill="none" height="29" viewBox="0 0 31 29" width="31" xmlns="http://www.w3.org/2000/svg"
                                            data-help-main-icon="plus">
                                            <rect fill="#F4F7F9" height="29" rx="2" width="30.3684" x="0.31543">
                                            </rect>
                                            <rect fill="#3587AF" height="21" rx="1.5" transform="rotate(90 25.3154 13)" width="3" x="25.3154" y="13">
                                            </rect>
                                            <rect fill="#3587AF" height="21" rx="1.5" width="3" x="13.3154" y="4">
                                            </rect>
                                        </svg>
                                    </i>
                                    button to add fields.
                                </div>
                                <div>
                                    Once the fields are selected you can search for and select values for each of them.
                                </div>
                                <div>
                                    Click 
                                    <span class="sjsf-search-help-popup-btn">
                                        <button disabled="disabled">:partial</button> 
                                    </span>
                                    to search anywhere in that field.
                                </div>
                            </div>
                            <div data-mode="sjql">
                            ${hotActions}
                                <div>
                                    ${tooltip()}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            `);

            var $actionsUlEl = this.$actionsEl.find('ul');
            $actionsUlEl.append('<li></li>');
            this.$bottomEl.append(this.$actionsEl);

            this.$actionsEl.find('li').on('click', function (e) {
                var $el = $(e.target);
                var action = $el.data('action');
                switch (action) {
                    case 'operator':
                        break;
                    case 'expression':
                        break;
                }
            });

            this.$msgEl = $('<div class="sj-query-bar-message"><span></span></div>');
            this.$bottomEl.append(this.$msgEl);

            this.$innerEl.attr('data-mode', enableLayouts ? 'filters' : 'sjql');

            if (enableLayouts) {
                this.filters = [];

                var placeholderHtml = `<span class='sj-query-bar-placeholder'>
                    <svg width="14" height="10" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M13 5H1M1 5L5 1M1 5L5 9" stroke="#ABB1B8" stroke-linecap="round" stroke-linejoin="round"/>
                    </svg>
                    <span>
                        Click to add fields to filter by
                    </span>
                    <span>
                        Click to filter by this field
                    </span>
                </span>`;

                var queryModeFieldSelector = `<div class="sj-query-bar-query-filter-selector-wrapper">
                    <i class="fa-solid fa-gear"></i>
                    <div class="sj-section sj-section-f sj-query-bar-query-filter-selector-menu">
                        <div class="sj-query-bar-query-filter-selector-menu-mode" data-fields-mode>
                            <div>
                                Search in:
                            </div>
                            <div>
                                <div class="sj-label sj-label-inline">
                                    <label>
                                        <div class="sj-input">
                                            <input type="radio" name="${this.id}-filter-selector-menu" value="default" data-state="fields-mode"/>
                                        </div>
                                        <span>default fields</span>
                                    </label>
                                </div>
                            
                                <div class="sj-label sj-label-inline">
                                    <label>
                                        <div class="sj-input">
                                            <input type="radio" name="${this.id}-filter-selector-menu" value="all" data-state="fields-mode"/>
                                        </div>
                                        <span>all fields</span>
                                    </label>
                                </div>
                            </div>
                        </div>

                        ${this.settings.getDefaultFields ? `<div class="sj-query-bar-query-filter-default-fields">
                            <label>Default Fields:</label>
                            <ul>
                            </ul>
                        </div>` : ''}

                    </div>
                </div>`;

                this.$filtersEl = $(`<div class="sj-query-bar-filters">
                    <div class="sj-query-bar-filters-left">
                      <div class="sj-query-bar-filters-right-wrap">
                        <div class="sj-query-bar-filters-right sj-input sj-input-xs sj-input-search ${this.settings.showFieldselector ? 'sj-query-bar-query-filter-selector' : ''}">
                            <textarea data-state="contains"></textarea>
                            <span class="sj-query-bar-filter-search-icon sj-input-search-icon">
                                <svg width="16" height="16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M15.6344 13.8641L11.8938 10.1239C12.7444 8.86123 13.1641 7.28359 12.9394 5.60252C12.5563 2.74313 10.2125 0.416195 7.34999 0.0538272C3.09404 -0.484647 -0.484725 3.09372 0.0538391 7.3492C0.41634 10.2126 2.74385 12.558 5.60386 12.9392C7.28512 13.1639 8.86325 12.7444 10.1258 11.8937L13.8664 15.6339C14.3545 16.122 15.1461 16.122 15.6342 15.6339C16.1219 15.1452 16.1219 14.3516 15.6344 13.8641ZM2.47185 6.4993C2.47185 4.29391 4.26623 2.49972 6.47186 2.49972C8.6775 2.49972 10.4719 4.29391 10.4719 6.4993C10.4719 8.70469 8.6775 10.4989 6.47186 10.4989C4.26623 10.4989 2.47185 8.70531 2.47185 6.4993Z" fill="#D8DDE3"/>
                                </svg>
                            </span>
                            <span class="sj-query-bar-filter-search-remove sj-input-clear-icon">
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 352 512">
                                    <path d="M242.72 256l100.07-100.07c12.28-12.28 12.28-32.19 0-44.48l-22.24-22.24c-12.28-12.28-32.19-12.28-44.48 0L176 189.28 75.93 89.21c-12.28-12.28-32.19-12.28-44.48 0L9.21 111.45c-12.28 12.28-12.28 32.19 0 44.48L109.28 256 9.21 356.07c-12.28 12.28-12.28 32.19 0 44.48l22.24 22.24c12.28 12.28 32.2 12.28 44.48 0L176 322.72l100.07 100.07c12.28 12.28 32.2 12.28 44.48 0l22.24-22.24c12.28-12.28 12.28-32.19 0-44.48L242.72 256z"/>
                                </svg>
                            </span>
                            ${(this.settings.autocomplete || {}).suggest ? suggestions(this.settings.autocomplete.classNames) : ''}
                            ${this.settings.showFieldselector ? queryModeFieldSelector : ''}
                        </div>
                      </div>
                      ${placeholderHtml}
                    </div>
                    <div style='clear:both'></div>
                </div>`);

                this.$filtersEl.appendTo(this.$innerEl);
                this.$filtersContainer = this.$filtersEl.find('.sj-query-bar-filters-left');

                this.getContainsEl().on({
                    'blur': e => {
                        this.searchTextChanged();
                    },
                    // prevent new lines in textarea
                    'keypress': e => {
                        switch (e.which) {
                            case 13:
                                e.preventDefault();
                                break;
                        }
                    },
                    'keydown': e => {
                        if (e.keyCode == 13) {
                            this.searchTextChanged();
                        }
                    },
                    'input': e => {
                        this.refreshContainsHeight();
                        this.refreshContainsState();
                    }
                });

                this.getFieldsModeEl().on({
                    'change': e => {
                        this.searchTextChanged();
                        this.refreshContainsPlaceholder();
                    }
                });


                if (this.settings.getDefaultFields) {
                    var defaultFields = this.settings.getDefaultFields();
                    if (defaultFields && defaultFields.length) {
                        this.$filtersEl.find('.sj-query-bar-query-filter-default-fields ul')
                            .html('')
                            .append(defaultFields.map(function (f) {
                                return $('<li>').text(f);
                            }));
                    }
                }

                this.$filtersEl.find('.sj-query-bar-filters-right .sj-query-bar-filter-search-remove').on('click', e => {
                    this.getContainsEl().val('');
                    this.refreshContainsHeight();
                    this.refreshContainsState();
                    this.runSearch();
                });

                // toggle default fields menu
                this.$filtersEl.on('click', '.sj-query-bar-query-filter-selector-wrapper > i', e => {
                    this.openAllFieldsMenu();
                });
                // prevent closing modal after click inside menu
                this.$filtersEl.on('click', '.sj-query-bar-query-filter-selector-wrapper', e => {
                    e.stopPropagation();
                });

                this.$filtersEl.on('click', '.sj-query-bar-placeholder', (e) => {
                    // setTimeout to not close just opened dropdown after click outside
                    setTimeout(() => {
                        var $el = $(e.currentTarget);
                        if ($el.hasClass('sj-query-bar-placeholder-facet')) {
                            this.filters[0].show();
                        } else {
                            this.filters[1].show();
                        }
                    });
                });

                this.$bottomEl.find('.sj-querybar-bar-switch').on('click', e => {
                    if (this.isModeSwitchDisabled()) {
                        return;
                    }
                    var newMode = this.getMode() == 'sjql' ? 'filters' : 'sjql';
                    if (newMode == 'filters') {
                        var success = this.filtersFromQuery(this.getAutocompleteQuery());
                        if (success) {
                            this.runSearch(true);
                        } else {
                            console.warn('unable to switch to Filters mode');
                            return;
                        }
                    }
                    this.$innerEl.attr('data-mode', newMode);
                    this.runSearch(false);
                    if (newMode == 'filters') {
                        this.refreshContainsHeight();
                    }
                    this.refreshUI();
                });
            }

            this.$bottomEl.appendTo(this.$innerEl);

            this.$bottomEl.find('.sj-query-bar-copy-sjql').on('click', e => {
                var q = this.getMode() == 'sjql' ? this.getAutocompleteQuery() : this.getQuery();
                if (!q) {
                    return;
                }

                var $el = $(e.currentTarget);

                var tempInput = document.createElement('textarea');
                tempInput.style.position = 'absolute';
                tempInput.style.top = '-99999px';

                document.body.appendChild(tempInput);
                tempInput.value = q;
                tempInput.select();

                document.execCommand("copy");
                document.body.removeChild(tempInput);

                $el.addClass('sj-query-bar-copy-sjql-copied');
                setTimeout(() => {
                    $el.removeClass('sj-query-bar-copy-sjql-copied');
                }, 3000);
            });

            this.$innerEl.on('click', '.sjsf-submit-btn', e => {
                if (this.settings.autocomplete.disabled !== true) {
                    this.runSearch();
                }
            });
        }

        var $suggestWrapper = this.$innerEl.find('.sj-query-bar-suggestions-wrapper');
        // close suggest
        $suggestWrapper.on('click', '.sj-query-bar-suggestions-close', e => {
            $(e.currentTarget)
                .closest('.sj-query-bar-suggestions-wrapper')
                .removeClass('sj-query-bar-suggestions-visible');
            if (this.settings.suggestionClosed) {
                this.settings.suggestionClosed();
            }
        });

        // select suggestion
        $suggestWrapper.on('click', '.sj-query-bar-suggestions-item', e => {
            var newKeyword = $(e.currentTarget).attr('data-text');

            if (this.getMode() == 'filters') {
                this.getContainsEl().val(newKeyword);
                this.refreshContainsHeight();
                this.refreshContainsState();
                this.runSearch();
            } else {
                this.autocomplete.set_value(newKeyword, true);
                this.runSearch();
            }

            this.checkModeSwitchDisabled();
            this.updateCopyStatus();

            $(e.currentTarget)
                .closest('.sj-query-bar-suggestions-wrapper')
                .removeClass('sj-query-bar-suggestions-visible');
        });


        this.$rootEl.append(this.$innerEl);

        if (!this.settings.autocomplete) {
            this.settings.autocomplete = {};
        }

        if (this.settings.mode == 'advanced' || this.settings.mode == 'minimal') {
            var query_changed = this.settings.autocomplete.query_changed;
            this.settings.autocomplete.query_changed = (q) => {
                if (query_changed) {
                    query_changed(q);
                }
                this.checkModeSwitchDisabled();
                this.updateCopyStatus();
                this.updateEnterPlaceholder();
            };
        }

        this.settings.autocomplete.parent = this;
        this.autocomplete = this.$innerEl.find('.sj-query-bar-autocomplete').sjautocomplete(this.settings.autocomplete);

        if (this.settings.autocomplete.disabled) {
            //do nothing for disabled querybar
        } else {
            this.autocomplete.getDataAsync(null, null, null, true);
        }

        if (this.settings.mode == 'advanced' || this.settings.mode == 'minimal') {
            if (enableLayouts) {
                var q = this.getAutocompleteQuery();
                this.applyQuery(q);
            }
            this.updateEnterPlaceholder();
        }
    }

    searchDefaultFieldsGlobalHandler: any;
    openAllFieldsMenu(show?: boolean) {
        var $menu = this.$filtersEl.find('.sj-query-bar-query-filter-selector-wrapper');

        if (show === undefined) {
            show = $menu.attr('data-open') == 'true' ? false : true;
        }
        $menu.attr('data-open', show);

        if (!this.searchDefaultFieldsGlobalHandler) {
            this.searchDefaultFieldsGlobalHandler = (e) => {
                this.openAllFieldsMenu(false);
            };
        }
        document.removeEventListener('click', this.searchDefaultFieldsGlobalHandler);
        if (show) {
            document.addEventListener('click', this.searchDefaultFieldsGlobalHandler);
        }
    }

    refreshContainsHeight() {
        var $containsEl = this.getContainsEl();
        $containsEl.height('');
        var scrollHeight = $containsEl.prop('scrollHeight');
        if (scrollHeight > $containsEl.height()) {
            $containsEl.height(`${scrollHeight - 8}px`);
        }
    }

    searchTextChanged() {
        if (this.settings.searchTextChanged) {
            this.settings.searchTextChanged();
        }
        this.runSearch();
        this.redrawPlaceholder();
        this.updateCopyStatus();

        var pin = this.getPinFilter();
        if (pin) {
            pin.show(true);
        }
    }

    getAutocompleteQuery() {
        return trim(this.autocomplete.get_value() || '');
    }

    checkModeSwitchDisabled() {
        if (this.getMode() == 'sjql') {
            var isSimpleQuery = this.isSimpleQuery();
            var $switchToFilters = this.$innerEl.find('.sj-querybar-bar-switch');
            if (isSimpleQuery) {
                $switchToFilters.removeClass('sj-querybar-bar-switch-disabled');
            } else {
                $switchToFilters.addClass('sj-querybar-bar-switch-disabled');
            }
        }
    }

    updateEnterPlaceholder() {
        var $enterPlaceholder = this.$innerEl.find('.sj-ac-input-enter');
        var q = this.getAutocompleteQuery();
        if (q) {
            $enterPlaceholder.addClass('sj-ac-input-enter-hidden');
        } else {
            $enterPlaceholder.removeClass('sj-ac-input-enter-hidden');
        }
    }

    refreshContainsState() {
        var $containsEl = this.getContainsEl();
        var containsVal = $containsEl.val();
        if (containsVal) {
            $containsEl.addClass('sj-query-bar-contains-active');
        } else {
            $containsEl.removeClass('sj-query-bar-contains-active');
        }
    }

    applyQuery(q) {
        if (this.settings.mode == 'advanced' || this.settings.mode == 'minimal') {
            var mode: void | 'sjql' | 'filters' = 'sjql';

            if (this.settings.enableLayouts && this.settings.mode == 'advanced') {
                var preferedMode = this.settings.getPreferedMode ? this.settings.getPreferedMode() : this.getMode();

                mode = preferedMode;
                if (!mode) {
                    mode = 'filters';
                }

                if (mode == 'filters') {
                    this.$innerEl.attr('data-mode', mode);
                    var success = this.filtersFromQuery(q);
                    if (!success) {
                        mode = 'sjql';
                    }
                }
            }

            if (mode == 'sjql') {
                this.$innerEl.attr('data-mode', mode);
                this.autocomplete.set_value(q, true);
            }
            this.checkModeSwitchDisabled();
            this.updateCopyStatus();
            this.updateEnterPlaceholder();
        }
    }

    isSimpleQuery() {
        var q = this.getAutocompleteQuery();
        return !q || this.filtersFromQuery(q, true);
    }

    filtersFromBoxes(queryBoxes) {
        var facets = {};

        // merge state filters and parsed filters
        var stateFilters = this.settings.getFilters() || [];

        var pinFilter;
        if (stateFilters.length && (stateFilters[0] === '' || stateFilters.lastIndexOf(stateFilters[0]) !== 0)) {
            pinFilter = stateFilters[0];
            stateFilters.shift();
        }

        var queryBoxesMap = {};
        queryBoxes.forEach(qb => {
            if (qb.field) {
                queryBoxesMap[qb.field] = qb;
                if (stateFilters.indexOf(qb.field) == -1) {
                    stateFilters.push(qb.field);
                }
            }
        });

        // mark shown facets
        stateFilters.forEach(f => {
            facets[f] = true;
        });

        // set contains value
        this.getContainsEl().val('');
        this.getFieldsModeEl()
            .prop('checked', false)
            .filter(`[value="default"]`)
            .prop('checked', true);
        this.refreshContainsHeight();
        queryBoxes.forEach(qb => {
            if (!qb.field) {
                this.getContainsEl().val(qb.value);
                this.refreshContainsHeight();
                this.refreshContainsState();

                this.getFieldsModeEl()
                    .prop('checked', false)
                    .filter(`[value="${qb.fmode || 'default'}"]`)
                    .prop('checked', true);
            }
        });
        this.refreshContainsPlaceholder();

        var fFilterState: any = {
            selected: facets
        };
        if (pinFilter === "") {
            fFilterState.pined = true;
        }
        this.addFilter(null, fFilterState, true);

        stateFilters.forEach(f => {
            var qb = queryBoxesMap[f];

            var state;
            if (qb) {
                if (qb.type == 'nofield') {
                    return;
                }

                switch (getFieldMode(qb.field)) {
                    case 'num':
                        state = {
                            op: qb.op,
                            val: qb.value
                        };
                        break;
                    case 'date':

                        /*
                            if (qb.style == 'relative' && qb.op != '>') {
                                return;
                            }
                        */
                        state = {
                            style: qb.style,

                            rel_sign: '+',
                            rel_op: '<',
                            rel_val: '',
                            rel_per: 'd',

                            abs_per: 'd',
                            selected: {},
                            not: qb.not || false
                        };

                        if (state.style == 'relative') {
                            state.rel_op = qb.op;
                            state.rel_val = qb.value;
                            state.rel_sign = qb.sign;
                            state.rel_per = qb.period;
                        } else {
                            state.abs_per = qb.period;
                            if (qb.value) {
                                if (Array.isArray(qb.value)) {
                                    qb.value.forEach(v => {
                                        state.selected[v] = true;
                                    });
                                } else {
                                    state.selected[qb.value] = true;
                                }
                            }
                        }
                        break;
                    case 'text':
                        var selected = {};
                        if (qb.op == ':') {
                            state = {
                                selected: {},
                                val: qb.value,
                                fuzzy: true,
                                not: qb.not || false
                            }
                        } else {
                            if (qb.value) {
                                if (Array.isArray(qb.value)) {
                                    qb.value.forEach(v => {
                                        selected[v] = true;
                                    });
                                } else {
                                    selected[qb.value] = true;
                                }
                            }
                            state = {
                                selected: selected,
                                val: '',
                                fuzzy: false,
                                not: qb.not || false
                            }
                        }
                        break;
                    case 'tree':
                        var selected = {};
                        if (qb.value) {
                            if (Array.isArray(qb.value)) {
                                qb.value.forEach(v => {
                                    selected[v] = true;
                                });
                            } else {
                                selected[qb.value] = true;
                            }
                        }
                        if (qb.except) {
                            if (Array.isArray(qb.except)) {
                                qb.except.forEach(v => {
                                    selected[v] = false;
                                });
                            } else {
                                selected[qb.except] = false;
                            }
                        }

                        state = {
                            selected: selected,
                            val: '',
                            exact: qb.op == '=',
                            not: qb.not || false
                        };
                        break;
                }
            }
            if (pinFilter !== undefined && state) {
                if (pinFilter == f) {
                    state.pined = true;
                }
            }
            var filter = this.addFilter(f, state, true);
        });

        if (pinFilter !== undefined) {
            var filterForPin = this.getFilterForPin();
            if (filterForPin) {
                this.addFilter(pinFilter || null, filterForPin.state, null, true, filterForPin.lastResponse);
            }
        }

        this.filtersListChanged();
    }

    getParserOptions() {
        return {
            supportAllFields: this.settings.showFieldselector
        };
    }

    filtersFromQuery(q, validateOnly = false) {
        q = q || "";

        var queryBoxes = queryToBoxes(q, this.getParserOptions());
        var newQ = boxesToQuery(queryBoxes || []) || "";

        var useFiltersMode = q == newQ;
        if (validateOnly) {
            return useFiltersMode;
        }

        this.removeFilters();

        if (useFiltersMode) {
            this.filtersFromBoxes(queryBoxes);
            return true;
        } else {
            console.warn('Unable to parse so complex query.', q, newQ);
            return false;
        }
    }

    hideFilters() {
        this.filters.forEach(filter => {
            filter.hide();
        });
    }

    addFilter(facet, state = null, multiple = false, pined = false, lastResponse = null) {

        if (pined) {
            var pin = this.getPinFilter();
            if (pin) {
                this.removeFilter(pin.settings.field, true);
            }
        }

        var queryBarFilter = new QueryBarFilter({
            $root: this.$filtersContainer,
            field: facet || '',
            pinable: !pined,
            pined: pined,
            getQuery: () => {
                var q = this.getQuery(queryBarFilter);
                if (this.settings.autocomplete.format_query) {
                    return this.settings.autocomplete.format_query(q);
                }
                var fqDfd = $.Deferred();
                fqDfd.resolve(q);
                return fqDfd.promise();
            },
            request: this.request,
            onStateChange: (field, action) => {
                this.runSearch();
                this.redrawPlaceholder();
                this.updateCopyStatus();

                var pin = this.getPinFilter();
                if (pin) {
                    var refreshPin = pin.settings.field != field || action != 'toggle';
                    if (refreshPin) {
                        pin.show(true);
                    }
                }
            },
            getLastUsedArr: () => {
                return this.annotations.getLastUsedArr();
            },
            onFilterChange: (term, selected, replaceWithSet) => {
                if (selected == 'true') {
                    this.annotations.addTermToLastUsed(term);
                    this.addFilter(term);
                } else {
                    if (replaceWithSet) {
                        var setFilter = this.filters.find(f => f.settings.field == 'set');
                        if (setFilter) {
                            setFilter.state.selected[term] = true;
                            setFilter.renderTermLabel();
                        } else {
                            var selectedMap = {};
                            selectedMap[term] = true;
                            this.addFilter('set', {
                                selected: selectedMap,
                                fuzzy: false,
                                val: ''
                            }, true);
                        }
                    }
                    this.removeFilter(term);
                }
                this.redrawPlaceholder();
                this.updateCopyStatus();
            },
            addSelectedToTable: this.settings.addSelectedToTable,
            getTableFields: this.settings.getTableFields,
            refreshUI: this.settings.refreshUI,

        }, this, state);

        if (pined) {
            queryBarFilter.lastResponse = lastResponse || {};
            this.filters.unshift(queryBarFilter);
        } else {
            this.filters.push(queryBarFilter);
        }

        queryBarFilter.draw(baseFieldNamesMap, [], [], false);
        this.annotations.getAnnotations().then(annotations => {
            var $ulScroll = queryBarFilter.$el.find('.sj-query-bar-filter-list-inner > ul:visible');
            var scrollTop = $ulScroll.length ? $ulScroll.scrollTop() : null;
            queryBarFilter.$el.remove();
            queryBarFilter.draw(annotations.by_field || {}, annotations.recommended || [], annotations.hide_fields || [], annotations.wizard || false);
            if (scrollTop !== null) {
                queryBarFilter.show().then(() => {
                    $ulScroll = queryBarFilter.$el.find('.sj-query-bar-filter-list-inner > ul:visible');
                    if ($ulScroll.length) {
                        $ulScroll.scrollTop(scrollTop);
                    }
                });
            }
        });

        this.redrawPlaceholder();
        this.updateCopyStatus();

        if (multiple !== true) {
            this.filtersListChanged();
        }

        if (pined) {
            queryBarFilter.show();
        }

        this.refreshUI();

        return queryBarFilter;
    }

    filtersListChanged() {
        this.settings.filtersChanged(this.filters.map(f => {
            if (f.settings.field || f.settings.pined) {
                return f.settings.field || '';
            }
        }).filter(f => f !== undefined));
    }

    redrawPlaceholder() {
        var $placeholder = this.$rootEl.find('.sj-query-bar-placeholder');
        if (this.filters.length > 1) {
            var hasValues = false;
            this.filters.forEach(f => {
                if (f.getBox()) {
                    hasValues = true;
                }
            });
            if (hasValues) {
                $placeholder.removeClass('sj-query-bar-placeholder-facet');
                $placeholder.removeClass('sj-query-bar-placeholder-value');
            } else {
                $placeholder.removeClass('sj-query-bar-placeholder-facet');
                $placeholder.addClass('sj-query-bar-placeholder-value');
            }
        } else {
            $placeholder.addClass('sj-query-bar-placeholder-facet');
            $placeholder.removeClass('sj-query-bar-placeholder-value');
        }
    }

    // enable/disable copy query
    updateCopyStatus() {
        var $copyBtn = this.$rootEl.find('.sj-query-bar-copy-sjql');
        var q = this.getMode() == 'sjql' ? this.getAutocompleteQuery() : this.getQuery();
        if (q) {
            $copyBtn.removeClass('sj-query-bar-copy-sjql-blank');
        } else {
            $copyBtn.addClass('sj-query-bar-copy-sjql-blank');
        }
    }

    getContainsEl() {
        return this.$filtersEl.find('[data-state="contains"]');
    }

    getFieldsModeEl() {
        return this.$filtersEl.find('[data-state="fields-mode"]');
    }

    refreshContainsPlaceholder() {
        var $containsEl = this.getContainsEl();
        var $fieldsModeEl = this.getFieldsModeEl();
        var fieldsMode = $fieldsModeEl.filter(':checked').val() || 'default';

        $fieldsModeEl.closest('.sj-query-bar-query-filter-selector-menu-mode').attr('data-fields-mode', fieldsMode);
        $containsEl.attr('placeholder', `Search ${fieldsMode} fields for...`);
    }

    getQuery(filterIgnore = null) {

        if (this.settings.mode == 'minimal') {
            return this.autocomplete.get_value();
        }

        var boxes = [];

        this.filters.forEach(f => {
            var ignore = false;
            if (filterIgnore && (f == filterIgnore || f.settings.field == filterIgnore.settings.field)) {
                ignore = true;
            }
            if (!ignore) {
                var box = f.getBox();
                if (box) {
                    boxes.push(box);
                }
            }
        });

        var globalQ = this.getContainsEl().val();
        var fieldsMode = this.getFieldsModeEl().filter(':checked').val();
        if (globalQ || fieldsMode != 'default') {
            var nofieldBox = {
                type: 'nofield',
                value: typeof globalQ == 'string' ? globalQ : null
            };
            if (fieldsMode != 'default') {
                nofieldBox['fmode'] = fieldsMode;
            }
            boxes.push(nofieldBox);
        }

        var q = boxesToQuery(boxes);
        return q;
    }

    runSearch(triggerSearch = true) {
        var q = this.getQuery();
        if (this.autocomplete.set_value) {
            this.autocomplete.set_value(q, triggerSearch);
        }
        if (triggerSearch) {
            this.autocomplete.getDataAsync();
        }
    }

    removeFilters(leaveRoot = false) {
        if (leaveRoot) {
            this.filters.forEach((f, i) => {
                if (i) {
                    f.remove();
                } else {
                    f.state.selected = {};
                }
            });
            this.filters.slice(1);
        } else {
            this.filters.forEach(f => {
                f.remove();
            });
            this.filters = [];
        }
    }

    getFilterForPin() {
        return this.filters.find(filter => {
            return !filter.settings.pined && filter.state.pined;
        });
    }

    getPinFilter() {
        return this.filters.find(filter => {
            return filter.settings.pined;
        });
    }

    removeFilter(term, pined = false) {
        var filter = this.filters.find(filter => {
            return filter.settings.field == term && filter.settings.pined == pined;
        });
        if (filter) {
            if (pined) {
                var mainFilter = this.filters.find(filter => {
                    return filter.settings.field == term && !filter.settings.pined
                });
                if (mainFilter) {
                    mainFilter.state.pined = false;
                    mainFilter.$el.removeClass('sj-query-bar-filter-pinable');
                }
            } else {
                delete this.filters[0].state.selected[term];
                this.filters[0].renderTermLabel();
            }

            filter.remove();
            this.filters = this.filters.filter(f => f != filter);

            if (!pined) {
                this.removeFilter(term, true);
                this.runSearch();
            }

            var pin = this.getPinFilter();
            if (pin) {
                if (pin.settings.field) {
                    pin.show(true);
                } else {
                    var $label = pin.$el.find(`.sj-query-bar-filter-item[data-term="${term}"] label[data-checked]`);
                    $label.attr('data-checked', false);
                }
            }
        }
        this.redrawPlaceholder();
        this.updateCopyStatus();
        this.filtersListChanged();
        this.refreshUI();
    }

    /**
     * Returns query bar mode: 'filters' or 'sjql'
     */
    getMode() {
        return this.$innerEl.attr('data-mode') || 'sjql';
    }

    /**
     * Check if 'Switch to Query Builder' is disabled (unable to convert query to filters)
     */
    isModeSwitchDisabled() {
        return this.$bottomEl.find('.sj-querybar-bar-switch').hasClass('sj-querybar-bar-switch-disabled');
    }

    refreshUI() {
        var setMargin = this.getMode() == 'filters';
        var filter;
        if (setMargin) {
            filter = this.filters.find(filter => {
                return filter.settings.pined;
            });
        }

        var isTheme = this.$acEl.parents('.sj-theme-shooju-22').length;

        if (filter) {
            this.$innerEl.addClass('sj-query-bar-ml');
            var $filtersListEl = filter.$el.find('.sj-query-bar-filter-list');
            var left = ($filtersListEl.width()) + 'px';

            var $filtersItems = this.$filtersEl.find('.sj-query-bar-filters-left');
            $filtersListEl.height('');
            $filtersListEl.height($filtersItems.height() + 'px');
            if (isTheme) {
                // this.$filtersEl.css('marginLeft', left);
                this.$innerEl.css('marginLeft', left);
                this.$filtersContainer.css('marginLeft', '-' + left);
            } else {
                this.$bottomEl.css('marginLeft', left);
            }
        } else {
            this.$innerEl.removeClass('sj-query-bar-ml');

            if (isTheme) {
                // this.$filtersEl.css('marginLeft', '');
                this.$innerEl.css('marginLeft', '');
                this.$filtersContainer.css('marginLeft', '');
            } else {
                this.$bottomEl.css('marginLeft', '');
            }
        }

        if (this.settings.refreshUI) {
            this.settings.refreshUI(filter);
        }
    }

    destroy() {
        if (this.autocomplete) {
            this.autocomplete.destroy();
        }

        this.$rootEl.empty().removeAttr('sj-qb-mode').removeAttr('data-sj-widget');
        this.$rootEl.append(this.$children);
    }
    set_title_status(tooltipType) {
        var $icon = this.$innerEl.find('.sjsf-search-help-icon');
        $icon.removeClass('sjsf-search-tip-type-structured sjsf-search-tip-type-unstructured sjsf-search-tip-type-error');
        if (tooltipType) {
            $icon.addClass('sjsf-search-tip-type-' + tooltipType);
        }
    }
    set_status(data, err, q) {
        if (!data) {
            return;
        }
        if (this.settings.mode == 'advanced' || this.settings.mode == 'minimal') {
            var tooltipType;
            var tooltipText;

            if (data.success || data['validate_query']) {
                var validate_query = data['validate_query'];
                if (validate_query) {
                    tooltipType = validate_query.type;
                    switch (validate_query.type) {
                        case 'structured':
                            break;
                        case 'unstructured':
                            tooltipText = validate_query.description;
                            break;
                        case 'error':
                            tooltipText = validate_query.description;
                            break;
                    }
                }
            } else {
                tooltipType = 'error';
                tooltipText = 'Invalid query: ' + data['error'];
            }

            if (tooltipType != 'error') {
                if (this.settings.enableLayouts && this.getMode() == 'filters') {
                    var isUnstructured = /*this.getContainsEl().val() ||*/ this.filters.filter(f => {
                        return f.isUnstructured();
                    }).length;
                    if (isUnstructured) {
                        tooltipType = 'unstructured';
                        tooltipText = 'remove fuzzy queries to make structured';
                    }
                }
            }

            this.set_title_status(tooltipType);

            if (tooltipType) {
                this.$statusEl.show().attr('data-status', tooltipType).text(tooltipType);
                if (tooltipText) {
                    this.$msgEl.find('span').text(tooltipText).attr('title', tooltipText);
                } else {
                    this.$msgEl.find('span').text('').attr('title', '');
                }
            } else {
                this.$statusEl.hide();
                this.$msgEl.find('span').text('').attr('title', '');
            }
        }
    }

    setSuggestions(suggestions) {
        this._setSuggestions(suggestions, this.$rootEl);
    }

    _setSuggestions(suggestions, $container) {
        if (suggestions && suggestions.length) {
            var $sEl = $container
                .find('.sj-query-bar-suggestions-wrapper')
                .addClass('sj-query-bar-suggestions-visible')
                .find('.sj-query-bar-suggestions-items')
                .html('');
            suggestions.forEach((s, i) => {
                $sEl.append(() => {
                    return $(`<span>${s.highlighted}</span>`)
                        .addClass(`sj-query-bar-suggestions-item`)
                        .attr('data-text', s.value);
                });
                if (i < suggestions.length - 1) {
                    $sEl.append(', ');
                }
            });
        } else {
            $container
                .find('.sj-query-bar-suggestions-wrapper')
                .removeClass('sj-query-bar-suggestions-visible');
        }
    }
}

export default QueryBar;
import { $, endsWith } from './helpers';

var baseFieldNamesMap = {
    'sid': {
        name: 'Series ID'
    },
    'set': {
        name: 'Fields Set'
    }
};

class Annotations {
    sjAcLastUsedKeyName = 'sjAcLastUsed';

    constructor(private request) {
    }

    getAnnotations() {
        if (window['sjAcAnnotations']) {
            return window['sjAcAnnotations'];
        }

        var dfd = $.Deferred();

        if (window['SHOOJU_CONFIG'] && window['SHOOJU_CONFIG'].use_annotations) {
            this.request.call({
                method: 'POST',
                url: "/api/1/processors/sjextensions/call/annotations",
                data: {
                    settings: {}
                },
                success: (response, textStatus) => {
                    dfd.resolve(this.addAnnotations(Object.assign({
                        by_field: {},
                        recommended: [],
                        hide_fields: []
                    }, response.result)));
                },
                error: (err) => {
                    dfd.resolve(this.addAnnotations({
                        by_field: {},
                        recommended: [],
                        hide_fields: []
                    }));
                }
            });
        } else {
            dfd.resolve(this.addAnnotations({
                by_field: {},
                recommended: [],
                hide_fields: []
            }));
        }

        window['sjAcAnnotations'] = dfd.promise();
        return window['sjAcAnnotations'];
    }

    addAnnotations(response) {
        Object.assign(response.by_field, baseFieldNamesMap);
        return response;
    }

    addTermToLastUsed(term) {
        if (term.indexOf('meta.') == 0 || term == 'sid' || term == 'set') {
            return;
        }

        var lastUsed = this.getLastUsedFields();
        if (!lastUsed[term]) {
            lastUsed[term] = 0;
        }
        lastUsed[term]++;

        var lastUsedArr = this.getLastUsedArr(lastUsed);
        var lastUsedArrTop = {};
        lastUsedArr.slice(0, 200).forEach(x => {
            lastUsedArrTop[x] = lastUsed[x];
        });

        localStorage.setItem(this.sjAcLastUsedKeyName, JSON.stringify(lastUsedArrTop));
    }

    getLastUsedFields() {
        var lastUsed;
        try {
            lastUsed = localStorage.getItem(this.sjAcLastUsedKeyName);
        } catch (e) {
            return {};
        }
        if (lastUsed) {
            var result = JSON.parse(lastUsed);
            // fallback to remove sid/set/meta.*/*_obj fields if they were added before (now there are not added)
            Object.keys(result).forEach(key => {
                if (key === 'sid' || key === 'set' || key.indexOf('meta.') === 0 || endsWith(key, '_obj')) {
                    delete result[key];
                }
            });
            return result;
        } else {
            return {};
        }
    }

    getLastUsedArr(lastUsed = null) {
        if (!lastUsed) {
            lastUsed = this.getLastUsedFields();
        }

        var lastUsedArr = Object.keys(lastUsed);
        lastUsedArr.sort((a, b) => {
            var ai = lastUsed[a];
            var bi = lastUsed[b];
            if (ai === bi) {
                return 0;
            }
            return ai > bi ? -1 : 1;
        });
        return lastUsedArr;
    }
}

export default Annotations;

export { baseFieldNamesMap };
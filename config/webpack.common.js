const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = {
  entry: './js/jquery.sjautocomplete.ts',
  output: {
    filename: 'jquery.sjautocomplete.js',
    path: path.resolve(__dirname, '../dist'),
  },
  externals: {
    "jquery": "jQuery"
  },
  resolve: {
    extensions: ['.js', '.ts']
  },
  module: {
    rules: [
      {
        test: /\.ts$/,
        use: 'ts-loader'
      },
      {
        test: /\.scss$/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader
          },
          'css-loader',
          {
            loader: "sass-loader",
            options: {
              // Prefer `dart-sass`
              implementation: require("sass"),
            }
          }
        ],
      }
    ]
  }
};
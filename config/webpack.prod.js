var commonConfig = require('./webpack.common.js');
var webpackMerge = require('webpack-merge');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = webpackMerge(commonConfig, {
    mode: 'production',
    output: {
        filename: 'jquery.sjautocomplete.min.js'
    },
    plugins: [
        new MiniCssExtractPlugin({
            filename: 'jquery.sjautocomplete.min.css',
            chunkFilename: 'jquery.sjautocomplete.min.css',
            ignoreOrder: false
        })
    ]
});
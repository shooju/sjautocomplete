var commonConfig = require('./webpack.common.js');
var webpackMerge = require('webpack-merge');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = webpackMerge(commonConfig, {
    mode: 'development',
    plugins: [
        new MiniCssExtractPlugin({
            filename: 'jquery.sjautocomplete.css',
            chunkFilename: 'jquery.sjautocomplete.css',
            ignoreOrder: false
        })
    ]
});